import AsyncStorage from '@react-native-community/async-storage';

export const storeData = async (key, value) => {
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem(`@oneway_${key}`, jsonValue)
    } catch (e) {
        // saving error
    }
}


export const getData = async (key) => {
    try {
        const jsonValue = await AsyncStorage.getItem(`@oneway_${key}`)
        return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
        // error reading value
    }
}

export const removeData = async (key) => {
    try {
        await AsyncStorage.removeItem(`@oneway_${key}`)
    } catch (e) {
        // remove error
    }

    //console.log('Done.')
}