
import Api from '../Constants/Api';
import axios from 'axios';
import Store from '../Store'
const delay = () => new Promise((resolve) => setTimeout(resolve, 1000));
class CallApi {
  static login = async (data) => {
    //console.log(Api.user.login)
    return await Axios.post(Api.user.login, data);
  }

  static register = async (data) => {
    return await Axios.post(Api.user.register, data);
  }

  static logout = async (data) => {
    return await Axios.post(Api.user.logout, data, true);
  }
  
  static sendPhoneForgotPass = async (data) => {
    return await Axios.post(Api.forgot_password.sendPhone, data, false);
  }

  static verificationCodeForgotPass = async (data) => {
    return await Axios.post(Api.forgot_password.verificationCode, data, false);
  }

  static setNewPassForgotPass = async (data) => {
    let token = data.token
    axios.defaults.headers['ACCESS_TOKEN'] = `Bearer ${token}`
    delete data.token
    //console.log(data, Api.forgot_password.newPassword)
    return await axios.post(Api.forgot_password.newPassword, data);
  }

  static changePassword = async (data) => {
    return await Axios.post(Api.user.changePassword, data, true);
  }

  static uploadAvatar = async (data) => {
    axios.defaults.headers['Content-Type'] = 'multipart/form-data; charset=utf-8;'
    axios.defaults.headers['ACCESS_TOKEN'] = `Bearer ${Store.getState().user.token}`
    return await axios.post(Api.user.uploadAvatar, data)
  }

  static getUserProfile = async () => {
    // return await HttpHelper.get(Api.profile,{},true);
  }

  static booking = async (data) => {
    return await Axios.post(Store.getState().user.isDriver ? Api.trip.register : Api.booking.register, data, true);
  }

  static getBookings = async (data) => {
    return await Axios.get(Store.getState().user.isDriver ? Api.trip.getTrips : Api.booking.getBookings, data, true);
  }

  static removeItemBookings = async (data) => {
    return await Axios.delete(Store.getState().user.isDriver ? Api.trip.delete : Api.booking.delete , data, true);
  }

  static findBookingsByDateAndAddress = async (data) => {
    return await Axios.get(Api.booking.findBookingsByDateAndAddress, data, true);
  }

  static cancelFindBookingsByDateAndAddress = async (data) => {
    return await Axios.post(Api.booking.cancelFindBookingsByDateAndAddress, null, true);
  }

  // find by user id 
  static findByUserId = async (data) => {
    return await Axios.get(Api.user.findByUserId, data, true);
  }

  static distanceTwoCoordinates = async (data) => {
    return await Axios.get(Api.trip.distance, data, true);
  }

  static getTripsByBookingId = async (data) => {
    return await Axios.get(Store.getState().user.isDriver ? Api.booking.getBookingByTripId : Api.trip.getTripsByBookingId, data, true);
  }
  static getNotification = async (data) => {
    return await Axios.get(Api.notification.getNotifications, data, true);
  }
  static setNotificationStatus = async (data) => {
    return await Axios.post(`${Api.notification.setNotificationStatus}?status=${data.status}`, null, true);
  }

  static getNotificationDetail = async (data) => {
    return await Axios.post(`${Api.notification.getNotificationDetail}?notificationId=${data.notificationId}`, null, true);
  }

  static getNotificationStatus = async () => {
    return await Axios.get(Api.notification.getNotificationStatus, null, true);
  }

  static updateUserProfile = async (data) => {
    //console.log(Api.user.updateUserInfo, data)
    return await Axios.put(Api.user.updateUserInfo, data, true);
  }
  
  static unfollowTripBooking = async (data) => {
    let url = ''
    if(data?.tripId){
      url = `${Api.trip.unfollow}?tripId=${data.tripId}`
    }else if(data?.bookingId){
      url = `${Api.booking.unfollow}?bookingId=${data.bookingId}`
    }
    return await Axios.post(url, null, true);
  }
  static getAllTripBookingByUser = async (data) => {
    let url = Store.getState().user.isDriver ? `${Api.booking.getAllBooking}` : `${Api.trip.getAllTrip}`
    return await Axios.get(url, null, true);
  }
  static getTripBookingById = async (data) => {
    let url = data?.tripId?`${Api.trip.getTripById}`: `${Api.booking.getBookingById}`
    return await Axios.get(url, data, true);
  }

  static getBaseInfo = async (data) => {
    let url = Api.user.getBaseInfo
    return await Axios.get(url, null, true);
  }
}

const addCommonHeader = (authorization) => {
  axios.defaults.headers['Content-Type'] = 'application/json';
  if (authorization) {
    axios.defaults.headers['ACCESS_TOKEN'] = `Bearer ${Store.getState().user.token}`;
  }
};

export class Axios {
  static post = async (url, params, authorization = false) => {
    params&&(params._t = new Date().getTime())
    addCommonHeader(authorization)
    return await axios.post(url, params)
  };
  static get = async (url, params, authorization = false) => {
    params&&(params._t = new Date().getTime())
    addCommonHeader(authorization)
    return await axios.get(url, { params })
  };
  static put = async (url, params, authorization = false) => {
    addCommonHeader(authorization)
    return await axios.put(url, params)
  };
  static delete = async (url, params, authorization = false) => {
    params._t = new Date().getTime();
    addCommonHeader(authorization)
    return await axios.delete(url, { params })
  };
}

export default CallApi;