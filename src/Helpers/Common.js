export const currencyFormat = (num) =>{
    return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + " VNĐ"
}

export const roundFloat = (value, round = 100) =>{
    return Math.round( value * round + Number.EPSILON ) / round
}