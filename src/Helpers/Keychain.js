import * as Keychain from 'react-native-keychain';

export const saveAccount = async (username, password) => {

  await Keychain.setGenericPassword(username, password);

}

export const getAccout = async () => {
  try {
    // Retrieve the credentials
    const credentials = await Keychain.getGenericPassword();
    if (credentials) {
      return credentials
    } else {
      return null
    }
  } catch (error) {
    return null
  }
}

export const removeAccount = async () => {
  await Keychain.resetGenericPassword();
}