import React, { Component } from 'react';
import { Alert, View, Text, Platform, StyleSheet, Image, Linking } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import { Notifications } from 'react-native-notifications';
import NotificationPopup from 'react-native-push-notification-popup';
import { connect } from 'react-redux';
import actions from '../Actions';
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import { Avatar } from 'react-native-elements';
const perfectSize = create(PREDEF_RES.iphoneX.dp)

import Store from './../Store'
import ModelCallApi from '../Model/ModelCallApi';
import { ModelParamsAPIGetTripsByBookingId } from '../Model/ModelTrip';
import Splash from 'react-native-splash-screen';
import Navigator from './../Navigations'
import { getAccout } from '../Helpers/Keychain';
import { CommonActions, useRoute, useNavigationState } from '@react-navigation/native';
import DeviceId from '../Utils/DeviceId';
import { check, PERMISSIONS, RESULTS, request, openSettings } from 'react-native-permissions';
import ModelUser from '../Model/ModelUser';
import ModalMSg from './ModalMsg';
import {requestNotifications} from 'react-native-permissions';

class FirebaseNotification extends Component {
    constructor(props) {
        super(props)
        this.popup = React.createRef()
        this.navigationRef = React.createRef();
        this.state = {
            isGotoMainScreen: false,
            deviceid: '',
            permissions: true,
            content: ''
        }
    }

    componentDidMount() {
        // if (Platform.OS == 'android') {
            this.checkPermission();
            this.createNotificationListeners();
        // } else {
        //     this.handleCheckLogin()
        // }
    }
    //1
    checkPermission = async () => {
        let fcmToken = await AsyncStorage.getItem('@fcmToken');
        if(!fcmToken){
            this.handleCheckLogin()
        }
        if(Platform.OS == 'ios'){
            await requestNotifications(['alert', 'sound'])
        }
        // if(!messaging().isDeviceRegisteredForRemoteMessages){
            await messaging().registerDeviceForRemoteMessages();
        // }
        const enabled = await messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.handleCheckLogin()
            this.requestPermission();
        }
    }

    //3
    getToken = async () => {
        try {
            let fcmToken = await AsyncStorage.getItem('@fcmToken');
            if (!fcmToken) {
                fcmToken = await messaging().getToken();
                if (fcmToken) {
                    // user has a device token
                    await AsyncStorage.setItem('@fcmToken', fcmToken);
                }
            }
            const { dispatch } = this.props
            dispatch(actions.setFcmToken(fcmToken))
            this.handleCheckLogin(fcmToken)
        } catch (error) {
            //console.error(error)
        }
    }

    //2
    requestPermission = async () => {
        try {
            await messaging().requestPermission({
                alert: true,
                announcement: false,
                badge: true,
                carPlay: true,
                provisional: false,
                sound: true,
              });
            // User has authorised
            this.getToken();
        } catch (error) {
            Alert.alert(
                'Ứng dụng không được cấp quyền',
                'Vui lòng chọn cấp quyền cho ứng dung, trong phần cài đặt!',
                [{
                    text: 'Mở cài đặt', onPress: () => {
                        BackHandler.exitApp();
                        openSettings(1).catch(() => {
                            //console.warn('cannot open settings')
                        })
                    }
                },])
        }
    }

    onMessageReceived = (remoteMessage) => {
        //console.log(remoteMessage)
        this.popup.current?.show && this.popup.current.show({
            onPress: () => remoteMessage?.data && this.handleOpenNotification(remoteMessage?.data),
            appIconSource: remoteMessage?.data?.type != 1 ? require('../../assets/icon_design/logo.png') : '',
            appTitle: 'Xe một chiều',
            timeText: 'Now',
            title: remoteMessage?.notification?.title,
            body: remoteMessage?.notification?.body,
            slideOutTime: 10000
        });

    }

    handleOpenNotification = (data) => {
        //console.log(data)
        const { dispatch } = this.props
        const nav = this.navigationRef.current
        var navParams = {}
        if (data.typeString == "TRIP" || data.typeString == "BOOKING") {
            var _data = new ModelCallApi()
            if (data.tripId) {
                _data.params = {
                    tripId: data.tripId
                }
                navParams = {
                    name: 'Booking_detail',
                    params: {
                        showIconCar: true
                    }
                }
            }
            if (data.bookingId) {
                _data.params = {
                    bookingId: data.bookingId
                }
                navParams = {
                    name: 'Booking_detail',
                    params: {
                        showIconCar: false
                    }
                }
            }

            _data.onSuccess = () => {
                this.navigationRef.current && this.navigationRef.current.dispatch(
                    CommonActions.navigate(navParams)
                )
            }

            dispatch(actions.getTripBookingById(_data))
        }
        if (data.typeString == "NOTIFICATION") {
            var _data = new ModelCallApi()
            _data.params = {
                notificationId: data.notificationId
            }
            _data.onSuccess = (result) => {
                this.navigationRef.current && this.navigationRef.current.dispatch(
                    CommonActions.navigate({
                        name: 'Notification_detail',
                        params: {
                            item: result
                        }
                    })
                )
            }
            dispatch(actions.getNotificationDetail(_data))
        }
    }

    createNotificationListeners = async () => {
        messaging.NotificationAndroidPriority.PRIORITY_MAX
        messaging().onMessage(this.onMessageReceived);
        messaging().onNotificationOpenedApp(remoteMessage => remoteMessage?.data && this.handleOpenNotification(remoteMessage?.data));
        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                if (remoteMessage?.data) {
                    this.checkLogin = setInterval(() => {
                        if (this.state.isGotoMainScreen) {
                            if (Store.getState().user.token != '') {
                                this.handleOpenNotification(remoteMessage?.data)
                            }
                            this.checkLogin && clearInterval(this.checkLogin);
                        }
                    }, 300);
                }
            });

        if(Platform.OS == 'ios'){
            messaging().setBackgroundMessageHandler(async remoteMessage => {
                if (remoteMessage?.data) {
                    this.checkLogin = setInterval(() => {
                        if (this.state.isGotoMainScreen) {
                            if (Store.getState().user.token != '') {
                                this.handleOpenNotification(remoteMessage?.data)
                            }
                            this.checkLogin && clearInterval(this.checkLogin);
                        }
                    }, 300);
                }
              });
        }
    }

    onChangeScreen = () => {
        const { dispatch, token, isDriver } = this.props
        var data = new ModelCallApi()
        data.onSuccess = (result) => {
            if (result?.roles.length == 0 && isDriver != null) {
                this.navigationRef.current && this.navigationRef.current.dispatch(
                    CommonActions.navigate("Official_home")
                )
                this.setState({
                    permissions: false,
                    content: result?.msg
                })
                return
            }
            if (result.roles.indexOf("HANH_KHACH") < 0 && isDriver == false) {
                this.navigationRef.current && this.navigationRef.current.dispatch(
                    CommonActions.navigate("Official_home")
                )
                this.setState({
                    permissions: false,
                    content: result?.msg
                })
                return
            }
            if (result.roles.indexOf("TAI_XE") < 0 && isDriver == true) {
                this.navigationRef.current && this.navigationRef.current.dispatch(
                    CommonActions.navigate("Official_home")
                )
                this.setState({
                    permissions: false,
                    content: result?.msg
                })
                return
            }
        }
        token && dispatch(actions.getBaseInfo(data))

    }

    renderCustomPopup = ({ appIconSource, appTitle, timeText, title, body }) => {
        return (
            <View style={styles.container}>
                {appIconSource != '' ? <Image source={appIconSource} style={{ height: perfectSize(40), width: perfectSize(40) }} /> : null}
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: perfectSize(15), fontWeight: 'bold' }} numberOfLines={1}>{title}</Text>
                    <Text style={{ fontSize: perfectSize(13) }} numberOfLines={3}>{body}</Text>
                </View>
            </View>
        )
    };

    handleCheckLogin = async (token = '') => {
        const { dispatch } = this.props

        var account = await getAccout()
        if (account == null || !account.username) {
            this.gotoMainScreen()
            return
        }
        var deviceid = await DeviceId()
        var data = new ModelCallApi();
        data.params = {
            username: account.username,
            password: account.password,
            regId: token,
            device: deviceid
        }
        data.onSuccess = () => {
            this.gotoMainScreen()
        }
        data.onFail = () => {
            this.gotoMainScreen()
        }

        dispatch(actions.login(data))
    }

    gotoMainScreen = () => {
        setTimeout(() => {
            Splash.hide()
            this.setState({
                isGotoMainScreen: true
            })
        }, 100);

    }

    render() {
        const {phoneNumber} = this.props
        const { isGotoMainScreen, permissions, content } = this.state
        if (isGotoMainScreen) {
            return (
                <View style={{ flex: 1 }}>
                    <Navigator ref={this.navigationRef} onStateChange={this.onChangeScreen} />
                    <NotificationPopup ref={this.popup} renderPopupContent={this.renderCustomPopup} />
                    <ModalMSg
                        isVisible={!permissions}
                        title={'Thông báo'}
                        content={content}
                        titleOk='Gọi điện'
                        onPressOK={() => {
                            if (phoneNumber == null || phoneNumber == '') {
                                Alert.alert(`Rất tiếc! Không có số điện thoại để liên hệ!`, null, [
                                    { text: 'Chấp nhận', onPress: () => {this.setState({ permissions: true })} },
                                ])
                                return;
                            }
                            this.setState({ permissions: true })
                            Linking.openURL(`tel:${phoneNumber}`)
                        }}
                        titleCancel='Đóng'
                        onPressCancel={() => this.setState({ permissions: true })}
                        horizoltal
                        xIcon={false}
                    />
                </View>
            )
        }
        return null

    }
}
const mapStateToProp = ({ user = new ModelUser() }) => ({
    token: user.token,
    isDriver: user.isDriver,
    phoneNumber: user.commonInfo.phoneNumber
})

export default connect(mapStateToProp)(FirebaseNotification)

const styles = StyleSheet.create({
    container: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 7,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.3)',
        top: Platform.OS == 'android' ? -perfectSize(20) : 0,
        marginHorizontal: perfectSize(10),
        padding: perfectSize(10),
        borderRadius: perfectSize(10),
        flexDirection: 'row',
        alignItems: 'center'
    }
})