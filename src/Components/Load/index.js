import React from 'react';
import { View, StyleSheet, ActivityIndicator, Text } from 'react-native'
import { Overlay } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import statusLoad from '../../Constants/StatusLoad';
import Actions from '../../Actions';

class LoadProcess extends React.Component {
    icon = () => {
        const props = this.props; 
        if(props.status == statusLoad.loading){
            return <ActivityIndicator
                color = "#ffffff"
                size = "large"
                style = {styles.activityIndicator}
            />
        }
        if(props.status == statusLoad.done){
            return <Icon
                name = 'check-circle'
                size = {33}
                solid
                color="#008000"
            />
        }
        if(props.status == statusLoad.error){
            return <Icon
                name = 'exclamation-circle'
                size = {33}
                solid
                color="#ffd700"
            />
        }
    }
    
    componentDidUpdate(prevProps){
        if(this.props.status == statusLoad.done || this.props.status == statusLoad.error){
            setTimeout(() => {
                this.props.dispatch(Actions.load.hinde())
            }, 1000);
        }
    }

    setTextContent = () => {
        const props = this.props;
        if(props.status == statusLoad.loading){
            return ''
        }else if(props.status == statusLoad.done){
            return ''
        }
    }

    render(){
        const props = this.props;
        return (
            <Overlay overlayStyle = {styles.background}
                isVisible = {props.status != statusLoad.hide}
                width = {80}
                height = {80}
                windowBackgroundColor="rgba(0, 0, 0, .3)"
            >
                <View>
                    {this.icon()}
                    <View style = {styles.textContainer}>
                        <Text style = {styles.textContent}>
                            {this.setTextContent()}
                        </Text>
                    </View>
                </View>
            </Overlay>
        );   
    }
    
}

LoadProcess.propTypes = {
} 

const mapStateToProps = function(state) {
    return {
      status: state.load.status,
    }
  }
export default connect(mapStateToProps)(LoadProcess);

const styles = StyleSheet.create({
    background: {
        backgroundColor: 'rgba(255,255,255,0)',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 0,
    },
    textContainer: {
        // flex: 1,
    },
    textContent: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'white',
    },
    activityIndicator: {
        flex: 1,
    }
});
