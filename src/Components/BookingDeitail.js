import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Dimensions, Image, Linking, Alert } from 'react-native'
import Modal from 'react-native-modal'

import EvilIcons from 'react-native-vector-icons/EvilIcons'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import ModelBooking, { ModelBookingDetail } from '../Model/ModelBooking'
import { connect } from 'react-redux';
import actions from '../Actions'
import { Avatar } from 'react-native-elements'
import ModelUser from '../Model/ModelUser'
import BtnLarge from './BtnLarge'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
const windowWidth = Dimensions.get('window').width;
import moment from 'moment'
import Validator from '../Utils/Validator'
import ModelTrips from '../Model/ModelTrip'
import ModelCallApi from '../Model/ModelCallApi'

class BookingDetail extends React.Component {
    constructor(props) {
        super(props)
    }

    close = () => {
        const { dispatch, navigation } = this.props
        dispatch(actions.hideTripDetail(false));
        navigation.navigate('HomeTab')
    }

    componentDidMount() { }

    carRender = () => {
        return (
            <Image
                source={require('./../../assets/icon_design/car.png')}
                style={{ width: 40, height: 40, transform: [{ rotate: `${Math.random(10, 360)}deg` }] }}
                resizeMode='contain'
            />
        )
    }

    userRender = () => {
        if (!this.props.isDriver) {
            return (
                <Image
                    source={require('./../../assets/icon_design/sidecar.png')}
                    style={{ width: perfectSize(65), height: perfectSize(25), position: 'absolute', right: 10, bottom: perfectSize(5) }}
                    resizeMode='contain'
                />
            )
        }
        return (
            <Image
                source={require('./../../assets/icon_design/locationuser.png')}
                style={{ width: 40, height: 40, position: 'absolute', right: 0, bottom: perfectSize(5) }}
            // resizeMode='contain'
            />
        )
    }

    onPressCall = () => {
        var numberPhone = this.getNumberPhone()
        if (numberPhone == null){
            Alert.alert(`Rất tiếc! Không có số điện thoại để liên hệ!`, null, [
                {text: 'Chấp nhận', onPress: () => {}},
            ])
        }
        Linking.openURL(`tel:${numberPhone}`)
    }

    getNumberPhone = () => {
        const {userBooking } = this.props
        // var mgsPhone = Validator.CheckNumberPhone(userBooking.phone)
        // var mgsPhoneByUsername = Validator.CheckNumberPhone(userBooking.username)
        // if(mgsPhone != '' && mgsPhoneByUsername != ''){
        //     return null
        // }

        // return userBooking.phone || userBooking.username
        return userBooking.phone
    }
    unfollow = () => {
        const {isDriver, trip, dispatch} = this.props
        var data = new ModelCallApi()
        if(isDriver){
            data.params = {
                tripId: trip.tripId
            }
        }else{
            data.params = {
                bookingId: trip.bookingId
            }
        }

        dispatch(actions.unfollowBookingTrip(data))
    }

    render() {
        const { isShowDetails, trip, userBooking } = this.props
        var numberPhone = this.getNumberPhone()
        return (
            <Modal
                isVisible={isShowDetails}
                animationIn='slideInUp'
                animationOut='slideInDown'
                onSwipeComplete={this.close}
                backdropTransitionOutTiming={0}
                backdropOpacity={0}
                backdropColor='black'
                coverScreen={false}
                hasBackdrop={false}
                style={styles.view}
                swipeDirection={['down']}
            >
                <View style={styles.container}>
                    <View style={{ borderBottomWidth: 1, paddingBottom: perfectSize(5), flexDirection: 'row', borderBottomColor: 'rgba(61, 99, 157, 0.7)' }}>
                        <Avatar
                            rounded title="AB"
                            containerStyle={styles.avatarContainer}
                            size={perfectSize(60)}
                            titleStyle={styles.avatarTitle}
                        />
                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{ fontSize: perfectSize(15), lineHeight: perfectSize(26) }}>{userBooking.fullname}</Text>
                            {userBooking.birthday == null ? null : <Text style={{ fontSize: perfectSize(11), lineHeight: perfectSize(13) }}>{moment().diff(moment(userBooking.birthday), 'years')}</Text>}
                        </View>
                        {this.userRender()}
                    </View>
                    <View style={{ justifyContent: 'center' }}>
                        {trip.totalSeat == 0 || trip.totalSeat == null ? null : <Text style={styles.textInfo}>{`Số ghế trống: ${trip.totalSeat}`}</Text>}
                        <Text style={styles.textInfo}>{`Điểm đến: ${trip.toAddress}`}</Text>
                        {trip.note == '' || trip.note == null ? null : <Text style={styles.textInfo}>{`Ghi chú: ${trip.note}`}</Text>}
                        <Text style={styles.textInfo}>{`Ngày khởi hành: ${trip.startDate == null || trip.startDate == "" ? '' : moment(trip.startDate).format('DD-MM-YYYY')} ${trip.startEarliestDate == null || trip.startEarliestDate == "" ? '' : moment(trip.startEarliestDate).format('HH:mm')}`}</Text>
                        {numberPhone == null ? null : <View style = {{flexDirection: 'row'}}><Text style={styles.textInfo}>Hotline: </Text><Text style={[styles.textInfo, {color:'rgba(0,140,250,1)'}]}>{numberPhone}</Text></View>}
                    </View>
                    <View style={{ flexDirection: 'row', flex: 1, paddingHorizontal: perfectSize(10) }}>
                        <BtnLarge title='Bỏ theo dõi' style={[styles.btn, { backgroundColor: '#FF647C' }]} onPress={this.unfollow} />
                        <View style={{ flex: 0.3 }} />
                        <BtnLarge title='Gọi điện' style={[styles.btn, {}]} onPress={this.onPressCall} />
                    </View>
                </View>
            </Modal>
        )
    }
}

const mapStateToProps = ({ user = new ModelUser(), trips = new ModelTrips() }) => ({
    trip: trips.trip,
    isShowDetails: trips.isShowDetails,
    isDriver: user.isDriver,
    userBooking: trips.user
})

export default connect(mapStateToProps)(BookingDetail)
const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: perfectSize(311),
        backgroundColor: 'white',
        borderRadius: 30,
        paddingHorizontal: perfectSize(10),
        paddingVertical: perfectSize(20),
    },
    view: {
        justifyContent: 'flex-end',
        marginBottom: 0,
        marginHorizontal: perfectSize(10),
    },
    avatarContainer: {
        backgroundColor: '#CDD2FD',
        borderColor: '#9BA6FA',
        borderWidth: 2,
        marginRight: perfectSize(30)
    },
    avatarTitle: {
        fontSize: perfectSize(28),
        fontWeight: '500',
        fontFamily: 'roboto'
    },
    title: {
        fontSize: perfectSize(24),
        marginVertical: 15,
        fontWeight: '200',
        textAlign: 'center'
    },
    btn: {
        height: perfectSize(36),
        flex: 1,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15
    },
    btnClose: {
        position: 'absolute',
        padding: 20,
        right: 0
    },
    textInfo: {
        fontSize: perfectSize(15),
        lineHeight: perfectSize(23)
    }
})