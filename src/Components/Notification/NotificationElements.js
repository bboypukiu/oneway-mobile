import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import IconFeather from 'react-native-vector-icons/Feather'
import { ModelNotificationDeitail } from '../../Model/ModelNotification'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

export default ({
    item = new ModelNotificationDeitail(),
    onPress
}) => {
    //console.log(item)
    var label = ''
    var totalCustomer = 0
    var fromAddress = ''
    var toAddress = ''
    var date = ''
    var Icon = <View style = {styles.iconDoneBook} />
    if (item.type == 0) {
        // label = 'Có chuyến mới!'
        // totalCustomer = item.totalCustomer
        // fromAddress = item.fromAddress
        // toAddress = item.toAddress
    }

    if (item.type == 1) {
        // label = 'Hoàn thành chuyến đi'
        // totalCustomer = item.totalCustomer
        // fromAddress = item.fromAddress
        // toAddress = item.toAddress
        Icon = <IconMaterial name="notifications" size={perfectSize(30)} />
    }

    if (item.type == 2) {
        // label = `Có ${item.totalCustomer} khách phù hợp với chuyến của bạn`
        // date = item.date
        Icon = <IconFeather name="filter" size={perfectSize(24)} />
    }

    if (item.type == 3) {
        // label = 'Lọc thành công'
        // totalCustomer = item.totalCustomer
        // fromAddress = item.fromAddress
        // toAddress = item.toAddress
        Icon = <View style = {styles.iconDoneBook} />
    }

                // {totalCustomer != 0 ? (<Text style={styles.text1}>{`Số lượng khách: ${totalCustomer}`}</Text>) : null}
                // {fromAddress != '' ? (<Text style={styles.text2}>{`Điểm đón: ${fromAddress}`}</Text>) : null}
                // {toAddress != '' ? (<Text style={styles.text2}>{`Điểm đến: ${toAddress}`}</Text>) : null}


    return (
        <TouchableOpacity style={styles.view} onPress={()=>onPress&&onPress(item)}>
            <View style = {styles.viewIcon}>
                {Icon}
            </View>
            <View>
                {item.title != '' ? (<Text style={styles.text1}>{`${item.title}`}</Text>) : ''}
                {/*{item.content != '' ? (<Text style={styles.text2}>{`${item.content }`}</Text>) : ''}*/}
                {/*{item.description != '' ? (<Text style={styles.text2}>{`${item.description }`}</Text>) : ''}*/}
                {/*{item.longDes != '' ? (<Text style={styles.text2}>{`${item.longDes }`}</Text>) : ''}*/}
            </View>
            {item?.isSeen == "N"?<View style = {{position: "absolute", left:0, right:0, top:0, bottom:0, backgroundColor:'rgba(196, 196, 196, 0.6)'}}/>: null}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    view: {
        width: '100%',
        borderTopWidth: perfectSize(1),
        borderColor: 'rgba(228, 228, 228, 0.6)',
        paddingHorizontal: perfectSize(30),
        paddingVertical: perfectSize(10),
        flexDirection: 'row'
    },
    viewIcon:{
        width: perfectSize(30),
        alignItems: 'center',
        justifyContent:'center',
        marginRight: perfectSize(10)
    },
    text1:{
        fontSize: perfectSize(13),
        lineHeight: perfectSize(18),
        fontWeight: 'bold'
    },
    text2: {
        fontSize: perfectSize(11),
        lineHeight: perfectSize(13),
        fontWeight: 'bold'
    },
    iconDoneBook: {
        height: perfectSize(10),
        width: perfectSize(10),
        borderRadius: 10,
        backgroundColor: '#6979F8'
    },
    iconNewBook: {
        height: perfectSize(10),
        width: perfectSize(10),
        borderRadius: 10,
        backgroundColor: '#00e500'
    }
})
