import React from 'react'
import { Badge } from 'react-native-elements'
import { connect } from 'react-redux';
import ModelNotifications from '../Model/ModelNotification';
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

class BadgeNotification extends React.Component {

    render() {
        const { size, hasNewNotification } = this.props
        if (hasNewNotification == "Y") {
            return (
                <Badge
                    status="error"
                    badgeStyle={{borderColor: "#6979F8", borderWidth: perfectSize(1) , width :10,height :10}}
                    containerStyle={{ position: 'absolute', top: size + perfectSize(2), right: size, borderColor: "#6979F8" }}
                />
            )
        }
        return null
    }
}

const mapStateToProp = ({ user = new ModelUser(), notification = new ModelNotifications() }) => ({
    token: user.token,
    hasNewNotification: notification.hasNewNotification
})

export default connect(mapStateToProp)(BadgeNotification)
