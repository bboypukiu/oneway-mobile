import React from 'react'
import { Text, StyleSheet } from 'react-native'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
import { TouchableOpacity } from 'react-native-gesture-handler'

export default BtnLarge = ({title, onPress, style = {}}) => (
    <TouchableOpacity style={[styles.btnLarge, style]} onPress={onPress}>
        <Text style={styles.lableBtnLarge}>{title}</Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    lableBtnLarge: {
        fontSize: perfectSize(16),
        fontWeight: 'bold',
        color: 'white',
        fontFamily: 'roboto'
    },
    btnLarge: {
        // marginVertical: 13,
        backgroundColor: '#6979F8',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: perfectSize(50)
    },
})