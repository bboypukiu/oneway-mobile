import React, { useState } from 'react';
import Modal from 'react-native-modal'
import { GOOGLE_PLACES_API_KEY } from './../Constants/Api'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Dimensions, StyleSheet, View, SafeAreaView, Alert, Text, ScrollView, Platform, StatusBar } from 'react-native';
const { width, height } = Dimensions.get('window');
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import ModelPlaceMap from '../Model/ModelPlaceMap';
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
import { useHeaderHeight } from '@react-navigation/stack';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default ModalInputGooglePlaces = ({
    isVisible = false,
    onPressClose = () => { },
    lableSreach = '',
    onDone = () => { }
}) => {
    const [textInput, setTextInput] = useState('');
    var error = null
    const handleError = (err) => {
        if (!error) {
            error = err
            Alert.alert('Đã xảy ra lỗi', error, [{
                text: 'Đồng ý',
                onPress: () => {
                    error = null
                },
            }])
        }
    }

    const handleResult = (data, details) => {
        //console.log(details)
        var _data = new ModelPlaceMap()
        _data.formatted_address = `${details.formatted_address}`.replace(', Vietnam', '')
        _data.location = details.geometry.location
        _data.id = details.id
        _data.place_id = details.place_id
        onDone(_data)
    }
    const rightButton = () => {
        return (
            <View style={styles.iconSearch}>
                <IconMaterial name={'search'} size={perfectSize(36)} color='rgba(0, 0, 0, 0.3)' />
            </View>
        )
    }
    const headerHeight = useHeaderHeight();
    const styles_google = StyleSheet.create({
        textInputContainer: {
            backgroundColor: 'white',
            borderTopWidth: 0,
            borderBottomWidth: 0,
            height: headerHeight,
            padding: perfectSize(10),
            paddingTop: Platform.OS == 'android' ? perfectSize(10) : getStatusBarHeight()
        },
        textInput: {
            transform: [{ translateY: Platform.OS == 'android' ? -8 : 0 }, { translateX: 0 }],
            width: '100%',
            height: '100%',
            fontSize: 16,
            borderColor: 'rgba(0, 0, 0, 0.3)',
            borderWidth: 1,
            borderRadius: 5,
            bottom: Platform.OS == 'android' ? 0 : perfectSize(5),
            position: Platform.OS == 'android' ? 'relative' : 'absolute'
        },
        predefinedPlacesDescription: {
            color: '#1faadb',
        },
        listView: {
            backgroundColor: 'white'
        }
    })
    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0)',
        },
        view: {
            justifyContent: 'flex-end',
            margin: 0
        },
        title: {
            fontSize: perfectSize(24),
            fontWeight: 'bold',
            fontFamily: 'roboto',
            textAlign: 'center'
        },
        iconSearch: {
            bottom:   Platform.OS == 'android' ? (headerHeight - perfectSize(36)) / 2 : perfectSize(5) ,
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            right: perfectSize(20)
        }
    });


    return (
        <Modal
            isVisible={isVisible}
            animationIn='fadeInDown'
            animationOut='fadeOutUp'
            onSwipeComplete={() => onPressClose()}
            backdropTransitionOutTiming={0}
            backdropColor='black'
            style={styles.view}
            keyboardShouldPersistTaps={'handled'}
            onBackdropPress={() => onPressClose()}
            onBackButtonPress={() => onPressClose()}
            backdropOpacity={0.3}
        >
            <GooglePlacesAutocomplete
                keyboardShouldPersistTaps="never"
                query={{
                    key: GOOGLE_PLACES_API_KEY,
                    language: 'vn',
                    // types: 'address', // default: 'geocode'
                    components: 'country:vn'
                }}
                placeholder={`Nhập ${lableSreach}`}
                minLength={4}
                fetchDetails={true}
                returnKeyType={"search"}
                nearbyPlacesAPI={'GooglePlacesSearch'}
                onPress={(data, details = null) => handleResult(data, details)}
                // GooglePlacesDetailsQuery={{ fields: 'formatted_address' }}
                autoFocus={true}
                listViewDisplayed={true}
                onFail={(error) => handleError(error)}
                styles={styles_google}
                debounce={200}
                textInputProps={{
                    onChangeText: (text) => setTextInput(text)
                }}
                keyboardShouldPersistTaps={'handled'}
                renderRightButton={() => !textInput ? rightButton() : null}
            />
        </Modal>
    );
};