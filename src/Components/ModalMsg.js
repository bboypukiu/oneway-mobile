import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Dimensions } from 'react-native'
import Modal from 'react-native-modal'

import EvilIcons from 'react-native-vector-icons/EvilIcons'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
const windowWidth = Dimensions.get('window').width;
export default ModalMsg = ({
    title = '',
    content = '',
    isVisible = false,
    onPressOK = () => { },
    titleOk,
    onPressCancel = () => { },
    titleCancel,
    xIcon = true,
    horizoltal
}) => {
    return (
        <Modal
            isVisible={isVisible}
            // backdropOpacity={0}
            animationIn='fadeIn'
            animationOut='fadeOut'
            backdropTransitionOutTiming={0}
        >
            <View style={[styles.container, title != '' && content != '' && styles.container1]}>
                {xIcon && <TouchableOpacity
                    onPress={onPressCancel}
                    style={styles.btnClose}
                >
                    <EvilIcons
                        name='close'
                        size={30}
                        color='#999999'
                    />
                </TouchableOpacity>}
                {title != '' && <Text style={styles.title}>{title}</Text>}
                {content != '' && <Text style={styles.content}>{content}</Text>}
                <View style={[{ width: '100%' }, horizoltal&&{flexDirection: 'row', marginBottom: perfectSize(30)}]}>
                    {titleOk != '' && <TouchableOpacity
                        onPress={onPressOK}
                        style={horizoltal?styles.btnLarge:styles.btn}
                    >
                        {titleOk != '' && <Text style={horizoltal?styles.lableBtnLarge:{}}>{titleOk}</Text>}
                    </TouchableOpacity>}
                    {horizoltal&&<View style = {{width: perfectSize(30)}}/>}
                    {titleCancel != '' && <TouchableOpacity
                        onPress={onPressCancel}
                        style={horizoltal?styles.btnLarge:styles.btn}
                    >
                        {titleCancel != "" && <Text style={horizoltal?styles.lableBtnLarge:{}}>{titleCancel}</Text>}
                    </TouchableOpacity>}
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: perfectSize(315),
        backgroundColor: 'white',
        borderRadius: 30,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: perfectSize(34),
        paddingVertical: 40
    },
    container1:{
        paddingVertical: 0
    },
    title: {
        fontSize: perfectSize(20),
        marginVertical: 15,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    content: {
        // fontSize: perfectSize(20),
        fontWeight: '200',
    },
    btn: {
        height: perfectSize(50),
        width: '100%',
        borderRadius: 5,
        borderColor: '#6979F8',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15
    },
    btnClose: {
        position: 'absolute',
        padding: 20,
        right: 0
    },
    btnLarge: {
        // marginVertical: 13,
        flex:1,
        backgroundColor: '#6979F8',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: perfectSize(37),
        marginTop: perfectSize(25)
    },
    lableBtnLarge: {
        fontSize: perfectSize(16),
        fontWeight: 'bold',
        color: 'white',
        fontFamily: 'roboto'
    },
})