import React, { useState } from 'react'
import { TextInput, Text, View, StyleSheet, PixelRatio, TouchableOpacity } from 'react-native'
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
export default Input = React.forwardRef((
    {
        label = '',
        errText,
        isRequire = false,
        keyboardType,
        onChangeText,
        onSubmitEditing,
        returnKeyType = 'done',
        style = {},
        secureTextEntry,
        styleInputContainer = {},
        editable = true,
        defaultValue,
        value,
        maxLength,
        onFocus,
        onEndEditing,
        onPressSearch,
        placeholder = '', 
        disableInput, 
        styleLabelContainer = {},
        labelInput = {}
    },
    ref) => {
    const rightIcon = () => {
        const [textInput, setTextInput] = useState('');
        if (onPressSearch)
            return (
                <TouchableOpacity style={styles.iconSearch} onPress={() => onPressSearch()}>
                    <IconMaterial name={'search'} size={perfectSize(26)} color='rgba(0, 0, 0, 0.3)' />
                </TouchableOpacity>
            )
    }
    return (
        <View style={[styles.container, style]}>
            {label != '' ? <View style={[styles.labelView, styleLabelContainer]}>
                <Text style={[styles.labelInput, labelInput]}>{label}</Text>
                {isRequire && <Text style={styles.labelRequire}> (*)</Text>}
            </View> : null}
            <View>
                <View pointerEvents={disableInput?"none":"auto"}>
                    <TextInput
                        ref={ref ? ref : React.createRef()}
                        style={[styles.textInput, errText && styles.borderErr, !editable && styles.disableInput,styleInputContainer, onPressSearch && { paddingRight: perfectSize(30) }]}
                        keyboardType={keyboardType}
                        editable={editable}
                        defaultValue={defaultValue}
                        value={value}
                        maxLength={maxLength}
                        onChangeText={onChangeText}
                        onFocus={onFocus}
                        placeholder={placeholder}
                        onEndEditing={onEndEditing}
                        secureTextEntry={secureTextEntry}
                        onSubmitEditing={onSubmitEditing}
                        returnKeyType={returnKeyType}
                        placeholderTextColor={'black'}
                    />
                </View>
                {editable && <Text style={styles.errText}>{errText}</Text>}
                {rightIcon()}
            </View>
        </View>
    )
})

const styles = StyleSheet.create({
    container: {
        // width: '100%'
    },
    textInput: {
        height: perfectSize(50),
        fontWeight: 'bold',
        borderColor: 'rgba(228, 228, 228, 0.6)',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: perfectSize(20),
        fontFamily: 'roboto',
        fontWeight: 'bold',
        fontSize: perfectSize(13),
        color: '#151522',
        paddingVertical: 0
    },
    errText: {
        position: 'absolute',
        textAlign: 'center',
        fontSize: perfectSize(13),
        color: 'red',
        top: - perfectSize(9),
        backgroundColor: 'white',
        // fontFamily: 'roboto',
        // fontWeight: '100',
        marginLeft: 10
    },
    borderErr: {
        borderColor: '#FF647C',
    },
    labelView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: perfectSize(15)
    },
    labelInput: {
        fontFamily: 'roboto',
        fontWeight: 'bold',
        fontSize: perfectSize(15)
    },
    labelRequire: {
        fontSize: perfectSize(15),
        fontFamily: 'roboto',
        fontWeight: '500',
        color: '#FF647C'
    },
    disableInput: {
        backgroundColor: '#rgba(0,0,0,0.1)'
    },
    iconSearch: {
        width: perfectSize(30),
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: perfectSize(5),
    }
})