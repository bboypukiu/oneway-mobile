import React from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet, Image, ActivityIndicator, Dimensions } from 'react-native'
import IconAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { PERMISSIONS, request, openSettings } from 'react-native-permissions';
import { connect } from 'react-redux'
import ModelTrips from '../../Model/ModelTrip';
import { ModelBookingDetail } from '../../Model/ModelBooking';
import ModelUser from '../../Model/ModelUser';
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import ModelCallApi from '../../Model/ModelCallApi';
import actions from '../../Actions';
import { getZoomDefault, getRegionForCoordinates } from '../../Utils/Map';
const perfectSize = create(PREDEF_RES.iphoneX.dp)

const sizeWindow = Dimensions.get('window')
class Map extends React.Component {
    constructor(props) {
        super(props)
        this.map_ref = React.createRef()
        this.state = {
            mapReady: false
        }
    }

    addMaker = () => {
        const { currentLocation: { longitude, latitude, latitudeDelta, longitudeDelta }, items } = this.props
        var arr = []
        items.map((data = new ModelBookingDetail(), index) => {
            arr.push({
                longitude: data.fromLongtitude,
                latitude: data.fromLatitude,
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta,
                data: data
            })
        })
        return arr
    }


    carRender = () => {
        return (
            <Image
                source={require('./../../../assets/icon_design/car.png')}
                style={{ width: 40, height: 40 }}
                resizeMode='contain'
            />
        )
    }

    userRender = () => {
        return (
            <Image
                source={require('./../../../assets/icon_design/locationuser.png')}
                style={{ width: 40, height: 40 }}
                resizeMode='contain'
            />
        )
    }

    openSettings = () => {
        openSettings(1).catch(() => {
            //console.warn('cannot open settings')
        });
    }

    render() {
        const { currentLocation, isDriver, onPressMarker } = this.props
        const { mapReady } = this.state
        if (currentLocation == null) {
            return (
                <View style={styles.containerPermission}>
                    <Text style={{ fontFamily: 'roboto', fontSize: perfectSize(18), paddingBottom: perfectSize(10) }}>Quyền truy cập vị trí</Text>
                    <Text style={{ fontFamily: 'roboto', textAlign: 'center' }}>Bạn cần phải cấp phép để ứng dụng truy cập vị trí của bạn!</Text>
                    <BtnLarge title='Mở cài đặt' style={[styles.btn]} onPress={this.openSettings} />
                </View>
            )
        }
        if (currentLocation.longitude == null) {
            return (
                <View style={styles.containerPermission}>
                    <Text style={{ fontFamily: 'roboto', textAlign: 'center' }}>Đang lấy vị trí của bạn vui lòng chờ!</Text>
                    <ActivityIndicator
                        style={{ color: '#000' }}
                    />
                </View>
            )
        }
        var items = this.addMaker()
        const initialRegion = getZoomDefault(currentLocation.latitude ,currentLocation.longitude )
        return (
            items.length > 0 ? <MapView
                ref={this.map_ref}
                style={styles.container}
                initialRegion={initialRegion}
                provider={PROVIDER_GOOGLE}
                showsUserLocation={true}
                onMapReady={() => {
                    if (items.length == 0) {
                        return
                    }
                    setTimeout(() => {
                        this.setState({
                            mapReady: true
                        })
                    }, 300)
                }}
            >
                {items.map(marker => (
                    <MapView.Marker
                        coordinate={marker}
                        tracksViewChanges={!mapReady}
                        onPress={() => onPressMarker(marker.data)}
                    >
                        {isDriver ? this.userRender() : this.carRender()}
                    </MapView.Marker>
                ))}

            </MapView>
                :
                <MapView
                    style={styles.container}
                    initialRegion={initialRegion}
                    provider={PROVIDER_GOOGLE}
                    showsUserLocation={true}
                />
        )
    }
}
const mapStateToProps = ({ map, trips = new ModelTrips(), user = new ModelUser() }) => {
    return {
        currentLocation: map,
        isDriver: user.isDriver,
        isShowDetails: trips.isShowDetails,
    }
}


export default connect(mapStateToProps)(Map)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%'
    },
    btn: {
        height: perfectSize(36),
        paddingHorizontal: perfectSize(10),
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15
    },
    containerPermission: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center",
        paddingHorizontal: perfectSize(50),
        backgroundColor: 'white'
    }
})