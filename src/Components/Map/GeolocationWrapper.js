import React, { Component } from 'react'
import {
    Dimensions, PermissionsAndroid, Platform, Alert, AppState
} from 'react-native'
import Geolocation from 'react-native-geolocation-service'

import { connect } from 'react-redux'
import actions from './../../Actions'
import { PERMISSIONS, request, openSettings, check } from 'react-native-permissions';

class GeolocationWrapper extends Component {
    constructor() {
        super()
        this.state = {
            requestCount: 0,
            appState: AppState.currentState
        }
    }

    componentDidMount() {
        if (Platform.OS === 'ios') {
            this.getUserLocation()
            this.watchUserLocation()
        } else {
            this.requestLocationPermission()
        }
        AppState.addEventListener("change", this._handleAppStateChange);
    }

    componentWillUnmount() {
        if (this.watchId !== null) {
            Geolocation.clearWatch(this.watchId)
        }
        AppState.removeEventListener("change", this._handleAppStateChange);
    }

    _handleAppStateChange = nextAppState => {
        if (
            this.state.appState.match(/inactive|background/) &&
            nextAppState === "active"
        ) {
            this.getUserLocation()
        }
        this.setState({ appState: nextAppState });
    };

    requestLocationPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Quyền truy cập vị trí',
                    message: 'Bạn cần phải cấp phép để ứng dụng truy cập vị trí của bạn',
                },
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.getUserLocation()
                this.watchUserLocation()
                //console.log('You can use the location')
                this.setLocationDefaultAndroid()
            } else {
                //console.log('Location permission denied')
            }
        } catch (err) {
            //console.warn(err)
        }
    }

    getUserLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                if (position == null) {
                    setTimeout(() => {
                        this.getUserLocation()
                    }, 1000)
                    return
                }
                const { coords: { latitude, accuracy }, coords } = position
                const { latitudeDelta, longitudeDelta } = this.getLatLongDelta(latitude, accuracy)
                const { requestCount } = this.state
                const { dispatch } = this.props;
                dispatch(actions.setCurentLocation({ ...coords, latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta }));
                this.setState({
                    requestCount: requestCount + 1,
                })
            },
            error => {
                if (error.code == 1) {
                    Alert.alert('Quyền truy cập vị trí', 'Bạn cần phải cấp phép để ứng dụng truy cập vị trí của bạn!', [
                        {
                            text: 'Cài đặt',
                            onPress: () => {
                                openSettings(1).catch(() => {
                                    //console.warn('cannot open settings')
                                });
                            }
                        },
                        {
                            text: 'Huỷ',
                            onPress: () => { }
                        },
                    ])
                }
                if (error.code == 3) {
                    this.getUserLocation()
                }
                this.setState({ error: error.message })
            },
            { timeout: 50000, maximumAge: 1000 },
        )
    }

    getLatLongDelta = (latitude, accuracy) => {
        const oneDegreeOfLatitudeInMeters = 120 * 10
        const latitudeDelta = accuracy / oneDegreeOfLatitudeInMeters
        const longitudeDelta = accuracy / (oneDegreeOfLatitudeInMeters * Math.cos(latitude * (Math.PI / 180)))
        return { latitudeDelta, longitudeDelta }
    }

    watchUserLocation = () => {
        this.watchId = Geolocation.watchPosition(
            (position) => {
                const {
                    coords: {
                        latitude, accuracy, coords,
                    },
                } = position

                const { latitudeDelta, longitudeDelta } = this.getLatLongDelta(latitude, accuracy)
                const { requestCount } = this.state

                if (requestCount >= 100) {
                    dispatch(actions.setCurentLocation({ ...coords, latitudeDelta, longitudeDelta }));
                }
                this.setState((prevState) => { prevState.requestCount >= 100 ? 0 : prevState.requestCount + 1 })
            },
            error => this.setState({ error: error.message }),
            {
                enableHighAccuracy: false, interval: 5000, distanceFilter: 1, fastestInterval: 10000
            },
        )
    }
    setLocationDefaultAndroid = () => {
        const { dispatch } = this.props;
        dispatch(actions.setCurentLocation(
            {
                longitude:null,
                latitude: null,
                latitudeDelta: null,
                longitudeDelta: null,
            }
        ));
    }


    render() {
        return null
    }
}

const mapStateToProps = state => ({
    // currentEveryOneMeterPosition: state.map.currentEveryOneMeterPosition,
})


export default connect(mapStateToProps)(GeolocationWrapper)
