import react from 'react'
import {Image} from 'react-native'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';

export default MapViewPickAddress = ({
    onPickLo
}) =>{
    return(
        <MapView
                ref={this.map_ref}
                style={styles.container}
                region={this.props.currentLocation}
                provider={PROVIDER_GOOGLE}
                showsUserLocation={true}
                
            >
                <MapView.Marker
                    coordinate={this.props.currentLocation}
                    draggable
                    onDragEnd={(e) => {
                        //console.log('dragEnd', e.nativeEvent.coordinate)
                        }
                    }
                >
                    <Image
                        source={require('./../../../assets/icon_design/flag_green.png')}
                        style={{ width: 40, height: 40}}
                        resizeMode='contain'
                    />
                </MapView.Marker>
                <MapView.Marker
                    draggable
                    coordinate={this.props.currentLocation}
                    onDragEnd={(e) => {
                        //console.log('dragEnd', e.nativeEvent.coordinate)
                        }
                    }
                >
                    <Image
                        source={require('./../../../assets/icon_design/flag_red.png')}
                        style={{ width: 40, height: 40}}
                        resizeMode='contain'
                    />
                </MapView.Marker>
            </MapView>
    )
}