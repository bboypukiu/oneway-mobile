import actionTypes from '../Actions/ActionTypes';
import ModelBooking, { ModelListBookings } from '../Model/ModelBooking';

const initState = new ModelBooking()

export const BookingReducer = (state = initState, action) => {
  const { type, data } = action
  switch (type) {

    case actionTypes.REDUX_ADD_BOOKINGS_TO_STORE:
      var newData = new ModelListBookings() 
      newData.bookingDtos = data.bookingDtos ? (data.currentPage == 0 ? [...data.bookingDtos]: [...state.list_bookings.bookingDtos, ...data.bookingDtos]) : (data.currentPage == 0 ? [...data.tripDtos] : [...state.list_bookings.bookingDtos, ...data.tripDtos])
      newData.size = data.size
      newData.totalPages = data.totalPages
      newData.currentPage = data.currentPage
      return Object.assign({}, state, { list_bookings: newData })

    case actionTypes.REDUX_USER_LOGOUT:
      return Object.assign({}, state, { ...initState })
    case actionTypes.REDUX_SET_TRIP_BOOKING_DEFAULT:
      return Object.assign({}, state, { ...initState })
      
    default:
      return state
  }
}

export default BookingReducer;