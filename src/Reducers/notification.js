import actionTypes from '../Actions/ActionTypes';
import ModelNotifications from '../Model/ModelNotification';

const initState = new ModelNotifications()

export const NotificationReducer = (state = initState, action) => {
  const { type, data } = action
  switch (type) {

    case actionTypes.REDUX_ADD_NOTIFICATIONS_TO_STORE:
      var newData = new ModelNotifications()
      newData.notificationDtos = data.currentPage == 0 ? [...data.notificationDtos] : [...state.notificationDtos, ...data.notificationDtos]
      newData.size = data.size
      newData.totalPages = data.totalPages
      newData.currentPage = data.currentPage
      return Object.assign({}, state, newData)

    case actionTypes.REDUX_ADD_NOTIFICATION_STATUS_TO_STORE:
      return Object.assign({}, state, { status: data })

    case actionTypes.REDUX_SET_BASE_INFO:
      return Object.assign({}, state, { hasNewNotification: data.hasNewNotification, notificationStatus: data.notificationStatus})

    case actionTypes.REDUX_USER_LOGOUT:
      return Object.assign({}, state, { ...initState })

    default:
      return state
  }
}

export default NotificationReducer;