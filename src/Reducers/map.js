import actionTypes from '../Actions/ActionTypes';

const initState = null;


const map = (state = initState, action) => {
    const { type, data } = action
    switch (type) {
        case actionTypes.REDUX_SET_CURRENT_LOCATION:
            return Object.assign({}, state, { ...data })
        default:
            return state
    }
}

export default map;