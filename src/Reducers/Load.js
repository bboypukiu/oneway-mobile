import actionTypes from '../Actions/ActionTypes';
import statusLoad from '../Constants/StatusLoad';

const initState = {
    status: statusLoad.hide
};


const LoadReducer = (state = initState, action) => {
    const { type } = action
    const {REDUX_LOAD_DONE, REDUX_LOAD_ERROR, REDUX_LOAD_HIDE, REDUX_LOAD_LOADING} = actionTypes.LOAD
    switch (type) {
      case REDUX_LOAD_DONE :
        return Object.assign({}, state, { status: statusLoad.done })
      case REDUX_LOAD_ERROR :
        return Object.assign({}, state, { status: statusLoad.error })
      case REDUX_LOAD_HIDE :
        return Object.assign({}, state, { status: statusLoad.hide })
      case REDUX_LOAD_LOADING :
        return Object.assign({}, state, { status: statusLoad.loading })
      default:
        return state
    }
  }
  
  export default LoadReducer;