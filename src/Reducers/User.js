import actionTypes from '../Actions/ActionTypes';
import User from '../Model/ModelUser';

const initState = new User()

export const UserReducer = (state = initState, action) => {
  const { type, data } = action
  switch (type) {
    case actionTypes.REDUX_SET_CURRENT_USER_IS_DRIVER:
      return Object.assign({}, state, { isDriver: data })

    case actionTypes.REDUX_SET_CURRENT_USER_AVATAR:
      return Object.assign({}, state, { avatar: data })

    case actionTypes.REDUX_SET_CURRENT_USER_PROFILE:
      return Object.assign({}, state, { ...data })

    case actionTypes.REDUX_SET_FIND_BOOOKING_USER:
      return Object.assign({}, state, { findBooking: { ...data } })

    case actionTypes.REDUX_USER_SET_FCM_TOKEN:
      return Object.assign({}, state, { fcmToken: data })

    case actionTypes.REDUX_SET_BASE_INFO:
      return Object.assign({}, state, { permissions: [...data.roles], commonInfo: { ...data.commonInfo }, findBooking: { ...data.findBooking } })

    case actionTypes.REDUX_USER_LOGOUT:
      var stateDefalt = { ...initState }
      stateDefalt.fcmToken = state.fcmToken
      return Object.assign({}, state, { ...stateDefalt })

    default:
      return state
  }
}

export default UserReducer;