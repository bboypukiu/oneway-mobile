import user from './User';
import load from './Load';
import map from './map';
import booking from './booking'
import trips from './trips'
import notification from './notification'
export default {
  user,
  map,
  load,
  booking,
  trips,
  notification
};