import actionTypes from '../Actions/ActionTypes';
import ModelTrips from '../Model/ModelTrip';

const initState = new ModelTrips()

export const TripsReducer = (state = initState, action) => {
  const { type, data } = action
  switch (type) {

    case actionTypes.REDUX_ADD_TRIPS_BY_BOOKING_ID_TO_STORE:
      return Object.assign({}, state, { trips_by_bookingId: data })
    case actionTypes.REDUX_SHOW_TRIP_DEITAIL:
      return Object.assign({}, state, { trip: data.trip, isShowDetails: true, user: data.user })
    case actionTypes.REDUX_HIDE_TRIP_DETAIL:
      return Object.assign({}, state, { isShowDetails: false})
    case actionTypes.REDUX_ADD_TRIPS_BOOKINGS_AVAILABLE:
      return Object.assign({}, state, { trips_bookings_available: data})
      // return Object.assign({}, state, { trips_by_bookingId: data})
    case actionTypes.REDUX_DELETE_BOOKINGS_TRIPS_AVAILABLE:
      return Object.assign({}, state, {trips_bookings_available: [] })
    case actionTypes.REDUX_USER_LOGOUT:
      return Object.assign({}, state, { ...initState })
    case actionTypes.REDUX_SET_TRIP_BOOKING_DEFAULT:
      return Object.assign({}, state, { ...initState })
    default:
      return state
  }
}

export default TripsReducer;