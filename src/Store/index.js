import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import createSagaMiddleware from 'redux-saga'
import devToolsEnhancer, {composeWithDevTools} from 'remote-redux-devtools';
import reducers from '../Reducers';
import rootSagas from '../Sagas';

const sagaMiddleware = createSagaMiddleware();
const rootReducer = combineReducers(reducers);
// const composeEnhancer = composeWithDevTools({realtime: true,hostname: '192.168.100.53', port:8000});
const composeEnhancer = composeWithDevTools({realtime: true, port:8000});
// const Store = createStore(rootReducer, composeEnhancer(applyMiddleware(sagaMiddleware)));
const Store = createStore(rootReducer, compose(applyMiddleware(sagaMiddleware)));
sagaMiddleware.run(rootSagas);
export default Store;