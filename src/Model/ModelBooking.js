import ModelUser from "./ModelUser";

export default class ModelBooking {
    list_bookings = new ModelListBookings()
    new_booking = new ModelNewBooking()
    booking_detail = new ModelBookingDetail()
}

export class ModelNewBooking {
    starting_point = ''
    destination = ''
    departure_time = ''
    earliest_time = ''
    latest_time = ''
    seater = ''
    note = ''
}

export class ModelListBookings {
    bookingDtos = []
    size = 0
    totalPages = 0
    currentPage = 0
}

export class ModelBookingDetail {
    bookingId = null
    userId = null
    note = ''
    startDate = ''
    startEarliestDate = ''
    startLatestDate = ''
    fromLatitude = null
    fromLongtitude = null
    fromName = ''
    fromTel = ''
    fromAddress = ''
    fromProvinceId = null
    fromDistrictId = null
    fromWardId = null
    fromProvince = ''
    fromDistrict = ''
    fromWard = ''
    fromStreet = ''
    toLatitude = null
    toLongtitude = null
    toName = ''
    toTel = ''
    toAddress = ''
    toProvinceId = null
    toDistrictId = null
    toWardId = null
    toProvince = ''
    toDistrict = ''
    toWard = ''
    toStreet = ''
    totalSeat = null
    totalSeatUsed = null
    totalSeatAvaiable = null
}

export class ModelParamsAPINewBooking {
    // bookingId = 0
    fromAddress = ''
    // fromDistrict = ''
    // fromDistrictId = 0
    fromLatitude = 0
    fromLongtitude = 0
    // fromName = ''
    // fromProvince = ''
    // fromProvinceId = 0
    // fromStreet = ''
    // fromTel = ''
    // fromWard = ''
    // fromWardId = 0
    note = ''
    startDate = ''
    startEarliestDate = ''
    startLatestDate = ''
    toAddress = ''
    // toDistrict = ''
    // toDistrictId = 0
    toLatitude = 0
    toLongtitude = 0
    // toName = ''
    // toProvince = ''
    // toProvinceId = 0
    // toStreet = ''
    // toTel = ''
    // toWard = ''
    // toWardId = 0
    totalSeat = 0
    // totalSeatAvaiable = 0
    // totalSeatUsed = 0
    userId = 0
}

export class ModelParamsAPIGetBooking {
    page = 0
    size = 0
    paged = true
    isLoadFirst = false
}

export class ModelParamsAPIFindBookig {
    inputDate = null
    lon = null
    lat = null
}