export default class ModelUser {
    userId = ''
    email = ''
    fullname = ''
    username = ''
    password = ''
    deleted = ''
    isActive = ''
    createdBy = ''
    updatedBy = ''
    token = ''
    isDriver = null
    avatar = null
    fcmToken = ''
    permissions = []
    address = null
    birthday = null
    firstname = null
    lastname = null
    job = null
    passport = null
    phone = null
    sex = null
    userCode = null
    findBooking = new ModelFindBooking()
    commonInfo = new CommonInfo()

}

export class ModelFindBooking {
    lat = null
    long = null
    date = null
}
class CommonInfo {
    phoneNumber = null
    contentShare = null
    urlAndroid = null
    urlIOS = null
}

export class ModelParamsAPIChangePass {
    newPassword = null
    oldPassword = null
    userId = null
}