import React from 'react'
import {Alert} from 'react-native'
export default class ModelCallApi {
    params = {}
    paramsTwo = {}
    onSuccess = value => value
    onFail = (err) => {
        setTimeout(() => {
            Alert.alert(err.title,err.message,[{
                text: 'Đồng Ý',
                onPress: () => {
                }
            }])
        }, 200);
    }
}
