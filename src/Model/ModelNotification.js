export default class ModelNotifications {
    notificationDtos = []
    size = 0
    totalPages = 0
    currentPage = 0
    status = false
    hasNewNotification = null
    notificationStatus = null
}

export class ModelNotificationDeitail {
    notificationId = null
    userId = null
    type = null
    title = null
    content = null
    description = null
    longDes = null
    isSeen = null

}