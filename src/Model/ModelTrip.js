import ModelUser from "./ModelUser"

export default class ModelTrips {
    trips_by_bookingId = []
    trips_bookings_available = []
    trip = new ModelTripsDetail()
    isShowDetails = false
    user = new ModelUser()
}

export class ModelTripsDetail {
    tripId= null
    userId= null
    startDate= null
    startEarliestDate= null
    startLatestDate= null
    fromLatitude= null
    fromLongtitude= null
    fromName= null
    fromTel= null
    fromAddress= null
    fromProvinceId= null
    fromDistrictId= null
    fromWardId= null
    fromProvince= null
    fromDistrict= null
    fromWard= null
    fromStreet= null
    toLatitude= null
    toLongtitude= null
    toName= null
    toTel= null
    toAddress= null
    toProvinceId= null
    toDistrictId= null
    toWardId= null
    toProvince= null
    toDistrict= null
    toWard= null
    toStreet= null
    totalSeat= null
    totalSeatUsed= null
    totalSeatAvaiable= null
}

export class ModelParamsAPIDistanceTwoCoordinates {
    lat1 = null
    lat2 = null
    lon1 = null
    lon2 = null
    unit = 'K'
}

export class ModelParamsAPIGetTripsByBookingId {
    bookingId = null
}

export class ModelOnSuccessGetTripsByBookingId {
    isEmpty = false
    firstUserID = null
}


export class ModelParamsAPINewTrip {
    // tripId = 0
    fromAddress = ''
    // fromDistrict = ''
    // fromDistrictId = 0
    fromLatitude = 0
    fromLongtitude = 0
    // fromName = ''
    // fromProvince = ''
    // fromProvinceId = 0
    // fromStreet = ''
    // fromTel = ''
    // fromWard = ''
    // fromWardId = 0
    note = ''
    startDate = ''
    startEarliestDate = ''
    startLatestDate = ''
    toAddress = ''
    // toDistrict = ''
    // toDistrictId = 0
    toLatitude = 0
    toLongtitude = 0
    // toName = ''
    // toProvince = ''
    // toProvinceId = 0
    // toStreet = ''
    // toTel = ''
    // toWard = ''
    // toWardId = 0
    totalSeat = 0
    // totalSeatAvaiable = 0
    // totalSeatUsed = 0
    userId = 0
}