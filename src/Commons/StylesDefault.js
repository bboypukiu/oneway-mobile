import {StyleSheet, PixelRatio} from 'react-native'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
const styles = StyleSheet.create({
    textHeader: {
        fontFamily:'roboto',
        fontWeight: 'bold',
        fontSize: perfectSize(24),
        lineHeight: perfectSize(34),
        color: '#151522'
    },
    labelInput: {
        fontFamily: 'roboto',
        fontWeight: 'bold',
        fontSize: 16
    },
    textInputUserBold: {
        height: 54,
        fontWeight: 'bold',
        borderColor: 'rgba(228, 228, 228, 0.6)',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 20,
        fontFamily: 'roboto',
        fontWeight: 'bold',
        fontSize: 14,
        color: '#151522'
    }
})


export default styles