import actionTypes from './ActionTypes';
import load from './Load';
export default {

  login: (data) => ({ type: actionTypes.SAGA_USER_LOGIN, data }),
  sendNumberPhoneForgotPass: (data) => ({ type: actionTypes.SAGA_USER_SEND_NUMBER_PHONE_FORGOT_PASS, data }),
  sendOTPForgotPass: (data) => ({ type: actionTypes.SAGA_USER_SEND_OTP_FORGOT_PASS, data }),
  newPassForgotPass: (data) => ({ type: actionTypes.SAGA_USER_NEW_PASS_FORGOT_PASS, data }),
  sendChangePass: (data) => ({ type: actionTypes.SAGA_USER_CHANGE_PASS, data }),
  register: (data) => ({ type: actionTypes.SAGA_USER_REGISTER, data }),
  uploadUserAvatar: (data) => ({ type: actionTypes.SAGA_UPLOAD_USER_AVATAR, data }),
  logout:(data)=>({ type: actionTypes.SAGA_USER_LOGOUT, data}),
  setFcmToken:(data)=>({ type: actionTypes.REDUX_USER_SET_FCM_TOKEN, data}),
  updateUserProfile:(data)=>({ type: actionTypes.SAGA_UPDATE_USER_PROFILE, data}),
  setCurrentProFile: (data) => ({type: actionTypes.REDUX_SET_CURRENT_USER_PROFILE, data}),
  setCurrentUserAvatar: (data) => ({type: actionTypes.REDUX_SET_CURRENT_USER_AVATAR, data}),
  setLogout: (data) => ({type: actionTypes.REDUX_USER_LOGOUT, data}),
  load,
  setCurentLocation: (data) => ({ type: actionTypes.REDUX_SET_CURRENT_LOCATION, data }),
  setIsDriver: (value) => ({ type: actionTypes.REDUX_SET_CURRENT_USER_IS_DRIVER, data: value }),
  
  //booking 
  setNewBooking: (data) => ({ type: actionTypes.SAGA_NEW_BOOKING, data }),
  getBookings: (data) => ({ type: actionTypes.SAGA_GET_BOOKINGS, data }),
  addBookings: (data) => ({ type: actionTypes.REDUX_ADD_BOOKINGS_TO_STORE, data }),
  removeItemBooking: (data) => ({ type: actionTypes.SAGA_BOOKING_REMOVE_ITEM, data }),
  //  find user id 
  findByUserId: (data) => ({ type: actionTypes.SAGA_BOOKING_REMOVE_ITEM, data }),
  getDistanceThreePoint: (data) => ({ type: actionTypes.SAGA_GET_DISTANCE_THREE_POINT, data }),
  
  getTripsByBookingId: (data) => ({ type: actionTypes.SAGA_GET_TRIPS_BY_BOOKING_ID, data }),
  addTripsByBookingIdToStore: (data) => ({ type: actionTypes.REDUX_ADD_TRIPS_BY_BOOKING_ID_TO_STORE, data }),
  hideTripDetail: (data) => ({ type: actionTypes.REDUX_HIDE_TRIP_DETAIL, data }),
  showTripDetail: (data) => ({ type: actionTypes.REDUX_SHOW_TRIP_DEITAIL, data }),
  getTripDetail: (data) => ({ type: actionTypes.SAGA_GET_TRIP_DETAIL, data }),
  
  findBookingsByDateAndAddress: (data) => ({ type: actionTypes.SAGA_FIND_BOOKINGS_BY_DATE_AND_ADDRESS, data }),
  cancelFindBookingsByDateAndAddress: (data) => ({type: actionTypes.SAGA_CANCEL_FIND_BOOKINGS_BY_DATE_AND_ADDRESS, data}),

  addTripsBookingsAvailable: (data) => ({ type: actionTypes.REDUX_ADD_TRIPS_BOOKINGS_AVAILABLE, data }),
  
  //Notifi
  getNotification: (data) => ({ type: actionTypes.SAGA_GET_NOTIFICATIONS, data }),
  getNotificationStatus: (data) => ({ type: actionTypes.SAGA_GET_NOTIFICATION_STATUS, data }),
  setNotificationStatus: (data) => ({ type: actionTypes.SAGA_SET_NOTIFICATION_STATUS, data }),
  addNotificationStatusToStore: (data) => ({ type: actionTypes.REDUX_ADD_NOTIFICATION_STATUS_TO_STORE, data }),
  addNotification: (data) => ({ type: actionTypes.REDUX_ADD_NOTIFICATIONS_TO_STORE, data }),
  getNotificationDetail: (data) => ({ type: actionTypes.SAGA_GET_NOTIFICATIONS_DETAIL, data }),

  setTripBookingDefault: (data) => ({ type: actionTypes.REDUX_SET_TRIP_BOOKING_DEFAULT, data }),
  unfollowBookingTrip: (data) => ({ type: actionTypes.SAGA_UNFOLLOW_BOOKING_TRIP, data }),
  
  //API in home screen
  getAllTripBookingByUser: (data) => ({ type: actionTypes.SAGA_GET_ALL_TRIP_BOOKING_BY_USER, data }),

  getTripBookingById: (data) => ({ type: actionTypes.SAGA_GET_BOOKING_TRIP_BY_ID, data }),
  deleteBookingsTripsAvailable: (data) => ({ type: actionTypes.REDUX_DELETE_BOOKINGS_TRIPS_AVAILABLE, data }),
  setFindBookingUser: (data)=> ({ type: actionTypes.REDUX_SET_FIND_BOOOKING_USER, data }),
  
  getBaseInfo: (data)=> ({ type: actionTypes.SAGA_GET_BASE_INFO, data }),
  setBaseInfo: (data)=> ({ type: actionTypes.REDUX_SET_BASE_INFO, data }),
}