
//actions type
export const LOAD = {
    "REDUX_LOAD_LOADING": "REDUX_LOAD_LOADING",
    "REDUX_LOAD_ERROR": "REDUX_LOAD_ERROR",
    "REDUX_LOAD_DONE": "REDUX_LOAD_DONE",
    "REDUX_LOAD_HIDE": "REDUX_LOAD_HIDE"
}

//actions
export default {
    loading: () => ({ type: LOAD.REDUX_LOAD_LOADING }),
    error: () => ({ type: LOAD.REDUX_LOAD_ERROR }),
    done: () => ({ type: LOAD.REDUX_LOAD_DONE }),
    hinde: () => ({ type: LOAD.REDUX_LOAD_HIDE })
}