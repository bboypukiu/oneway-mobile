import {StyleSheet} from 'react-native'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: perfectSize(25),
        backgroundColor: 'white',
    },
    contentContainerStyle: {
        flexGrow: 1
    },
    marginTopHeader:{
        flex: 0.2
    },
    header: {
        // marginTop: '10.6%',
        justifyContent: 'flex-end'
    },
    marginBottomHeader:{
        flex: 0.62 
    },
    body: {
        // marginTop: '32.4%',
    },
    viewTextInputUser: {
        marginBottom: perfectSize(20),
    },
    labelForgotPass: {
        fontFamily: 'roboto',
        fontWeight: 'bold',
        fontSize: perfectSize(13),
        color: '#6979F8',
        paddingTop: perfectSize(20.4),
        lineHeight: perfectSize(20)
    },
    groupBtn: {
        flex: 1,
        borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'flex-end',
        paddingBottom: '14.5%'
    },
    buttonResiter:{
        marginTop: perfectSize(20)
    }
})

export default styles