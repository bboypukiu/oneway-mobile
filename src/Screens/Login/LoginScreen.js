import React from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Alert } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Input from '../../Components/Input'
import BtnLarge from './../../Components/BtnLarge'
import Validator from './../../Utils/Validator'
import { connect } from 'react-redux'
import actions from './../../Actions'

//styles screen
import styles from './styles'
import styles_df from './../../Commons/StylesDefault'

import { StackActions } from '@react-navigation/native';

import ModelCallApi from '../../Model/ModelCallApi'
import { saveAccount } from '../../Helpers/Keychain'
import ModelUser from '../../Model/ModelUser'
import DeviceId, { permissionGetDeviceId } from '../../Utils/DeviceId'
import { getDeviceId } from 'react-native-device-info'

class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            phone: '',
            pass: '',
            deviceid: '',
            //Validate
            errorMgsPhone: '',
            errorMgsPass: '',
        }
        this.inputPassRefs = React.createRef();
    }

    onPressforgotPassword = () => {
        this.props.navigation.navigate('Forgot_password')
    }

    onPressLogin = () => {
        if (this.validateInput()) {
            return
        }

        const { phone, pass, deviceid } = this.state
        const {navigation, dispatch, fcmToken} = this.props

        var data = new ModelCallApi();
        data.params = {
            username: phone,
            password: pass,
            regId: fcmToken,
            device: deviceid
        }
        data.onSuccess = () => {
            navigation.dispatch(
                StackActions.replace('Official_home')
            )
            saveAccount(phone, pass)
        }

        dispatch(actions.login(data))
    }

    validateInput = () => {
        var errorMgsPhone = Validator.CheckNumberPhone(this.state.phone)
        var errorMgsPass = Validator.CheckNormal(this.state.pass, 'Mật Khẩu', 6)
        this.setState({
            errorMgsPhone,
            errorMgsPass
        })
        if (errorMgsPhone != '' || errorMgsPass != '') {
            return true;
        }
        return false;
    }

    onPressRegister = () => {
        this.props.navigation.navigate('Register')
    }

    componentDidMount = async () =>{
        var deviceid = await DeviceId()
        this.setState({
            deviceid
        })
    }

    render() {
        const { errorMgsPhone, errorMgsPass } = this.state
        return (
            <KeyboardAwareScrollView
                style={styles.container}
                resetScrollToCoords={{ x: 0, y: 0 }}
                scrollEnabled={true}
                contentContainerStyle={styles.contentContainerStyle}
            >
                <View style={styles.marginTopHeader} />
                <View style={styles.header}>
                    <Text style={styles_df.textHeader}>Đăng Nhập</Text>
                </View>
                <View style={styles.marginBottomHeader} />
                <View style={styles.body}>
                    <Input
                        style={styles.viewTextInputUser}
                        label='Nhập Số Điện Thoại'
                        keyboardType='numeric'
                        onChangeText={(text) => this.setState({ phone: text, errorMgsPhone: '' })}
                        onSubmitEditing={() => this.inputPassRefs.current.focus()}
                        returnKeyType='next'
                        errText={errorMgsPhone}
                    />
                    <Input
                        label='Mật Khẩu'
                        ref={this.inputPassRefs}
                        secureTextEntry={true}
                        onChangeText={(text) => this.setState({ pass: text, errorMgsPass: '' })}
                        onSubmitEditing={this.onPressLogin}
                        returnKeyType='done'
                        errText={errorMgsPass}
                    />
                    <TouchableOpacity onPress={this.onPressforgotPassword}>
                        <Text style={styles.labelForgotPass}>Quên mật khẩu?</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.groupBtn}>
                    <BtnLarge onPress={this.onPressLogin} title='Đăng Nhập' />
                    <BtnLarge onPress={this.onPressRegister} title='Đăng Ký' style = {styles.buttonResiter}/>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProp = ({user = new ModelUser()}) => ({
    fcmToken: user.fcmToken
})

export default connect(mapStateToProp)(LoginScreen);
