import React from 'react'
import { View, Text, ScrollView, StyleSheet } from 'react-native'

class TermsScreen extends React.Component {
    render() {
        return (
            <View style = {styles.container}>
                <View style={styles.body}>
                    <Text style={styles.text}>- App là một ứng dụng tiện chuyến{"\n"}
                    - Công cụ giúp tài xế lấy thông tin khách tìm xe mà không mất công tìm kiếm, không trung gian, không phí chiết khấu{"\n"}
                    - Liên hệ trực tiếp và thương lượng cụ thể với khách bằng cách bấm nút gọi điện{"\n"}
                    - Quan điểm là thuận mua vừa bán, tôn trọng lẫn nhau{"\n"}
                    - Nghiêm cấm mọi hành vi kích động, đả kích, lăng mạ hoặc tương tự
                    </Text>
                </View>
            </View>
        )
    }
}

export default TermsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    body: {
        margin: 30,
        flex: 1,
        alignItems: 'center',
        borderColor: 'rgba(0,0,0,0.05)',
        borderWidth: 2,
        borderRadius: 5,
    },
    text: {
        fontSize: 16,
        fontWeight: '500',
        paddingHorizontal: 10,
        paddingVertical: 30
    }
})