import React from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, ScrollView, Image, Alert, Platform } from 'react-native'
import { Avatar } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Input from '../../Components/Input'
import BtnLarge from './../../Components/BtnLarge'
import Validator from './../../Utils/Validator'
import { CommonActions } from '@react-navigation/native';

import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
import ImagePicker from 'react-native-image-picker';


import ModelCallApi from '../../Model/ModelCallApi'

import { connect } from 'react-redux'
import actions from './../../Actions'
import ModelUser, { ModelParamsAPIChangePass } from '../../Model/ModelUser'
import { removeAccount, getAccout, saveAccount } from '../../Helpers/Keychain'
import { urlAvatar } from '../../Constants/Api';
import { Value } from 'react-native-reanimated'
class RegisterScreen extends React.Component {
    constructor(props) {
        super(props)
        this.inputRefs = [
            React.createRef(),
            React.createRef(),
            React.createRef(),
            React.createRef(),
        ]
        const { fullname, passport, email } = this.props
        this.state = {
            // login phone
            pass: '',
            newPass: '',
            confirmNewPass: '',
            full_name: fullname,
            isEditName: false,
            passportState: passport,
            isEditPassport: false,
            emailState: email,
            isEditEmail: false,
            // is check terms
            isAcceptTerms: false,
            // validate
            errorMgsEmail: '',
            errorMgsNewName: '',
            errorMgsPassport: '',
            errorMgsPass: '',
            errorMgsNewPass: '',
            errorMgsConfirmNewPass: '',
        }
    }

    onPressSave = async () => {
        var account = await getAccout()

        if (this.validateInput(account.password)) return
        const { newPass, confirmNewPass, pass } = this.state
        const { navigation, dispatch, user_id } = this.props
        var data = new ModelCallApi();
        var params = new ModelParamsAPIChangePass()

        params.newPassword = newPass
        params.oldPassword = pass
        params.userId = user_id

        data.params = params
        data.onSuccess = () => {
            saveAccount(account.username, newPass)
            this.inputRefs.map((ref) => {
                ref.current.clear()
            })
        }

        dispatch(actions.sendChangePass(data))
    }

    validateInput = (oldPassword) => {
        var { newPass, confirmNewPass, pass, errorMgsConfirmNewPass } = this.state
        var errorMgsPass = Validator.CheckNormal(pass, 'Mật khẩu cũ', 6)
        var errorMgsNewPass = Validator.CheckNormal(newPass, 'Mật khẩu mới', 6)
        if (errorMgsPass == '' && oldPassword != pass) {
            errorMgsPass = 'Mật khẩu cũ không đúng!'
        }
        if (newPass != confirmNewPass) {
            errorMgsConfirmNewPass = 'Mật khẩu mới không trùng nhau!'
        } else if (newPass == '') {
            errorMgsConfirmNewPass = 'Vui lòng nhập lại mật khẩu mới!'
        }

        this.setState({
            errorMgsNewPass,
            errorMgsPass,
            errorMgsConfirmNewPass,
        })
        if (errorMgsNewPass != '' || errorMgsPass != '' || errorMgsConfirmNewPass != '') {
            return true;
        }
        return false;
    }

    selectPhotoTapped = () => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };

        ImagePicker.showImagePicker(options, response => {
            //console.log('Response = ', response);

            if (response.didCancel) {
                //console.log('User cancelled photo picker');
            } else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                //console.log('User tapped custom button: ', response.customButton);
            } else {
                const { dispatch } = this.props
                var data = new ModelCallApi();
                var params = new FormData()
                params.append('file', {
                    uri: Platform.OS === 'android' ? response.uri : response.uri.replace('file://', ''),
                    type: response.type,
                    name: 'profile-picture'
                });


                data.params = params
                data.paramsTwo = {
                    uri: response.uri
                }

                data.onSuccess = () => {
                }

                dispatch(actions.uploadUserAvatar(data))
            }
        });
    }

    onPressLogout = () => {
        Alert.alert('Đăng xuất', 'Bạn muốn đăng xuất tài khoản?', [
            {
                text: 'Đồng ý',
                onPress: () => this.handleLogout()
            },
            {
                text: 'Không',
                onPress: () => {
                }
            }
        ])
    }

    handleLogout = () => {
        const { navigation, route, dispatch, token, regId } = this.props
        var data = new ModelCallApi();
        data.params = {
            token,
            regId
        }
        data.onSuccess = () => {
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        { name: 'LoginStack' },
                    ],
                })
            );
            setTimeout(() => {
                dispatch(actions.setLogout({}))
            }, 200);
            removeAccount()
        }

        dispatch(actions.logout(data))
    }

    inputName = () => {
        const { fullname } = this.props
        const { full_name, errorMgsNewName, isEditName } = this.state
        return <Input
            style={[styles.viewInput, { flex: 1, justifyContent: 'center', marginTop: 0 }]}
            styleInputContainer={[styles.styleInputContainer, styles.labelAvartar, !isEditName && { borderWidth: 0 }]}
            value={full_name}
            onFocus={() => this.setState({ isEditName: true })}
            onEndEditing={this.saveName}
            ref={this.inputRefs[3]}
            returnKeyType='done'
            onChangeText={(text) => this.setState({ full_name: text, errorMgsNewName: '' })}
            errText={errorMgsNewName}
        />
    }

    saveName = () => {
        const { fullname, dispatch } = this.props
        const { full_name, isEditName } = this.state
        if (full_name == fullname) {
            this.setState({ isEditName: false })
            return
        }
        var errorMgsNewName = Validator.CheckNormal(full_name, 'Họ và tên', 6)
        if (errorMgsNewName != '') {
            this.setState({ errorMgsNewName })
            return
        }
        this.setState({ isEditName: false })
        var data = new ModelCallApi();
        data.params = {
            fullname: full_name
        }
        dispatch(actions.updateUserProfile(data))
    }

    inputPassport = () => {
        const { passport } = this.props
        const { passportState, errorMgsPassport, isEditPassport } = this.state
        return <Input
            styleLabelContainer={styles.styleLabelContainer}
            label={'CMND/Hộ Chiếu/Căn Cước/GPLX'}
            style={styles.viewInput}
            labelInput = {{fontSize: perfectSize(15),}}
            styleInputContainer={[styles.styleInputContainer, {fontSize: perfectSize(20)}, (passport != '' || passport != null)&&{backgroundColor: 'rgba(105,121,248, 0.5)'}]}
            value={passportState}
            onFocus={() => this.setState({ isEditPassport: true })}
            onEndEditing={this.savePassport}
            returnKeyType='done'
            editable={passport == '' || passport == null}
            onChangeText={(text) => this.setState({ passportState: text, errorMgsPassport: '' })}
            errText={errorMgsPassport}
        />
    }

    savePassport = () => {
        const { passport, dispatch } = this.props
        const { passportState, isEditPassport } = this.state
        if (passport == passportState) {
            this.setState({ isEditPassport: false })
            return
        }
        var errorMgsPassport = Validator.CheckNormal(passportState, 'CMND/ Hộ chiếu/ Căn Cước', 6)
        if (errorMgsPassport != '') {
            this.setState({ errorMgsPassport })
            return
        }
        Alert.alert(null, "BẠN ĐÃ CHẮC CHẮN THÔNG TIN BẠN CẬP NHẬT LÀ CHÍNH XÁC?", [
            {
                text: 'Đồng ý',
                onPress: () => {
                    var data = new ModelCallApi();
                    data.params = {
                        passport: passportState
                    }
                    dispatch(actions.updateUserProfile(data))
                }

            },
            {
                text: 'Không',
                onPress: () => {
                }
            }
        ])
        this.setState({ isEditPassport: false })

    }

    inputEmail = () => {
        const { email } = this.props
        const { emailState, errorMgsEmail, isEditEmail } = this.state
        return <Input
            styleLabelContainer={styles.styleLabelContainer}
            label={'Email'}
            style={styles.viewInput}
            styleInputContainer={styles.styleInputContainer}
            value={emailState}
            onFocus={() => this.setState({ isEditEmail: true })}
            onEndEditing={this.saveEmail}
            returnKeyType='done'
            // editable={email == '' || email == null}
            onChangeText={(text) => this.setState({ emailState: text, errorMgsEmail: '' })}
            errText={errorMgsEmail}
        />
    }

    saveEmail = () => {
        const { email, dispatch } = this.props
        const { emailState, isEditEmail } = this.state
        if (email == emailState) {
            this.setState({ isEditEmail: false })
            return
        }
        var errorMgsEmail = Validator.CheckEmail(emailState)
        if (errorMgsEmail != '') {
            this.setState({ errorMgsEmail })
            return
        }
        Alert.alert(null, "BẠN ĐÃ CHẮC CHẮN THƯ ĐIỆN TỬ LÀ CHÍNH XÁC?", [
            {
                text: 'Đồng ý',
                onPress: () => {
                    var data = new ModelCallApi();
                    data.params = {
                        email: emailState
                    }
                    dispatch(actions.updateUserProfile(data))
                }

            },
            {
                text: 'Không',
                onPress: () => {
                }
            }
        ])
        this.setState({ isEditPassport: false })

    }

    render() {
        const {
            errorMgsPass,
            errorMgsNewPass,
            errorMgsConfirmNewPass,
        } = this.state
        const {
            avatarSource,
            fullname,
            user_phone
        } = this.props
        return (
            <KeyboardAwareScrollView
                style={styles.container}
                resetScrollToCoords={{ x: 0, y: 0 }}
                scrollEnabled={true}
                contentContainerStyle={styles.contentContainerStyle}
            >
                <View style={styles.viewAvatar}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: perfectSize(15) }}>
                        <Avatar
                            rounded title="AB"
                            containerStyle={styles.avatarContainer}
                            size={perfectSize(86)}
                            titleStyle={styles.avatarTitle}
                            source={{ uri: `${urlAvatar}${avatarSource}` }}
                        />
                        {this.inputName()}
                    </View>
                    <TouchableOpacity onPress={this.selectPhotoTapped} style={styles.viewUploadAvatar}>
                        <Image style={styles.editImage} source={require('./../../../assets/icon_design/pencil.png')} />
                        <Text style={styles.labelChangeAvartar}>{`  Đổi ảnh đại diện`}</Text>
                    </TouchableOpacity>
                </View>
                {this.inputPassport()}
                <Input
                    styleLabelContainer={styles.styleLabelContainer}
                    style={styles.viewInput}
                    styleInputContainer={styles.styleInputContainer}
                    label='Số điện thoại'
                    editable={false}
                    defaultValue={user_phone}
                    returnKeyType='next'
                />
                {this.inputEmail()}
                <Input
                    styleLabelContainer={styles.styleLabelContainer}
                    style={styles.viewInput}
                    label='Mật khẩu cũ'
                    styleInputContainer={styles.styleInputContainer}
                    ref={this.inputRefs[0]}
                    onSubmitEditing={() => this.inputRefs[1].current.focus()}
                    returnKeyType='next'
                    secureTextEntry={true}
                    onChangeText={(pass) => this.setState({ pass, errorMgsPass: '' })}
                    errText={errorMgsPass}
                />
                <Input
                    styleLabelContainer={styles.styleLabelContainer}
                    style={styles.viewInput}
                    styleInputContainer={styles.styleInputContainer}
                    label='Mật khẩu mới'
                    ref={this.inputRefs[1]}
                    onSubmitEditing={() => this.inputRefs[2].current.focus()}
                    returnKeyType='next'
                    secureTextEntry={true}
                    onChangeText={(newPass) => this.setState({ newPass, errorMgsNewPass: '' })}
                    errText={errorMgsNewPass}
                />
                <Input
                    styleLabelContainer={styles.styleLabelContainer}
                    style={styles.viewInput}
                    label='Nhập lại mật khẩu mới'
                    styleInputContainer={styles.styleInputContainer}
                    ref={this.inputRefs[2]}
                    returnKeyType='done'
                    secureTextEntry={true}
                    onSubmitEditing={this.onPressSave}
                    onChangeText={(confirmNewPass) => this.setState({ confirmNewPass, errorMgsConfirmNewPass: '' })}
                    errText={errorMgsConfirmNewPass}
                />
                <View style={styles.btnView}>
                    <BtnLarge onPress={this.onPressLogout} title='Đăng xuất' />
                    <BtnLarge onPress={this.onPressSave} title='Đổi mật khẩu' style={styles.buttonSave} />
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProp = ({ user = new ModelUser() }) => ({
    avatarSource: user.avatar,
    fullname: user.fullname,
    user_phone: user.phone,
    user_id: user.userId,
    passport: user.passport,
    token: user.token,
    regId: user.fcmToken,
    email: user.email
})
export default connect(mapStateToProp)(RegisterScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: perfectSize(25),
        backgroundColor: 'white',
    },
    contentContainerStyle: {
        flexGrow: 1,
    },
    viewAvatar: {
        borderBottomColor: 'rgba(241, 243, 246, 0.6)',
        borderBottomWidth: 1
    },
    marginTopView: {
        flex: 1.2
    },
    viewInput: {
        marginTop: perfectSize(5),
        paddingLeft: perfectSize(15)
    },
    styleLabelContainer: {
        marginBottom: perfectSize(5),
    },
    inputName: {
        flex: 1,
        justifyContent: 'center',
        marginTop: 0
    },
    textInput: {
        fontSize: 14,
        fontFamily: 'roboto',
        fontWeight: 'bold',
        borderColor: 'rgba(0,0,0,0.05)',
        borderWidth: 2,
        borderRadius: 5,
        padding: 17,
        marginVertical: 10
    },
    btnTerms: {
        flexDirection: 'row',
        height: perfectSize(50),
        alignItems: "center",
    },
    labelTerms: {
        color: '#999999',
        height: '100%',
        lineHeight: perfectSize(50)
    },
    labelTerms1: {
        color: 'rgba(0,140,250,1)',
        lineHeight: perfectSize(50),
        height: '100%',
    },
    btnView: {
        flex: 1,

        justifyContent: 'flex-end',
        marginTop: perfectSize(10)
    },
    avatarContainer: {
        backgroundColor: '#CDD2FD',
        borderColor: '#9BA6FA',
        borderWidth: 2,
    },
    editImage: {
        height: perfectSize(18),
        width: perfectSize(18),
    },
    avatarTitle: {
        fontSize: perfectSize(28),
        fontWeight: '500',
        fontFamily: 'roboto'
    },
    viewLabelAvatar: {
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center'
    },
    labelAvartar: {
        fontSize: perfectSize(15),
        lineHeight: perfectSize(20),
        // height: perfectSize(50)
        // fontFamily: 'roboto',
    },
    labelChangeAvartar: {
        fontSize: perfectSize(15),
        lineHeight: perfectSize(20),
        height: perfectSize(20)
        // fontFamily: 'roboto',
    },
    viewUploadAvatar: {
        flexDirection: 'row',
        paddingBottom: perfectSize(16),
        paddingTop: perfectSize(8)
    },
    buttonSave: {
        marginTop: perfectSize(20)
    },
    styleInputContainer: {
        height: perfectSize(38)
    }
})
