import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Dimensions, Image, Linking, Alert,ScrollView  } from 'react-native'
import Modal from 'react-native-modal'

import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import ModelBooking, { ModelBookingDetail } from '../../Model/ModelBooking'
import { connect } from 'react-redux';
import actions from '../../Actions'
import { Avatar } from 'react-native-elements'
import ModelUser from '../../Model/ModelUser'
import BtnLarge from './../../Components/BtnLarge'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
const windowWidth = Dimensions.get('window').width;
import moment from 'moment'
import Validator from '../../Utils/Validator'
import ModelTrips from '../../Model/ModelTrip'
import ModelCallApi from '../../Model/ModelCallApi'
import { urlAvatar } from '../../Constants/Api';
import styleHeader from '../../Navigations/styles'

class BookingDetail extends React.Component {
    constructor(props) {
        super(props)
        const { navigation, route, isDriver } = this.props
        let showIconCar = !isDriver
        if (route.params?.showIconCar !=undefined) {
            showIconCar = route.params?.showIconCar
        }

        navigation.setOptions({
            headerTitle: props => <Text style={styleHeader.titleLogin}>{!showIconCar ? 'Thông Tin Khách Hàng' : 'Thông Tin Chuyến Xe'}</Text>,
        })
    }

    gotoHomeMap = () => {
        const { dispatch, navigation } = this.props
        // dispatch(actions.hideTripDetail(false));
        navigation.navigate('HomeTab')
    }

    componentDidMount() { }

    carRender = () => {
        return (
            <Image
                source={require('./../../../assets/icon_design/car.png')}
                style={{ width: 40, height: 40, transform: [{ rotate: `${Math.random(10, 360)}deg` }] }}
                resizeMode='contain'
            />
        )
    }

    userRender = () => {
        const { isDriver, route } = this.props
        let showIconCar = !isDriver
        if (route.params?.showIconCar !=undefined) {
            showIconCar = route.params?.showIconCar
        }
        if (showIconCar) {
            return (
                <Image
                    source={require('./../../../assets/icon_design/car_detail.png')}
                    style={{ width: perfectSize(75), height: perfectSize(35), position: 'relative', right: 10, bottom: perfectSize(5) }}
                    resizeMode='contain'
                />
            )
        }
        return (
            <Image
                source={require('./../../../assets/icon_design/family.png')}
                style={{ width: perfectSize(61), height: perfectSize(32), position: 'relative', right: 0, bottom: perfectSize(5) }}
            // resizeMode='contain'
            />
        )
    }

    onPressCall = () => {
        const { currentUserId, userBooking } = this.props
        if (currentUserId == userBooking.userId) {
            Alert.alert('Bạn Không Thể Gọi Được!', 'Đây Là Booking Của Chính Bạn Tạo', [
                { text: 'Chấp Nhận', onPress: () => { } },
            ])
            return
        }
        var numberPhone = this.getNumberPhone()
        if (numberPhone == null) {
            Alert.alert(`Rất Tiếc! Không Có Số Điện Thoại Để Liên Hệ!`, null, [
                { text: 'Chấp Nhận', onPress: () => { } },
            ])
        }
        Linking.openURL(`tel:${numberPhone}`)
    }

    getNumberPhone = () => {
        const { userBooking } = this.props
        // var mgsPhone = Validator.CheckNumberPhone(userBooking.phone)
        // var mgsPhoneByUsername = Validator.CheckNumberPhone(userBooking.username)
        // if(mgsPhone != '' && mgsPhoneByUsername != ''){
        //     return null
        // }

        // return userBooking.phone || userBooking.username
        return userBooking.phone
    }
    unfollow = () => {
        const { isDriver, trip, dispatch, navigation, currentUserId, userBooking, route } = this.props
        if (currentUserId == userBooking.userId) {
            Alert.alert('Bạn Không Thể Bỏ Theo Dõi!', 'Đây Là Booking Của Chính Bạn Tạo', [
                { text: 'Chấp Nhận', onPress: () => { } },
            ])
            return
        }
        Alert.alert(null, 'Bạn Muốn Bỏ Theo Dõi Booking Này?', [
            {
                text: 'Chấp Nhận', onPress: () => {
                    var data = new ModelCallApi()
                    //console.log(trip)
                    if (trip?.tripId) {
                        data.params = {
                            tripId: trip.tripId
                        }
                    }
                    if (trip?.bookingId) {
                        data.params = {
                            bookingId: trip.bookingId
                        }
                    }

                    data.onSuccess = () => {
                        if (route.params?.showIconCar) {
                            navigation.goBack()
                        } else {
                            this.gotoHomeMap()
                        }
                    }

                    dispatch(actions.unfollowBookingTrip(data))
                }
            },
            { text: 'Thoát', onPress: () => { } },
        ])
    }

    render() {
        const { isDriver, trip, userBooking ,route } = this.props
        let showIconCar = !isDriver
        if (route.params?.showIconCar !=undefined) {
            showIconCar = route.params?.showIconCar
        }
        var numberPhone = this.getNumberPhone()
        return (
            <>

                <View style={{ flex: 1, backgroundColor: 'white', paddingHorizontal: "auto" }}>

                    <ScrollView style={styles.container}>

                        <View style={{ borderBottomWidth: 1, paddingBottom: perfectSize(20), flexDirection: 'column', borderBottomColor: 'rgba(61, 99, 157, 0.7)' }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: perfectSize(25) }}>
                                <Avatar
                                    rounded title="Avatar"
                                    containerStyle={styles.avatarContainer}
                                    size={perfectSize(120)}
                                    titleStyle={styles.avatarTitle}
                                    source={{ uri: `${urlAvatar}${userBooking.avatar}` }}
                                />
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Text style={{ fontSize: perfectSize(20), lineHeight: perfectSize(26) }}>{userBooking.fullname}</Text>
                                    {userBooking.birthday == null ? null : <Text style={{ fontSize: perfectSize(11), lineHeight: perfectSize(13) }}>{moment().diff(moment(userBooking.birthday), 'years')}</Text>}
                                </View>
                            </View>

                        </View>

                        <View>
                            <View style={{ paddingTop: perfectSize(25), paddingLeft: perfectSize(25) }}>
                                <View>
                                    <View style={{ flexDirection: "row", paddingTop: perfectSize(5) }}>
                                        <View>
                                            {this.userRender()}
                                            {trip.totalSeat == 0 || trip.totalSeat == null ? null : <Text style={styles.textInfo}>
                                                {showIconCar ? `Số ghế trống:` : `Số người:`}
                                            </Text>}
                                        </View>
                                        <View style={{ marginHorizontal: perfectSize(15) }}>{trip.totalSeat == 0 || trip.totalSeat == null ? null : <Text style={styles.textInfo, { fontSize: perfectSize(60) }}>{trip.totalSeat}</Text>}</View>
                                    </View>
                                </View>
                            </View>
                            <View style={{ justifyContent: 'center', padding: perfectSize(25) }}>
                                <View style={{ flexDirection: "row", }}>
                                    <View style={{
                                        height: perfectSize(15),
                                        width: perfectSize(15),
                                        marginRight: perfectSize(12),
                                        marginTop: perfectSize(18),
                                        marginLeft: perfectSize(5),
                                        alignItems: "stretch",
                                        borderRadius: 10,
                                        backgroundColor: '#16E469'
                                    }} /><Text style={styles.textInfo}>{`Điểm Đi: ${trip.fromAddress}`}</Text>
                                </View>
                                <View style={{ flexDirection: "row", }}>
                                    <View style={{
                                        height: perfectSize(15),
                                        width: perfectSize(15),
                                        marginRight: perfectSize(12),
                                        marginTop: perfectSize(18),
                                        marginLeft: perfectSize(5),
                                        alignItems: "stretch",
                                        borderRadius: 10,
                                        backgroundColor: '#EA1313'
                                    }} /><Text style={styles.textInfo}>{`Điểm Đến: ${trip.toAddress}`}</Text>
                                </View>
                                <View style={{ flexDirection: "row" }}>
                                    <Image
                                        source={require('./../../../assets/icon_design/calendar_black.png')}
                                        style={{ marginRight: perfectSize(7), marginTop: perfectSize(10), }}
                                    /><Text style={styles.textInfo}>{`Ngày Khởi Hành: ${trip.startDate == null || trip.startDate == "" ? '' : moment(trip.startDate).format('DD-MM-YYYY')} ${trip.startEarliestDate == null || trip.startEarliestDate == "" ? '' : moment(trip.startEarliestDate).format('HH:mm')}`}</Text>
                                </View>
                                {/*{numberPhone == null ? null : <View style={{ flexDirection: 'row' }}><Text style={styles.textInfo}>Hotline: </Text><Text style={[styles.textInfo, { color: 'rgba(0,140,250,1)' }]}>{numberPhone}</Text></View>}*/}
                                {trip.note == '' || trip.note == null ? null :
                                    (<View style={{ flexDirection: "row" }}>
                                        <Image
                                            source={require('./../../../assets/icon_design/note.png')}
                                            style={{
                                                marginTop: perfectSize(10),
                                                marginRight: perfectSize(5),
                                                height: perfectSize(22),
                                                width: perfectSize(22),
                                            }}
                                        /><Text style={styles.textInfo}>{`Ghi Chú: ${trip.note}`}</Text>
                                    </View>)}
                            </View>
                            <View style={{ flexDirection: 'row', paddingHorizontal: perfectSize(10), marginHorizontal: perfectSize(30), justifyContent: 'space-between'}}>
                                <BtnLarge title='Bỏ Theo Dõi' style={[styles.btn, { backgroundColor: '#FF647C', marginHorizontal: perfectSize(30) }]} onPress={this.unfollow} />
                                <BtnLarge title='Gọi Điện' style={styles.btn} onPress={this.onPressCall} />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </>
        )
    }
}

const mapStateToProps = ({ user = new ModelUser(), trips = new ModelTrips() }) => ({
    trip: trips.trip,
    isShowDetails: trips.isShowDetails,
    userBooking: trips.user,
    isDriver: user.isDriver,
    currentUserId: user.userId
})

export default connect(mapStateToProps)(BookingDetail)
const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: perfectSize(311),
        backgroundColor: 'white',
        borderRadius: 30,
        paddingHorizontal: perfectSize(10),
        paddingVertical: perfectSize(20),
    },
    view: {
        justifyContent: 'flex-end',
        marginBottom: 0,
        marginHorizontal: perfectSize(10),
    },
    avatarContainer: {
        backgroundColor: '#CDD2FD',
        borderColor: '#9BA6FA',
        borderWidth: 2,
        marginRight: perfectSize(30)
    },
    avatarTitle: {
        fontSize: perfectSize(28),
        fontWeight: '500',
        fontFamily: 'roboto'
    },
    title: {
        fontSize: perfectSize(24),
        marginVertical: 15,
        fontWeight: '200',
        textAlign: 'center'
    },
    btn: {
        height: perfectSize(50),
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15,
        width: windowWidth * 0.3
    },
    btnClose: {
        position: 'absolute',
        padding: 20,
        right: 0
    },
    textInfo: {
        fontSize: perfectSize(20),
        lineHeight: perfectSize(40)
    }
})
