import React from 'react'
import { View, Text, TouchableOpacity, FlatList, StyleSheet, Image, Alert, Linking, RefreshControl, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import ModelBooking, { ModelNewBooking, ModelBookingDetail, ModelParamsAPIGetBooking, ModelListBookings } from '../../Model/ModelBooking'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
import moment from 'moment'
import actions from '../../Actions'
import ModalMessage from '../../Components/ModalMsg'

import ModelCallApi from '../../Model/ModelCallApi'
import { AddButton } from '../../Navigations/HeaderElements'
import { ModelParamsAPIDistanceTwoCoordinates, ModelParamsAPIGetTripsByBookingId, ModelOnSuccessGetTripsByBookingId } from '../../Model/ModelTrip'
import ModelUser from '../../Model/ModelUser'

class BookingScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            bookingIdRemove: 0,
            loading: false,
            isRefreshing: false,
        }
        this.props.navigation.setOptions({
            headerRight: props => AddButton(this.props.navigation, {
                getBookings: () => {
                    this.callApiGetBookings()
                }
            })
        })
    }

    alertRemoveOk = () => {
        const { dispatch, isDriver } = this.props
        const { bookingIdRemove } = this.state
        var data = new ModelCallApi()
        if(isDriver){
            data.params = {
                tripId: bookingIdRemove
            }
        }else{
            data.params = {
                bookingId: bookingIdRemove
            }
        }
        data.onSuccess = () => {
            this.callApiGetBookings()
        }
        setTimeout(() => {
            dispatch(actions.removeItemBooking(data))
        }, 300);
        this.setState({
            isModalVisible: false
        })
    }

    alertRemoveCancel = () => {
        this.setState({
            isModalVisible: false
        })
    }

    componentDidMount() {
        this.callApiGetBookings(false, false)
    }

    callApiGetBookings = (isLoadMore = false, isRefreshing = false) => {
        const { dispatch, list_bookings } = this.props
        var data = new ModelCallApi()
        var params = new ModelParamsAPIGetBooking()

        if (isLoadMore && (list_bookings.totalPages == list_bookings.currentPage + 1)) {
            return;
        }
        if (!isLoadMore) {
            params.page = 0
            params.size = 20
            params.paged = true
            params.isLoadFirst = !isLoadMore && !isRefreshing
        } else {
            var page = list_bookings.currentPage
            params.page = page + 1
            params.size = list_bookings.size
            params.paged = true
            params.isLoadFirst = false
        }

        data.params = params
        data.onSuccess = (isEmpty) => {
            if (!isLoadMore) {
                const { isRefreshing } = this.state
                if (isRefreshing) {
                    this.setState({
                        isRefreshing: false
                    })
                }
            }
        }
        dispatch(actions.getBookings(data))
    }

    onRemove = (index) => {
        const { list_bookings: {bookingDtos}, isDriver} = this.props
        this.setState({
            bookingIdRemove: isDriver ? bookingDtos[index]?.tripId : bookingDtos[index]?.bookingId,
            isModalVisible: true
        })
    }

    showTripsDetail = (index) => {
        const { dispatch, list_bookings: {bookingDtos}, isDriver, navigation } = this.props
        var data = new ModelCallApi()
        var params = new ModelParamsAPIGetTripsByBookingId()

        params.bookingId = bookingDtos[index].bookingId
        data.params = params
        if(isDriver){
            data.params = {
                tripId: bookingDtos[index].tripId
            }
        }
        data.onSuccess = () => {
            navigation.navigate('Result_screen')
        }
        dispatch(actions.getTripsByBookingId(data))
    }

    onRefresh = () => {
        this.setState({
            isRefreshing: true
        })
        this.callApiGetBookings(false, true)
    }

    renderFooter = () => {
        const { dispatch, list_bookings } = this.props
        if (list_bookings.totalPages == list_bookings.currentPage + 1) return null;
        return (
            <ActivityIndicator
                style={{ color: '#000' }}
            />
        );
    };

    render() {

        const { list_bookings } = this.props
        const { isModalVisible } = this.state
        return (
            list_bookings.bookingDtos.length > 0 ?
                <View style={styles.container}>
                    <ModalMessage
                        title='Bạn Có Muốn Xóa Booking Không?'
                        titleOk='Xác Nhận'
                        titleCancel='Huỷ'
                        isVisible={isModalVisible}
                        onPressOK={this.alertRemoveOk}
                        onPressCancel={this.alertRemoveCancel}
                    />
                    <FlatList
                        style={styles.flatlist}
                        data={list_bookings.bookingDtos}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isRefreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                        renderItem={({ item, index }) => (
                            <ElementBooking
                                item={item}
                                index={index}
                                // selected={!!selected.get(item.id)}
                                onRemove={this.onRemove}
                                showDetail={this.showTripsDetail}
                            />
                        )}
                        keyExtractor={(item) => item.id}
                        onEndReachedThreshold={0.4}
                        onEndReached={({ distanceFromEnd }) => {
                            if (distanceFromEnd < 0) return;
                            this.callApiGetBookings(true)
                        }}
                        ListFooterComponent={this.renderFooter}
                    />
                </View>
                :
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
                    <Text>
                        Bạn chưa có chuyến đi nào vui lòng thêm mới!
                    </Text>
                </View>
        )
    }
}

const mapStateToProp = ({ booking = new ModelBooking(), map, user = new ModelUser() }) => ({
    list_bookings: booking.list_bookings,
    map: map,
    isDriver: user.isDriver,
})

export default connect(mapStateToProp)(BookingScreen);


const ElementBooking = ({ item = new ModelBookingDetail(), index, onRemove, showDetail, isDriver }) => {
    console.log("----"+item.tripId);
            let txt =`Số người: `;
        if(!item.tripId){
            txt= `Số người:`;
        }else{
            txt= `Số Chỗ Trống: `;
        }

    return (
        <View style={styles.elementContainer}>
            <View style={styles.viewRow}>
                {item.startDate == "" || item.startDate == null ? null : <Text style={styles.text}>{moment(item.startDate).format('DD-MM-YYYY')}</Text>}
                {item.startEarliestDate == "" || item.startEarliestDate == null ? null : <Text style={styles.text}>  {moment(item.startEarliestDate).format('HH:mm')}</Text>}
            </View>
            {(item.totalSeat == 0 || item.totalSeat == null) ? null : <View style={styles.viewRow}>
                <Text style={[styles.text, styles.seaterMarginLeft]}>{txt}</Text>
                <Text style={styles.text}>{item.totalSeat}</Text>
                {isDriver?<Text style={styles.text}> chỗ</Text>: null}
            </View>}
            <View style={styles.viewRow}>
                <View style={styles.pointGreen} />
                <Text numberOfLines={2} style={[styles.text, { flex: 1 }]}>{item.fromAddress}</Text>
            </View>
            <View style={styles.viewRow}>
                <View style={styles.pointRed} />
                <Text numberOfLines={2} style={[styles.text, { flex: 1 }]}>{item.toAddress}</Text>
            </View>
            <TouchableOpacity style={styles.btnDetail} onPress={() => showDetail(index)}>
                <Text style={styles.labelShowDetail}>Xem chi tiết</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elementBtnRemove} onPress={() => onRemove(index)}>
                <Image source={require('./../../../assets/icon_design/recycle.png')} style={styles.elementImgRemove} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    flatlist: {
        flex: 1,
        paddingHorizontal: perfectSize(30)
    },
    text: {
        fontFamily: 'roboto',
        fontWeight: 'bold',
        fontSize: perfectSize(15),
    },
    viewRow: {
        flexDirection: 'row',
        marginVertical: perfectSize(3),
    },
    btnDetail: {
        alignItems: 'flex-end'
    },
    elementContainer: {
        borderBottomWidth: 1,
        flex: 1,
        marginTop: perfectSize(10)
    },
    elementBtnRemove: {
        position: 'absolute',
        right: 0,
        top: 0,
    },
    elementImgRemove: {
        height: perfectSize(22),
        width: perfectSize(22),
        margin: perfectSize(5)
    },
    pointRed: {
        height: perfectSize(14),
        width: perfectSize(14),
        marginRight: perfectSize(16),
        borderRadius: perfectSize(14),
        backgroundColor: '#EA1313'
    },
    pointGreen: {
        height: perfectSize(14),
        width: perfectSize(14),
        marginRight: perfectSize(16),
        borderRadius: perfectSize(14),
        backgroundColor: '#16E469'
    },
    seaterMarginLeft: {
        marginLeft: perfectSize(30)
    },
    labelShowDetail: {
        color: '#6979F8',
        paddingVertical: perfectSize(5)
    }
})
