import React from 'react'
import { View, Text, StyleSheet, TextInput, ScrollView, Image, TouchableWithoutFeedback, Alert, Platform, Dimensions, TouchableOpacity } from 'react-native'
import { Avatar } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Input from '../../Components/Input'
import BtnLarge from '../../Components/BtnLarge'
import Validator from '../../Utils/Validator'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Toast from './../../Utils/Toast';
import { StackActions } from '@react-navigation/native';
import ModalInputGooglePlaces from './../../Components/ModalInputGooglePlaces'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
// import { TouchableOpacity } from 'react-native-gesture-handler'


import ModelCallApi from '../../Model/ModelCallApi'

import { connect } from 'react-redux'
import actions from '../../Actions'
import ModelUser from '../../Model/ModelUser'
import moment from 'moment'
import ModelBooking, { ModelParamsAPINewBooking } from '../../Model/ModelBooking'
import ModelPlaceMap from '../../Model/ModelPlaceMap'
import { ModelParamsAPINewTrip } from '../../Model/ModelTrip'
import { ranDomeMaker, getRegionForCoordinates, mapCoordinateToAddress } from '../../Utils/Map'
import MapViewDirections from 'react-native-maps-directions';
import { GOOGLE_PLACES_API_KEY } from '../../Constants/Api'

import styleHeader from './../../Navigations/styles'
import validator from 'validator'

const sizeWindow = Dimensions.get('window')

class NewBookingScreen extends React.Component {
    constructor(props) {
        super(props)
        this.inputRefs = [
            React.createRef(),
            React.createRef(),
            React.createRef(),
            React.createRef(),
            React.createRef(),
            React.createRef(),
            React.createRef(),
        ]
        this.scrollRef = React.createRef()
        this.map_ref = React.createRef()
        const { currentLocation } = this.props
        var location_default = ranDomeMaker(currentLocation)
        this.state = {
            // // login phone
            starting_point: '',
            fromLat: '',
            fromLng: '',
            destination: '',
            toLat: '',
            toLng: '',
            departure_time: '',
            earliest_time: '',
            latest_time: '',
            seater: '',
            note: '',
            dateTime: new Date(),
            modeDateTime: 'date',
            showDateTime: null,

            isInputStartingPoint: false,
            isInputDestination: false,

            errorMgsStartPoint: '',
            errorMgsDestination: '',
            errorMgsDeparture: '',
            errorMgsEarliestTime: '',
            errorMgsLatestTime: '',
            errorMgsSeater: '',

            locationStart: location_default[0],
            locationEnd: location_default[1],
            region: getRegionForCoordinates([currentLocation, location_default[0], location_default[1]]),
            isDirection: false,
            infoTag: null,

            regionChange: {},

            isSelectAddress: 'StartingPoint',
        }
    }

    handleDatePicker = (date) => {
        const { showDateTime, dateTime } = this.state
        if (showDateTime == 'departure_time') {
            var selectedDate = moment(date).valueOf()
            var now = moment(dateTime).valueOf()
            if (moment(selectedDate).format('YYYY-MM-DD') < moment(now).format('YYYY-MM-DD')) {
                Alert.alert('Sai Ngày chọn', 'Ngày Được Chọn Không Nhỏ Hơn Ngày Hiện Tại!', [
                    {
                        text: 'Chọn Lại', onPress: () => {

                        }
                    },
                ])
                return;
            }
            this.setState({
                showDateTime: null,
                modeDateTime: 'time',
                dateTime: new Date(),
                departure_time: moment(date).valueOf(),
                errorMgsDeparture: ''
            })
        } else if (showDateTime == 'earliest_time') {
            const { latest_time } = this.state
            if (latest_time != '') {
                if (moment(date).valueOf() > latest_time) {
                    Alert.alert('Sai Giờ Đón Sớm Nhất', 'Giờ Đón Sớm Nhất Phải Nhỏ Hơn Giờ Đón Muộn Nhất!', [
                        {
                            text: 'Chọn Lại', onPress: () => {

                            }
                        },
                    ])
                    return;
                }
            }
            this.setState({
                showDateTime: null,
                modeDateTime: 'time',
                dateTime: new Date(),
                earliest_time: moment(date).valueOf(),
                errorMgsEarliestTime: ''
            })
        } else {
            const { earliest_time } = this.state
            var selectedDate = moment(date).valueOf()
            var now = moment(earliest_time).valueOf()
            if (selectedDate < earliest_time) {
                Alert.alert('Sai Giờ Đón Muộn Nhất', 'Giờ Đón Muộn Nhất Phải Lớn Hơn Giờ Đón Sớm!', [
                    {
                        text: 'Chọn Lại', onPress: () => {

                        }
                    },
                ])
                return;
            }
            this.setState({
                showDateTime: null,
                latest_time: moment(date).valueOf(),
                errorMgsLatestTime: ''
            })
        }
    }

    onPressAdd = () => {
        const {
            starting_point,
            fromLat,
            fromLng,
            destination,
            toLat,
            toLng,
            departure_time,
            earliest_time,
            latest_time,
            seater,
            note
        } = this.state
        var errorMgsDeparture = Validator.CheckNormal(departure_time, 'Ngày Khởi Hành', 6)
        var errorMgsEarliestTime = Validator.CheckTime(earliest_time)
        var errorMgsLatestTime = Validator.CheckTime(latest_time)
        var errorMgsSeater =  !validator.isNumeric(seater, {no_symbols: true}) || seater <= 0
        if (errorMgsDeparture != '' || errorMgsSeater != '' || errorMgsLatestTime != '' || errorMgsEarliestTime != '') {
            this.setState({
                errorMgsDeparture,
                errorMgsSeater,
                errorMgsLatestTime,
                errorMgsEarliestTime
            })
            return;
        }


        const { dispatch, navigation, route, list_bookings, userId, isDriver } = this.props
        var data = new ModelCallApi()
        var _startDate = moment(departure_time).format('YYYY-MM-DD HH:mm:ss')
        var _startEarliestDate = earliest_time == '' ? _startDate : moment(moment(departure_time).format('YYYY-MM-DD') + " " + moment(earliest_time).format('HH:mm:ss')).format('YYYY-MM-DD HH:mm:ss')
        var _startLatestDate = latest_time == '' ? _startDate : moment(moment(departure_time).format('YYYY-MM-DD') + " " +moment(latest_time).format('HH:mm:ss')).format('YYYY-MM-DD HH:mm:ss')

        var bookingDto = new ModelParamsAPINewBooking()
        bookingDto.fromAddress = starting_point
        bookingDto.fromLatitude = fromLat
        bookingDto.fromLongtitude = fromLng
        bookingDto.toAddress = destination
        bookingDto.toLatitude = toLat
        bookingDto.toLongtitude = toLng
        bookingDto.startDate = _startDate
        bookingDto.startEarliestDate = _startEarliestDate
        bookingDto.startLatestDate = _startLatestDate
        bookingDto.totalSeat = seater
        bookingDto.note = note,
        bookingDto.userId = userId

        var tripDto = new ModelParamsAPINewTrip()
        tripDto.fromAddress = starting_point
        tripDto.fromLatitude = fromLat
        tripDto.fromLongtitude = fromLng
        tripDto.toAddress = destination
        tripDto.toLatitude = toLat
        tripDto.toLongtitude = toLng
        tripDto.startDate = _startDate
        tripDto.startEarliestDate = _startEarliestDate
        tripDto.startLatestDate = _startLatestDate
        tripDto.totalSeat = seater
        tripDto.note = note,
        tripDto.userId = userId

        if (isDriver) {
            data.params = {
                tripDto
            }
        } else {
            data.params = {
                bookingDto
            }
        }

        data.onSuccess = () => {
            route?.params?.getBookings && route.params.getBookings()
            //console.log('route', route)
            //console.log('navigation', navigation)
            navigation.goBack()
        }
        dispatch(actions.setNewBooking(data))
    }

    onHandleTime = (val) => {
        this.setState({
            showDateTime: val,
            modeDateTime: val == 'departure_time' ? 'date' : 'time',
            dateTime: new Date()
        })
    }

    hideDatePicker = () => {
        this.setState({
            showDateTime: null,
        })
    }

    hanldeInputAddress = (response = new ModelPlaceMap()) => {
        const { isInputDestination, isInputStartingPoint, locationStart, locationEnd } = this.state
        const { currentLocation } = this.props
        //console.log(response)
        var location = {
            longitude: response.location.lng,
            latitude: response.location.lat
        }
        if (isInputStartingPoint) {
            this.setState({
                starting_point: response.formatted_address,
                fromLat: response.location.lat,
                fromLng: response.location.lng,
                isInputStartingPoint: false,
                locationStart: location,
                errorMgsStartPoint: ''
            }, ()=> this.map_ref.current.animateToCoordinate(location))
        }
        if (isInputDestination) {
            this.setState({
                destination: response.formatted_address,
                toLat: response.location.lat,
                toLng: response.location.lng,
                isInputDestination: false,
                locationEnd: location,
                errorMgsDestination: ''
            }, ()=> this.map_ref.current.animateToCoordinate(location))
        }
    }

    handleAddAddress = () => {
        const { starting_point, destination } = this.state

        var errorMgsStartPoint = Validator.CheckNormal(starting_point, 'Điểm Đi', 6)
        var errorMgsDestination = Validator.CheckNormal(destination, 'Điểm Đến', 6)
        if (errorMgsDestination != '' || errorMgsStartPoint != '') {
            Toast(`Vui Lòng Nhấn Giữ Và Di Chuyển Cờ Xanh Hoặc Cờ Đỏ Để Chọn Địa Điểm!`, 3000, 0)
            this.setState({
                errorMgsStartPoint,
                errorMgsDestination,
            })
            return;
        }

        this.setState({
            isDirection: true,
            isSelectAddress: null
        },
            () => {
                this.scrollRef.current.scrollTo({ x: sizeWindow.width, animated: true })
                // this.props.navigation.setOptions({
                //     headerTitle: props => <Text style={styleHeader.titleLogin}>{'Thêm booking mới'}</Text>
                // })
            }
        )
    }

    onDragEndMarker = async (coordinate) => {
        const { isSelectAddress, isInputDestination, isInputStartingPoint, isDirection } = this.state
        if(isInputDestination || isInputStartingPoint || isDirection){
            return
        }
        var address = await mapCoordinateToAddress(coordinate)
        if (address) {
            if (isSelectAddress == 'StartingPoint') {
                this.setState({
                    locationStart: coordinate,
                    starting_point: address,
                    fromLat: coordinate.latitude,
                    fromLng: coordinate.longitude,
                    errorMgsStartPoint:''
                })
            } else if (isSelectAddress == 'Destination') {
                this.setState({
                    locationEnd: coordinate,
                    destination: address,
                    toLat: coordinate.latitude,
                    toLng: coordinate.longitude,
                    errorMgsDestination: ''
                })
            }
            return
        }
        Toast(`Vui Lòng Chọn ${isSelectAddress == 'StartingPoint' ? 'Điểm Đi' : 'Điểm Đến'} Trong Lãnh Thổ Việt Nam!`)
        if (isSelectAddress == 'StartingPoint') {
            this.setState({
                starting_point: '',
                fromLat: '',
                fromLng: '',
                locationStart: coordinate
            })
        } else if (isSelectAddress == 'Destination') {
            this.setState({
                destination: '',
                toLat: '',
                toLng: '',
                locationEnd: coordinate
            })
        }
    }

    directionMap = () => {
        const { toLat, toLng, fromLat, fromLng, isDirection } = this.state
        if (isDirection) {
            return <MapViewDirections
                origin={{ longitude: fromLng, latitude: fromLat }}
                destination={{ longitude: toLng, latitude: toLat }}
                apikey={GOOGLE_PLACES_API_KEY}
                strokeWidth={5}
                optimizeWaypoints={true}
                language='vi'
                strokeColor={"rgb(89,171,236)"}
                tappable
                onReady={result => {
                    //console.log(result)
                    setTimeout(() => {
                        this.setState({
                            infoTag: {
                                coordinate: result.coordinates[Math.floor(result.coordinates.length / 2)],
                                distance: Math.round((result.distance + 0.00001) * 100) / 100,
                                duration: `${moment.utc(moment.duration(result.duration, "minutes").asMilliseconds()).format('H [h] mm [p]')}`.replace("0 h ", "").replace("0 d ", "")
                            },

                        })
                    }, 300);
                    //console.log(result)
                    //console.log(`Distance: ${result.distance} km`)
                    //console.log(`Duration: ${result.duration} min.`)

                    this.map_ref.current.fitToCoordinates(result.coordinates, {
                        edgePadding: {
                            right: (sizeWindow.width / 20),
                            bottom: (sizeWindow.height / 20),
                            left: (sizeWindow.width / 20),
                            top: (sizeWindow.height / 20),
                        }
                    });
                }}
            />
        }
        return null
    }

    showLocationTag = () => {
        const { infoTag } = this.state

        if (infoTag) {
            //console.log(infoTag.duration.length)
            var scale = infoTag.duration.length > 4 ? (1 + (infoTag.duration.length - 4) * 0.1) : 1
            return <MapView.Marker
                coordinate={infoTag.coordinate}
                anchor={{ x: 0, y: 0.9 }}
                onDragEnd={(e) => this.onDragEndMarker(e.nativeEvent.coordinate)}
            >
                <Image
                    source={require('./../../../assets/icon_design/box.png')}
                    style={{ position: 'absolute', width: perfectSize(70) * scale, height: perfectSize(70) * scale, }}
                    resizeMode='contain'
                />
                <View style={{ width: perfectSize(70) * scale, height: perfectSize(60) * scale, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                        <Image
                            source={require('./../../../assets/icon_design/car_front.png')}
                            style={{ width: perfectSize(20), height: perfectSize(20) }}
                            resizeMode='contain'
                        />
                        <Text style={{ color: 'rgb(245,145,68)' }}> {infoTag.duration}</Text>
                    </View>
                    <Text>{infoTag.distance} km</Text>
                </View>
            </MapView.Marker>
        }
        return null
    }

    showMakerPicker = () => {
        const { isSelectAddress } = this.state
        let url = require('./../../../assets/icon_design/flag_green.png')
        if (isSelectAddress == "Destination") {
            url = require('./../../../assets/icon_design/flag_red.png')
        }
        if (isSelectAddress) {
            return <View style={styles.markerFixed}>
                <Image
                    source={url}
                    style={{ width: 50, height: 50 }}
                    resizeMode='contain'
                />
            </View>
        }
    }

    render() {
        const {
            dateTime,
            showDateTime,
            modeDateTime,
            departure_time,
            earliest_time,
            latest_time,
            note,
            seater,
            errorMgsDeparture,
            errorMgsDestination,
            errorMgsStartPoint,
            errorMgsEarliestTime,
            errorMgsSeater,
            errorMgsLatestTime,
            isInputDestination,
            isInputStartingPoint,
            starting_point,
            destination,
            locationStart,
            locationEnd,
            region,
            isDirection,
            isSelectAddress
        } = this.state
        const {
            isDriver,
            currentLocation
        } = this.props
        return (
            <KeyboardAwareScrollView
                style={styles.container}
                resetScrollToCoords={{ x: 0, y: 0 }}
                scrollEnabled={true}
                contentContainerStyle={styles.contentContainerStyle}
                keyboardShouldPersistTaps={'handled'}
                enableOnAndroid = {false}
            >
                <View style={{ flex: 1 }}>
                    <MapView
                        ref={this.map_ref}
                        style={styles.container}
                        initialRegion={currentLocation}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        onRegionChangeComplete={this.onDragEndMarker}
                    >
                       {!isSelectAddress&&<MapView.Marker
                            coordinate={locationStart}
                            anchor={{ x: 0.1, y: 1 }}
                        >
                            <Image
                                source={require('./../../../assets/icon_design/flag_green.png')}
                                style={{ width: 40, height: 40 }}
                                resizeMode='contain'
                            />
                        </MapView.Marker>}
                        {!isSelectAddress&&this.showLocationTag()}
                        {!isSelectAddress&&<MapView.Marker
                            coordinate={locationEnd}
                            anchor={{ x: 0.1, y: 1 }}

                        >
                            <Image
                                source={require('./../../../assets/icon_design/flag_red.png')}
                                style={{ width: 40, height: 40 }}
                                resizeMode='contain'
                            />
                        </MapView.Marker>}
                        {!isSelectAddress&&this.directionMap()}
                    </MapView>
                    {this.showMakerPicker()}
                </View>
                <View style={{ width: "100%" }}>
                    <ScrollView style={{ flex: 1 }} pagingEnabled={true} horizontal ref={this.scrollRef} scrollEnabled={false} showsHorizontalScrollIndicator = {false}>
                        <View style={{ paddingHorizontal: perfectSize(25), width: sizeWindow.width }}>
                            <View style={{ justifyContent: 'center', flex: 1 }}>
                                <View>
                                    <TouchableOpacity onPress={() => this.setState({ isSelectAddress: 'StartingPoint', isInputStartingPoint: true, isInputDestination: false  })}>
                                        <Input
                                            styleLabelContainer = {styles.styleLabelContainer}
                                            style={styles.viewInput}
                                            styleInputContainer={styles.styleInputContainer}
                                            label='Địa Chỉ Điểm Đi'
                                            ref={this.inputRefs[0]}
                                            // onSubmitEditing={() => this.inputRefs[1].current.focus()}
                                            returnKeyType='next'
                                            value={starting_point}
                                            errText={errorMgsStartPoint}
                                            disableInput
                                            onPressSearch = {()=>{}}
                                        />
                                    </TouchableOpacity>
                                    <View style={{
                                        height: perfectSize(10),
                                        width: perfectSize(10),
                                        borderRadius: 10,
                                        backgroundColor: '#00e500',
                                        position: 'absolute',
                                        bottom: perfectSize(14),
                                        left: - perfectSize(5)
                                    }} />
                                </View>
                                <View>
                                    <TouchableOpacity onPress={() => this.setState({ isSelectAddress: 'Destination', isInputStartingPoint: false, isInputDestination: true })}>
                                        <Input
                                            styleLabelContainer = {styles.styleLabelContainer}
                                            style={styles.viewInput}
                                            label='Địa Chỉ Điểm Đến'
                                            styleInputContainer={styles.styleInputContainer}
                                            ref={this.inputRefs[1]}
                                            onSubmitEditing={() => this.onHandleTime('departure_time')}
                                            returnKeyType='next'
                                            value={destination}
                                            errText={errorMgsDestination}
                                            disableInput
                                            onPressSearch = {()=>{}}
                                        />
                                    </TouchableOpacity>
                                    <View style={{
                                        height: perfectSize(10),
                                        width: perfectSize(10),
                                        borderRadius: 10,
                                        backgroundColor: '#EA1313',
                                        position: 'absolute',
                                        bottom: perfectSize(14),
                                        left: - perfectSize(5)
                                    }} />
                                </View>
                            </View>
                            <View style={styles.btnView}>
                                <BtnLarge title={'Tiếp Tục'} style={styles.buttonSave} onPress={this.handleAddAddress} />
                            </View>
                        </View>
                        <View style={{ paddingHorizontal: perfectSize(25), width: sizeWindow.width }}>
                            <TouchableOpacity onPress={() => this.onHandleTime('departure_time')}>
                                <Input
                                    styleLabelContainer = {styles.styleLabelContainer}
                                    style={styles.viewInput}
                                    styleInputContainer={styles.styleInputContainer}
                                    label='Ngày Khởi Hành'
                                    ref={this.inputRefs[2]}
                                    value={departure_time != '' && `${moment(departure_time).format('DD/MM/YY')}`}
                                    returnKeyType='next'
                                    errText={errorMgsDeparture}
                                    disableInput
                                />
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity onPress={() => this.onHandleTime('earliest_time')} style={styles.viewInput}>
                                        <Input
                                            styleLabelContainer = {styles.styleLabelContainer}
                                            label='Sớm Nhất'
                                            styleInputContainer={styles.styleInputContainer}
                                            ref={this.inputRefs[3]}
                                            value={earliest_time != '' && `${moment(earliest_time).format("HH:mm")}`}
                                            returnKeyType='next'
                                            disableInput
                                            errText={errorMgsEarliestTime}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: perfectSize(25) }} />
                                <View style={{ flex: 1 }} >
                                    <TouchableOpacity onPress={() => this.onHandleTime('latest_time')} style={styles.viewInput}>
                                        <Input
                                            styleLabelContainer = {styles.styleLabelContainer}
                                            label='Muộn Nhất'
                                            styleInputContainer={styles.styleInputContainer}
                                            ref={this.inputRefs[4]}
                                            returnKeyType='done'
                                            value={latest_time != '' && `${moment(latest_time).format("HH:mm")}`}
                                            disableInput
                                            errText={errorMgsLatestTime}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: perfectSize(25) }} />
                                <View style={{ flex: 1 }}>
                                    <Input
                                        styleLabelContainer = {styles.styleLabelContainer}
                                        style={styles.viewInput}
                                        label={isDriver?'Số Ghế': 'Số Người'}
                                        styleInputContainer={styles.styleInputContainer}
                                        ref={this.inputRefs[5]}
                                        returnKeyType='next'
                                        keyboardType='numeric'
                                        maxLength={2}
                                        onChangeText={(seater) => this.setState({ seater, errorMgsSeater: "" })}
                                        errText={errorMgsSeater}
                                    />
                                </View>
                            </View>
                            <Input
                                styleLabelContainer = {styles.styleLabelContainer}
                                style={styles.viewInput}
                                label='Ghi Chú'
                                styleInputContainer={styles.styleInputContainer}
                                ref={this.inputRefs[6]}
                                returnKeyType='done'
                                onChangeText={(note) => this.setState({ note })}
                            />
                            <View style={styles.btnView}>
                                <BtnLarge title={isDriver ? 'Thêm' : 'Tìm Chuyến'} style={styles.buttonSave} onPress={this.onPressAdd} />
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <DateTimePickerModal
                    isVisible={showDateTime != null}
                    mode={modeDateTime}
                    date={dateTime}
                    headerTextIOS={(showDateTime == 'departure_time' && 'Ngày Khởi Hành') || (showDateTime == 'earliest_time' && 'Giờ Đến Đón Sớm Nhất') || (showDateTime == 'latest_time' && 'Giờ Đến Đón Muộn Nhất!')}
                    locale="en_GB"
                    onConfirm={this.handleDatePicker}
                    onCancel={this.hideDatePicker}
                    cancelTextIOS='Huỷ'
                    confirmTextIOS='Chọn'
                />
                <ModalInputGooglePlaces
                    isVisible={isInputDestination || isInputStartingPoint}
                    onPressClose={() => this.setState({ isInputDestination: false, isInputStartingPoint: false })}
                    lableSreach={isInputDestination ? 'Địa Chỉ Điểm Đến' : 'Địa Chỉ Điểm Đi'}
                    onDone={this.hanldeInputAddress}
                />
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProp = ({ user = new ModelUser(), booking = new ModelBooking(), map }) => ({
    isDriver: user.isDriver,
    list_bookings: booking.list_bookings,
    userId: user.userId,
    currentLocation: map,
})

export default connect(mapStateToProp)(NewBookingScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',

    },
    contentContainerStyle: {
        flexGrow: 1,
    },
    viewAvatar: {
        borderBottomColor: 'rgba(241, 243, 246, 0.6)',
        borderBottomWidth: 1
    },
    marginTopView: {
        flex: 1.2
    },
    viewInput: {
        marginTop: perfectSize(5),
        flex: 1
    },
    textInput: {
        fontSize: 14,
        fontFamily: 'roboto',
        fontWeight: 'bold',
        borderColor: 'rgba(0,0,0,0.05)',
        borderWidth: 2,
        borderRadius: 5,
        padding: 17,
        marginVertical: 10
    },
    btnTerms: {
        flexDirection: 'row',
        height: perfectSize(50),
        alignItems: "center",
    },
    labelTerms: {
        color: '#999999',
        height: '100%',
        lineHeight: perfectSize(50)
    },
    labelTerms1: {
        color: 'rgba(0,140,250,1)',
        lineHeight: perfectSize(50),
        height: '100%',
    },
    btnView: {
        justifyContent: 'flex-end',
        marginBottom: perfectSize(10)
    },
    avatarContainer: {
        backgroundColor: '#CDD2FD',
        borderColor: '#9BA6FA',
        borderWidth: 2,
        marginRight: perfectSize(30)
    },
    editImage: {
        height: perfectSize(18),
        width: perfectSize(18),
    },
    avatarTitle: {
        fontSize: perfectSize(28),
        fontWeight: '500',
        fontFamily: 'roboto'
    },
    viewLabelAvatar: {
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center'
    },
    labelAvartar: {
        fontSize: perfectSize(15),
        lineHeight: perfectSize(20),
        // fontFamily: 'roboto',
        textAlign: 'center',
    },
    buttonSave: {
        marginTop: perfectSize(10)
    },
    styleInputContainer: {
        height: perfectSize(38),
        marginRight: 1
    },
    markerFixed: {
        left: '50%',
        marginLeft: -4,
        marginTop: -40,
        position: 'absolute',
        top: '50%'
    },
    styleLabelContainer: {
        marginBottom: perfectSize(5)
    }
})
