import React from 'react'
import { View, Text, FlatList, RefreshControl, ActivityIndicator } from 'react-native'
import NotificationElements from '../../Components/Notification/NotificationElements'
import { connect } from 'react-redux'
import actions from '../../Actions'
import ModelCallApi from '../../Model/ModelCallApi'
import ModelNotifications from '../../Model/ModelNotification'
import { ModelParamsAPIGetBooking } from '../../Model/ModelBooking'
import { ModelNotificationDeitail } from '../../Model/ModelNotification'

class NotificationScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            isRefreshing: false,
        }
    }

    componentDidMount() {
        this.callApiNotifications(false, false)
        this.subscribe()
    }

    subscribe = () => {
        const {navigation} = this.props
        navigation.addListener('focus', () => {
            const {notificationDtos} = this.props
            if(notificationDtos.length == 0){
                this.callApiNotifications(false, false)
            }
        });
    }

    callApiNotifications = (isLoadMore = false, isRefreshing = false) => {
        const { dispatch, currentPage, totalPages, size } = this.props
        var data = new ModelCallApi()
        var params = new ModelParamsAPIGetBooking()

        if (isLoadMore && (totalPages == currentPage + 1)) {
            return;
        }
        if (!isLoadMore) {
            params.page = 0
            params.size = 20
            params.paged = true
            params.isLoadFirst = !isLoadMore && !isRefreshing
        } else {
            var page = currentPage
            params.page = page + 1
            params.size = size
            params.paged = true
            params.isLoadFirst = false
        }

        data.params = params
        data.onSuccess = () => {
            if (!isLoadMore) {
                const { isRefreshing } = this.state
                if (isRefreshing) {
                    this.setState({
                        isRefreshing: false
                    })
                }
            }
        }
        dispatch(actions.getNotification(data))
    }

    renderFooter = () => {
        const { currentPage, totalPages } = this.props
        if (totalPages == currentPage + 1) return null;
        return (
            <ActivityIndicator
                style={{ color: '#000' }}
            />
        );
    };

    onRefresh = () => {
        this.setState({
            isRefreshing: true
        })
        this.callApiNotifications(false, true)
    }

    openNotifiDeitail = (item = new ModelNotificationDeitail()) => {
        const {navigation, dispatch} = this.props

        var data = new ModelCallApi()
        data.params = {
            notificationId: item.notificationId
        }
        data.onSuccess = (result) =>{
            navigation.navigate('Notification_detail', {
                item: result,
                reload: ()=> this.callApiNotifications(false, false)
            })
        }
        dispatch(actions.getNotificationDetail(data))

    }

    render() {
        const { notificationDtos } = this.props
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                {notificationDtos.length > 0 ?
                    <FlatList
                        data={notificationDtos}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isRefreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                        renderItem={({ item, index }) => <NotificationElements item={item} onPress = {this.openNotifiDeitail}/>}
                        keyExtractor={(item, index) => index}
                        onEndReachedThreshold={0.4}
                        onEndReached={({ distanceFromEnd }) => {
                            if (distanceFromEnd < 0) return;
                            this.callApiNotifications(true)
                        }}
                        ListFooterComponent={this.renderFooter}
                    />
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
                        <Text>
                            Không có thông báo nào!
                        </Text>
                    </View>
                }
            </View>
        )
    }
}

const mapStateToProps = ({ notification = new ModelNotifications() }) => ({
    notificationDtos: notification.notificationDtos,
    size: notification.size,
    totalPages: notification.totalPages,
    currentPage: notification.currentPage
})

export default connect(mapStateToProps)(NotificationScreen);