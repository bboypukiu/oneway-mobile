import React from 'react'
import { View, Text, ScrollView, StyleSheet } from 'react-native'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

class NotificationDetailScreen extends React.Component {

    componentWillUnmount() {
        const { reload } = this.props.route.params
        reload && reload()
    }
    render() {

        const { item } = this.props.route.params
        //console.log(item)
        return (
            <ScrollView style={styles.container}>
                {item.title ? <Text style={styles.textTitle}>{`${item.title}\n`}</Text> : null}
                {item.content ? <Text style={styles.text}>{`${item.content}`}</Text> : null}
                {item.longDes ? <Text style={styles.text}>{`${item.longDes}`}</Text> : null}
            </ScrollView>
        )
    }
}

export default NotificationDetailScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: perfectSize(30),
        backgroundColor: 'white',
        borderTopWidth: perfectSize(1),
        borderColor: 'rgba(228, 228, 228, 0.6)'
    },
    body: {
        alignItems: 'center',
        borderColor: 'rgba(0,0,0,0.05)',
        borderWidth: 2,
        borderRadius: 5,
    },
    textTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingHorizontal: 10,
    },
    text: {
        fontSize: 16,
        fontWeight: '300',
        paddingHorizontal: 10,
    }
})