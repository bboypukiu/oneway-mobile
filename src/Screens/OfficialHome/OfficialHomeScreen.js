import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, Dimensions, Share, Alert, Linking, Platform } from 'react-native'
import { Avatar } from 'react-native-elements'
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import actions from './../../Actions'

import IconAwesome5 from 'react-native-vector-icons/FontAwesome5'
import ModelCallApi from '../../Model/ModelCallApi'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

import ModelUser from '../../Model/ModelUser'
import { urlAvatar } from '../../Constants/Api';
import Toast from '../../Utils/Toast';
import ModalMSg from '../../Components/ModalMsg';
import Rate, { AndroidMarket } from 'react-native-rate';

const { width, height } = Dimensions.get('window');
class OfficialHomeScreen extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            modalPermissions: true,
            content: '',
        }
    }

    toggleModal = (value) => {
        this.setState({
            isModalVisible: value
        })
    };

    componentWillMount(){
        var data = new ModelCallApi()
        const {dispatch} = this.props
        dispatch(actions.getBaseInfo(data))
    }
    onPressFindCustomer = () => {
        const { navigation, dispatch, permissions } = this.props
        if (permissions.indexOf("TAI_XE") < 0) {
            this.checkPermissions()
            return
        }
        this.setState({
            isModalVisible: false
        })
        dispatch(actions.setIsDriver(true))
        navigation.navigate("MyTabs", { isDriver: true })
    }

    onPressFindDriver = () => {
        const { navigation, dispatch, permissions } = this.props
        if (permissions.indexOf("HANH_KHACH") < 0) {
            this.checkPermissions()
            return
        }
        this.setState({
            isModalVisible: false
        })
        dispatch(actions.setIsDriver(false))
        navigation.navigate("MyTabs")
    }

    checkPermissions = () => {
        const {dispatch} = this.props
        var data = new ModelCallApi()
        data.onSuccess = (result) =>{
            this.setState({
                modalPermissions: false,
                content: result?.msg
            })
        }
        dispatch(actions.getBaseInfo(data))

    }

    onPressProfile = () => {
        const { navigation, dispatch } = this.props
        // this.setState({
        //     isModalVisible: false
        // })
        // dispatch(actions.setIsDriver(false))
        navigation.navigate("Profile")
    }

    onPressShare = async () => {
        try {
            const {contentShare, urlAndroid, urlIOS} = this.props
            const result = await Share.share({
                message:`${contentShare} ${Platform.OS == 'android' ? urlAndroid&&urlAndroid : urlIOS&&urlIOS}`,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    componentDidMount() {
        this.subscribe()
    }

    subscribe = () => {
        const { navigation, dispatch } = this.props
        navigation.addListener('focus', () => {
            dispatch(actions.setIsDriver(null))
            dispatch(actions.setTripBookingDefault({}))
        });
    }

    showButton = () => {

    }

    onRate = () => {
        const options = {
            AppleAppID: "1525658426",
            GooglePackageName: "com.oneway",
            AmazonPackageName: "com.oneway",
            // OtherAndroidURL: "http://www.randomappstore.com/app/4972120904879488689",
            preferredAndroidMarket: AndroidMarket.Google,
            preferInApp: false,
            openAppStoreIfInAppFails: true,
            // fallbackPlatformURL: "http://www.mywebsite.com/myapp.html",
        }
        Rate.rate(options, success => {
            if (success) {
                // this technically only tells us if the user successfully went to the Review Page. Whether they actually did anything, we do not know.
                // var a = 1
                // Alert.alert(`Cảm ơn bạn đã đánh giá app ^^ !`, null, [
                //     {text: 'Chấp nhận', onPress: () => {}},
                // ])
            }
        })
    }


    render() {
        const { fullname, avatar, password, phoneNumber } = this.props
        const {modalPermissions, content} = this.state
        return (
            <SafeAreaView style={styles.safeAreaView} >
                <View style={styles.container}>
                    <View style={styles.body}>
                        <View style={styles.viewAvatar}>
                            <Avatar
                                rounded title="AB"
                                source={{ uri: `${urlAvatar}${avatar}` }}
                                containerStyle={styles.avatarContainer}
                                size={perfectSize(124)}
                                titleStyle={styles.avatarTitle}
                            />
                            <View style={styles.viewLabelAvatar}>
                                {/*<View style={styles.editImage} />*/}
                                <Text style={styles.labelAvartar}>{fullname}</Text>
                                {/*<TouchableOpacity onPress={this.onPressProfile}>
                                    <Image style={styles.editImage} source={require('./../../../assets/icon_design/pencil.png')} />
                                </TouchableOpacity>*/}
                            </View>
                            <TouchableOpacity disabled={password} onPress={this.onPressProfile}>
                                <Text style={styles.textPassport}>{password || 'Vui lòng cập nhật thông tin tài xế'}</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.btnGrid}>
                            <View style={styles.btnHorizontal}>
                                <TouchableOpacity
                                    onPress={this.onPressFindCustomer}
                                    style={styles.btn}>
                                    <Image source={require('./../../../assets/icon_design/timkhach.png')} style={styles.imgBtn} />
                                    {/* <Text style={[styles.labelBtn, {top: - perfectSize(20)}]}>Tìm khách</Text> */}
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.btn} onPress={this.onPressFindDriver}>
                                    <Image source={require('./../../../assets/icon_design/timxe.png')} style={styles.imgBtn} />
                                    {/* <Text style={[styles.labelBtn, {top: - perfectSize(20)}]}>Tìm xe</Text> */}
                                </TouchableOpacity>
                            </View>
                            <View style={styles.btnHorizontal}>
                                <TouchableOpacity style={styles.btn} onPress={this.onPressProfile}>
                                    <Image source={require('./../../../assets/icon_design/doithongtin.png')} style={styles.imgBtn} />
                                    {/* <Text style={styles.labelBtn}>Cá nhân</Text> */}
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.btn} onPress={this.onRate}>
                                    <Image source={require('./../../../assets/icon_design/danhgia.png')} style={styles.imgBtn} />
                                    {/* <Text style={styles.labelBtn}>Đánh giá app</Text> */}
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity style={styles.btn} onPress={this.onPressShare}>
                                <Image source={require('./../../../assets/icon_design/shareapp.png')} style={styles.imgBtn} />
                                {/* <Text style={styles.labelBtn}>Chia sẻ app</Text> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                    <ModalMSg
                        isVisible={!modalPermissions}
                        title={'Thông báo'}
                        content={content}
                        titleOk='Gọi điện'
                        onPressOK={() => {
                            if (phoneNumber == null || phoneNumber == '') {
                                Alert.alert(`Rất Tiếc! Không Có Số Điện Thoại Để Liên Hệ!`, null, [
                                    { text: 'Chấp Nhận', onPress: () => {this.setState({ modalPermissions: true })} },
                                ])
                                return;
                            }
                            this.setState({ modalPermissions: true })
                            Linking.openURL(`tel:${phoneNumber}`)
                        }}
                        titleCancel='Đóng'
                        onPressCancel={() => this.setState({ modalPermissions: true })}
                        horizoltal
                        xIcon={false}
                    />
                </View>
            </SafeAreaView>
        )
    }
}
const mapStateToProps = function ({ user = new ModelUser() }) {
    return {
        avatar: user.avatar,
        permissions: user.permissions,
        fullname: user.fullname,
        password: user.passport,
        token: user.token,
        phoneNumber: user.commonInfo.phoneNumber,
        contentShare: user.commonInfo.contentShare,
        urlAndroid: user.commonInfo.urlAndroid,
        urlIOS: user.commonInfo.urlIOS,

    }
}

export default connect(mapStateToProps)(OfficialHomeScreen);

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: perfectSize(69)
    },
    btnUser: {
        position: 'absolute',
        top: 1,
        right: 10,
        borderWidth: 2,
        backgroundColor: 'black',
        borderRadius: 100,
        height: 40,
        width: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    body: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    btnGrid: {
        width: '100%',
        alignItems: 'center',
        marginTop: perfectSize(10)
    },
    btnHorizontal: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around'
    },
    avatarContainer: {
        backgroundColor: '#CDD2FD',
        borderColor: '#9BA6FA',
        borderWidth: 2
    },
    avatarTitle: {
        fontSize: perfectSize(28),
        fontWeight: '500',
        fontFamily: 'roboto'
    },
    labelAvartar: {
        fontSize: perfectSize(28),
        lineHeight: perfectSize(34),
        fontWeight: '500',
        fontFamily: 'roboto',
        paddingVertical: 10,
        textAlign: 'center',
    },
    labelBtn: {
        fontFamily: 'roboto',
        fontSize: perfectSize(16),
        fontWeight: '500',
        textAlign: 'center',
        top: - perfectSize(10)
        // marginVertical: 10
    },
    btn: {
        marginBottom: perfectSize(15),
        height: height * 0.15,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    imgBtn: {
        height: perfectSize(105),
        width: perfectSize(90)
    },
    viewAvatar: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    editImage: {
        height: perfectSize(17),
        width: perfectSize(17),
        marginVertical: perfectSize(22),
        marginHorizontal: 10
    },
    viewLabelAvatar: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textPassport: {
        color: '#A3A3A3',
        fontSize: perfectSize(12)
    }
})
