import React from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, ScrollView, Image, Alert } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Input from '../../Components/Input'
import BtnLarge from './../../Components/BtnLarge'
import Validator from './../../Utils/Validator'
//styles screen
import styles_df from './../../Commons/StylesDefault'
const check = require('./../../../assets/icon_design/check.png')
const uncheck = require('./../../../assets/icon_design/uncheck.png')
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

import ModelCallApi from '../../Model/ModelCallApi'

import {connect} from 'react-redux'
import actions from './../../Actions'

class RegisterScreen extends React.Component {
    constructor(props) {
        super(props)
        this.inputRefs = [
            React.createRef(),
            React.createRef(),
            React.createRef(),
            React.createRef()
        ]
        this.state = {
            // login phone
            name: '',
            phone: '',
            pass: '',
            email: '',

            // is check terms
            isAcceptTerms: false,
            // validate
            errorMgsName: '',
            errorMgsPass: '',
            errorMgsPhone: '',
            errorMgsUserEmail: '',
            errorMgsAcceptTerms: ''
        }
    }

    onPressBtnRegister = () => {
        if (this.validateInput()) return
        const { name, phone, pass, email, isAcceptTerms } = this.state
        const {navigation, dispatch} = this.props
        var data = new ModelCallApi();
        data.params = {
            fullname: name,
            phone,
            password: pass,
            email
        }

        data.onSuccess = () => {
            navigation.navigate('Login')
        }

        dispatch(actions.register(data))
    }

    validateInput = () => {
        var errorMgsName = Validator.CheckNormal(this.state.phone, 'Họ Và Tên', 0)
        var errorMgsPass = Validator.CheckNormal(this.state.pass, 'Mật Khẩu', 6)
        var errorMgsPhone = Validator.CheckNumberPhone(this.state.phone)
        var errorMgsUserEmail = Validator.CheckEmail(this.state.email, false)
        var errorMgsAcceptTerms = !this.state.isAcceptTerms && 'Vui Lòng Chấp Nhận Điều Khoản Điều Kiện'
        this.setState({
            errorMgsPhone,
            errorMgsPass,
            errorMgsName,
            errorMgsAcceptTerms,
            errorMgsUserEmail
        })
        if (errorMgsPhone != ''|| errorMgsPass != '' || errorMgsName != ''|| errorMgsAcceptTerms != '' || errorMgsUserEmail != '') {
            return true;
        }
        return false;
    }

    render() {
        const {
            errorMgsName,
            errorMgsPass,
            errorMgsPhone,
            errorMgsUserEmail,
            errorMgsAcceptTerms,
            isAcceptTerms
        } = this.state
        return (
            <KeyboardAwareScrollView
                style={styles.container}
                resetScrollToCoords={{ x: 0, y: 0 }}
                scrollEnabled={true}
                contentContainerStyle={styles.contentContainerStyle}
            >
                <View style = {styles.marginTopView}/>
                <Input
                    style={styles.viewInputName}
                    label='Họ Và Tên'
                    isRequire
                    ref={this.inputRefs[0]}
                    onSubmitEditing={() => this.inputRefs[1].current.focus()}
                    returnKeyType='next'
                    onChangeText={(name) => this.setState({ name, errorMgsName: '' })}
                    errText={errorMgsName}
                />
                <Input
                    style={styles.viewInputPass}
                    label='Mật Khẩu'
                    isRequire
                    secureTextEntry = {true}
                    ref={this.inputRefs[1]}
                    onSubmitEditing={() => this.inputRefs[2].current.focus()}
                    returnKeyType='next'
                    onChangeText={(pass) => this.setState({ pass, errorMgsPass: '' })}
                    errText={errorMgsPass}
                />
                <Input
                    style={styles.viewInputPhone}
                    label='Số Điện Thoại'
                    ref={this.inputRefs[2]}
                    keyboardType='numeric'
                    isRequire
                    onSubmitEditing={this.onPressBtnRegister}
                    returnKeyType='next'
                    onChangeText={(phone) => this.setState({ phone, errorMgsPhone: '' })}
                    errText={errorMgsPhone}
                />
                <Input
                    style={styles.viewInputemail}
                    label='Thư Điện Tử '
                    ref={this.inputRefs[3]}
                    returnKeyType='done'
                    onSubmitEditing={this.onPressBtnRegister}
                    onChangeText={(email) => this.setState({ email, errorMgsUserEmail: '' })}
                    errText={errorMgsUserEmail}
                />
                <View style={styles.btnView}>
                    <TouchableOpacity onPress = {()=> this.setState({isAcceptTerms: !isAcceptTerms, errorMgsAcceptTerms: ''})}>
                        <View style={styles.btnTerms}>
                            <Image source={isAcceptTerms ? check : uncheck} style={{height: perfectSize(45), width:perfectSize(45), top:-perfectSize(3)}} />
                            <Text
                                style={[styles.labelTerms, errorMgsAcceptTerms && {color: 'red'}]}
                            >Tôi đã đọc và đồng ý với</Text>
                            <TouchableOpacity
                                style={styles.btn}
                                onPress={() => this.props.navigation.navigate('Terms')}
                            >
                                <Text
                                    style={[styles.labelTerms1, errorMgsAcceptTerms && {color: 'violet'}]}
                                > điều khoản</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                    <BtnLarge onPress={this.onPressBtnRegister} title='Lưu' />
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProp = (state) =>({

})
export default connect(mapStateToProp) (RegisterScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: perfectSize(25),
        backgroundColor: 'white',
    },
    contentContainerStyle: {
        flexGrow: 1,
    },
    marginTopView: {
        flex: 1.2
    },
    viewInputName: {
        marginBottom: perfectSize(20)
    },
    viewInputPass: {
        marginBottom: perfectSize(20)
    },
    viewInputPhone: {
        marginBottom: perfectSize(20)
    },
    textInput: {
        fontSize: 14,
        fontFamily: 'roboto',
        fontWeight: 'bold',
        borderColor: 'rgba(0,0,0,0.05)',
        borderWidth: 2,
        borderRadius: 5,
        padding: 17,
        marginVertical: 10
    },
    btnTerms: {
        flexDirection: 'row',
        height: perfectSize(50),
        alignItems: "center",
    },
    labelTerms: {
        color: '#999999',
        height: '100%',
        lineHeight: perfectSize(50)
    },
    labelTerms1: {
        color: 'rgba(0,140,250,1)',
        lineHeight: perfectSize(50),
        height: '100%',
    },
    btnView: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: '13.8%'
    }
})
