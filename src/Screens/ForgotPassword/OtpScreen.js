import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native'
import OtpInputs from 'react-native-otp-inputs'
import BtnLarge from './../../Components/BtnLarge'
import Validator from './../../Utils/Validator'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
import { StackActions } from '@react-navigation/native';
import CountDown from 'react-native-countdown-component';

import { connect } from 'react-redux'
import actions from './../../Actions'

import ModelCallApi from '../../Model/ModelCallApi'
import { storeData, getData } from '../../Helpers/Storage'
import moment from 'moment'

class ForgotPasswordOTPScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            otp: '',
            countDownt: true,
            //Validate
            errorMgsPhone: '',
            countReSendOTP: 9
        }
    }

    onPress = () => {
        if (this.validateInput()) {
            return
        }

        const { otp } = this.state
        const { navigation, dispatch } = this.props
        const { phoneOrEmail } = this.props.route.params
        var data = new ModelCallApi();
        data.params = {
            phoneOrEmail,
            code: otp
        }
        data.onSuccess = (data) => {
            //console.log(data)
            navigation.dispatch(
                StackActions.replace('Change_password', { token: data.token })
            )
        }
        dispatch(actions.sendOTPForgotPass(data))
    }

    reSendPhone = async () => {
        const { navigation, dispatch, route: { params } } = this.props
        var data = new ModelCallApi();
        data.params = params
        var countChangePass = await getData('change_pass')
        //console.log(countChangePass != null )
        if(countChangePass != null && countChangePass.current <= 0 ){
            Alert.alert('Chức Năng Đổi Mật Khẩu Tạm Khoá', 'Bạn đã yêu cầu đổi mật khẩu quá 5 lần, chức năng đổi mật khẩu sẽ tạm khoá 3 tiếng!', [{text: 'Chấp nhận', onPress: () => {}},])
            storeData('lock_change_pass', {lock: moment().valueOf()+ 10800000})
            navigation.navigate('Login')
            return
        }
        data.onSuccess = async () => {
            let current = countChangePass != null? countChangePass.current - 1 : 4
            storeData('change_pass', {
                current
            })
            this.setState({countDownt: true, countReSendOTP: current})
        }
        dispatch(actions.sendNumberPhoneForgotPass(data))
    }

    validateInput = () => {
        var errorMgsPhone = Validator.CheckNormal(this.state.otp, '', 6)
        this.setState({
            errorMgsPhone
        })
        if (errorMgsPhone != '') {
            return true;
        }
        return false;
    }

    showCountDown = () => {
        const { countDownt, countReSendOTP } = this.state
        const { sendPhone } = this.props.route.params
        if (!countDownt) {
            return (
                <TouchableOpacity onPress={() => this.reSendPhone()}>
                    <Text>{countReSendOTP > 3 ? `Bạn Chưa Nhận Được Mã OTP?`: `Bạn Còn ${countReSendOTP} Lần Yêu Cầu Gửi Mã OTP!`}</Text>
                    <Text style={styles.btnText}>Yêu cầu gửi lại mã OTP</Text>
                </TouchableOpacity>
            )
        }
        return (
            <View style={{ flex: 1, flexDirection: 'row', alignItems: "center", justifyContent: 'center' }}>
                <Text>Thời gian nhập mã OTP của bạn còn:</Text>
                <CountDown
                    until={59}
                    size={perfectSize(20)}
                    onFinish={() => this.setState({ countDownt: false })}
                    digitStyle={{ backgroundColor: '#FFF' }}
                    digitTxtStyle={{ color: '#000' }}
                    timeToShow={['S']}
                    timeLabels={{ s: null }}
                />
            </View>
        )
    }

    render() {
        const { errorMgsPhone } = this.state
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <KeyboardAvoidingView
                    behavior={Platform.OS == "ios" ? "padding" : null}
                    style={styles.container}
                    keyboardVerticalOffset={Platform.OS === "ios" ? 100 : 0}
                >
                    <View style={{ flex: 0.45 }} />
                    <View style={styles.inputView}>
                        <Text style={styles.labelInput}>OTP</Text>
                        <OtpInputs
                            handleChange={(otp) => this.setState({ otp, errorMgsPhone: '' })}
                            numberOfInputs={6}
                            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                            inputStyles={[styles.textInputOTP, { borderColor: errorMgsPhone ? "#FF647C" : 'rgba(228, 228, 228, 0.6)' }]}
                            inputContainerStyles={{ width: perfectSize(50), height: perfectSize(50) }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: "flex-end", justifyContent: 'center' }}>
                        {this.showCountDown()}
                    </View>
                    <View style={{ marginBottom: perfectSize(52) }}>
                        <BtnLarge onPress={this.onPress} title='Xác Nhận' />
                    </View>
                </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
        )
    }
}

const mapStateToProp = (state) => ({

})

export default connect(mapStateToProp)(ForgotPasswordOTPScreen);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: perfectSize(25),
        backgroundColor: 'white',
    },
    inputView: {
        marginVertical: perfectSize(15),
    },
    labelInput: {
        fontFamily: 'roboto',
        fontWeight: 'bold',
        fontSize: perfectSize(15),
        marginBottom: perfectSize(15)
    },
    labelRequire: {
        fontSize: 16,
        color: 'red'
    },
    textInput: {
        fontSize: 14,
        fontWeight: 'bold',
        borderColor: 'rgba(0,0,0,0.05)',
        borderWidth: 2,
        borderRadius: 5,
        padding: 17,

        // marginVertical: 10
    },
    textInputOTP: {
        fontSize: 14,
        fontWeight: 'bold',
        // borderColor: 'rgba(0,0,0,0.05)',
        borderWidth: 1,
        borderRadius: 5,
        marginVertical: 0,
        marginHorizontal: 0,
        flex: 1,
        textAlign: 'center',
        fontFamily: 'roboto',
        fontWeight: 'bold',
        color: '#151522'
    },
    btn: {
        marginVertical: 13,
        backgroundColor: 'rgb(108,120,240)',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: 58
    },
    lableBtn: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white'
    },
    btnText: {
        fontSize: perfectSize(15),
        textDecorationLine: 'underline',
        color: '#0084F4',
        textAlign: 'center',
        marginBottom: perfectSize(20)
    },
})
