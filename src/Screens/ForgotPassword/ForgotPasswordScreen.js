import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native'
import OtpInputs from 'react-native-otp-inputs';
import Clipboard from "@react-native-community/clipboard";
import BtnLarge from './../../Components/BtnLarge'
import Validator from './../../Utils/Validator'
import Input from '../../Components/Input'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

import { connect } from 'react-redux'
import actions from './../../Actions'

import ModelCallApi from '../../Model/ModelCallApi'
import DeviceId from '../../Utils/DeviceId';
import { getData } from '../../Helpers/Storage';
import moment from 'moment'

class ForgotPasswordScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            phone: '',

            //Validate
            errorMgsPhone: '',
            permission: true,
            dateUnlock: ''
        }
    }

    componentDidMount (){
        this.checkPermissionChangePass()
    }

    checkPermissionChangePass = async () =>{
        var permission = await getData('lock_change_pass')
        //console.log(moment().valueOf(), '')
        if(permission != null && permission.lock >= moment().valueOf()){
            var dateUnlock = moment(permission.lock).format('YYYY-MM-DD HH:mm')
            Alert.alert('Chức năng đổi mật khẩu tạm khoá', `Chức năng đổi mật khẩu sẽ mở lúc ${dateUnlock}`, [{text: 'Chấp nhận', onPress: () => {}},])
            this.setState({permission: false, dateUnlock})
        }
    }

    onPress = async () => {
        const { phone, permission, dateUnlock } = this.state

        if(!permission){
            Alert.alert('Chức năng đổi mật khẩu tạm khoá', `Chức năng đổi mật khẩu sẽ mở lúc ${dateUnlock}`, [{text: 'Chấp nhận', onPress: () => {}},])
            return
        }
        if (this.validateInput()) {
            return
        }
        const { navigation, dispatch } = this.props
        var data = new ModelCallApi();
        var deviceid = await DeviceId()
        if (!deviceid) {
            Alert.alert(null,'Vui lòng cấp phép ứng dụng truy cập điện thoại', [{text: 'Chấp Nhận', onPress: () => {}},])
            return
        }
        data.params = {
            phoneOrEmail: phone,
            device: deviceid,
        }
        data.onSuccess = () => {
            navigation.navigate('OTP', {...data.params})
        }
        dispatch(actions.sendNumberPhoneForgotPass(data))
    }

    validateInput = () => {
        var errorMgsPhone = Validator.CheckEmailOrNumberPhone(this.state.phone)
        this.setState({
            errorMgsPhone
        })
        if (errorMgsPhone != '') {
            return true;
        }
        return false;
    }

    render() {
        const { errorMgsPhone } = this.state
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <KeyboardAvoidingView
                    behavior={Platform.OS == "ios" ? "padding" : null}
                    style={styles.container}
                    keyboardVerticalOffset={Platform.OS === "ios" ? 100 : 0}
                >
                    <View style={{ flex: 0.45 }} />
                    <Input
                        style={styles.viewTextInputUser}
                        label='Vui lòng nhập Email'
                        // keyboardType='numeric'
                        isRequire
                        onChangeText={(phone) => this.setState({ phone, errorMgsPhone: '' })}
                        onSubmitEditing={this.onPress}
                        returnKeyType='next'
                        errText={errorMgsPhone}
                    />
                    <View style={{ flex: 1 }} />
                    <View style={{ marginBottom: perfectSize(52) }}>
                        <BtnLarge onPress={this.onPress} title='Gửi OTP' />
                    </View>
                </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
        )
    }
}

const mapStateToProp = (state) => ({

})

export default connect(mapStateToProp)(ForgotPasswordScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: perfectSize(25),
        backgroundColor: 'white',
    },
    btn: {
        marginVertical: 13,
        backgroundColor: 'rgb(108,120,240)',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: 58
    },
    lableBtn: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white'
    },
})
