import React from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet } from 'react-native'
import IconAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MapView from '../../Components/Map/MapView'
import BookingDeitail from '../../Components/BookingDeitail'
import { connect } from 'react-redux'
import ModelCallApi from '../../Model/ModelCallApi'
import ModelTrips from '../../Model/ModelTrip'
import ModelUser from '../../Model/ModelUser'
import actions from '../../Actions'
class HomeMapScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            mapReady: false
        }
    }

    onPressMarker = (obj) => {
        var data = new ModelCallApi()
        const { dispatch } = this.props
        //console.log(obj)
        data.params = obj
        data.onSuccess = () => {
            this.props.navigation.navigate('Booking_detail')
        }
        dispatch(actions.getTripDetail(data))
    }


    componentDidMount() {
        this.subscribe()
    }

    subscribe = () => {
        const { navigation, dispatch } = this.props
        navigation.addListener('focus', () => {
            var data = new ModelCallApi()
            data.onSuccess = () => {
                // setTimeout(() => {
                //     this.setState({
                //         mapReady: true
                //     })
                // }, 1000);
            }
            dispatch(actions.getAllTripBookingByUser(data))
        });
        navigation.addListener('blur', () => {
            // // var data = new ModelCallApi()
            setTimeout(() => {
                // this.setState({
                //     mapReady: false
                // })
                dispatch(actions.deleteBookingsTripsAvailable({}))
            }, 300);

        });
    }

    render() {
        const { navigation, trips_bookings_available } = this.props
        const {mapReady} = this.state
        return (
            <View style={styles.container}>
                <MapView
                    items={trips_bookings_available}
                    onPressMarker={this.onPressMarker}
                    mapReady = {mapReady}
                />
            </View>
        )
    }
}

const mapStateToProps = ({ map, trips = new ModelTrips(), user = new ModelUser() }) => {
    return {
        trips_bookings_available: trips.trips_bookings_available,
    }
}

export default connect(mapStateToProps)(HomeMapScreen);

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnUser: {
        position: 'absolute',
        top: 0,
        right: 0,
        margin: 10,
        borderWidth: 2,
        backgroundColor: 'black',
        borderRadius: 100,
        height: 40,
        width: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
})