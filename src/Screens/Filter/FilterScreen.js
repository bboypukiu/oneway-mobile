import React from 'react'
import { View, Text, StyleSheet, TextInput, Image, Alert, Dimensions, TouchableOpacity } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Input from '../../Components/Input'
import BtnLarge from '../../Components/BtnLarge'
import Validator from '../../Utils/Validator'
import moment from 'moment'
import { connect } from 'react-redux'
import actions from '../../Actions'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import ModalInputGooglePlaces from '../../Components/ModalInputGooglePlaces'
import ModelPlaceMap from '../../Model/ModelPlaceMap'

import ModelCallApi from '../../Model/ModelCallApi'
import { ModelParamsAPIFindBookig } from '../../Model/ModelBooking'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { getRegionForCoordinates, ranDomeMaker, mapCoordinateToAddress } from '../../Utils/Map'
import Toast from '../../Utils/Toast'
import Modal from 'react-native-modal'
const windowWidth = Dimensions.get('window').width;
// import { TouchableOpacity } from 'react-native-gesture-handler'
import AsyncStorage from '@react-native-community/async-storage'
import { SafeAreaView } from 'react-native-safe-area-context'
import { ModelFindBooking } from '../../Model/ModelUser'

class FilterScreen extends React.Component {
    constructor(props) {
        super(props)
        this.inputRefs = [
            React.createRef(),
            React.createRef(),
        ]
        this.map_ref = React.createRef()
        this.state = {
            dateTime: new Date(),
            showDateTime: false,

            destination: '',
            datePick: '',
            toLat: '',
            toLng: '',
            errorMgsDatePick: '',
            errorMgsDestination: '',
            isInputDestination: false,
            isFilter: true,
            locationEnd: {},
            isVisible: false,
            mapReady: false,
            isDrag: false
        }
    }

    componentDidMount() {
        this.loadCacheFilter()
    }

    loadCacheFilter = async () => {
        const {findBooking, currentLocation} = this.props
        //console.log(findBooking)
        if(!findBooking?.long){
            return
        }
        var coordinate = {
            latitudeDelta: currentLocation.latitudeDelta,
            longitudeDelta: currentLocation.longitudeDelta,
            latitude: findBooking.lat,
            longitude: findBooking.long,
        }
        var address = await mapCoordinateToAddress(coordinate)
        if (address) {
            this.setState({
                destination: address,
                toLat: coordinate.latitude,
                toLng: coordinate.longitude,
                locationEnd: coordinate,
                datePick: `${findBooking.date}`.split(" ")[0],
                isFilter: false
            },
            ()=> this.map_ref.current.animateToCoordinate(coordinate)
            )
        }
    }

    showDatePicker = () => {
        this.setState({
            showDateTime: true,
            dateTime: new Date()
        })
    }

    handleDatePicker = (date) => {
        const { showDateTime, dateTime } = this.state

        if (showDateTime) {
            var selectedDate = moment(date).valueOf()
            var now = moment(dateTime).valueOf()
            if (moment(selectedDate).format('YYYY-MM-DD') < moment(now).format('YYYY-MM-DD')) {
                Alert.alert('Sai Ngày Chọn', 'Ngày Được Chọn Không Nhỏ Hơn Ngày Hiện Tại!', [
                    {
                        text: 'Chọn Lại', onPress: () => { }
                    },
                ])
                return;
            }
            this.setState({
                showDateTime: false,
                datePick: moment(selectedDate).format('YYYY-MM-DD'),
                errorMgsDatePick: ''
            })
        }
    }

    hideDatePicker = () => {
        this.setState({
            showDateTime: false
        })
    }

    cancelFilter = () => {
        //console.log(111111)
        const { dispatch } = this.props
        var data = new ModelCallApi()
        data.onSuccess = () => {
            this.setState({
                dateTime: new Date(),
                showDateTime: false,

                destination: '',
                datePick: '',
                toLat: '',
                toLng: '',
                errorMgsDatePick: '',
                errorMgsDestination: '',
                isInputDestination: false,
                isFilter: true,
                locationEnd: {},
                isVisible: false,
                mapReady: false,
                isDrag: false
            })

            dispatch(actions.setFindBookingUser({}))
        }
        dispatch(actions.cancelFindBookingsByDateAndAddress(data))
    }

    onPressFilter = () => {
        const { destination, datePick, toLat, toLng, isFilter } = this.state
        if (!isFilter) {
            this.cancelFilter()
            return
        }
        var errorMgsDestination = Validator.CheckNormal(destination, 'Điểm Đến', 6)
        var errorMgsDatePick = Validator.CheckNormal(datePick, 'Ngày Lọc', 6)

        if (errorMgsDestination != '' || errorMgsDatePick != '') {
            this.setState({
                errorMgsDestination,
                errorMgsDatePick,
            })
            return;
        }
        var data = new ModelCallApi()
        var params = new ModelParamsAPIFindBookig()
        params.inputDate = datePick
        params.lon = toLng
        params.lat = toLat

        data.params = params

        const { dispatch, navigation, route, list_bookings } = this.props
        data.onSuccess = (value) => {
            this.setState({ isFilter: false })
            let _data = new ModelFindBooking()
            _data.long = toLng
            _data.lat = toLat
            _data.date = datePick
            dispatch(actions.setFindBookingUser(_data))
            if (value) {
                navigation.navigate('Result_screen')
            }
            // setTimeout(() => {
            //     AsyncStorage.setItem("filter", JSON.stringify(this.state))
            // }, 300);
        }
        dispatch(actions.findBookingsByDateAndAddress(data))
    }

    hanldeInputAddress = (response = new ModelPlaceMap()) => {
        var location = {
            longitude: response.location.lng,
            latitude: response.location.lat
        }
        const { currentLocation } = this.props
        this.setState({
            destination: response.formatted_address,
            toLat: response.location.lat,
            toLng: response.location.lng,
            isInputDestination: false,
            locationEnd: location,
            errorMgsDestination: ''
        }, () => this.map_ref.current.animateToCoordinate(location))
        // setTimeout(() => {
        //     this.showDatePicker()
        // }, 600);
    }

    onDragEndMarker = async (coordinate) => {
        const { isDrag } = this.state
        if (!isDrag) {
            return
        }
        var address = await mapCoordinateToAddress(coordinate)
        //console.log(coordinate)
        if (address) {
            this.setState({
                destination: address,
                toLat: coordinate.latitude,
                toLng: coordinate.longitude,
                errorMgsDestination: '',
                locationEnd: coordinate,
                isDrag: false
            })
            return
        }
        Toast(`Vui Lòng Chọn Điểm Đến Trong Lãnh Thổ Việt Nam!`)
        this.setState({
            destination: '',
            toLat: '',
            toLng: '',
            locationEnd: coordinate,
        })
    }

    render() {
        const {
            showDateTime,
            dateTime,
            errorMgsDatePick,
            errorMgsDestination,
            datePick,
            destination,
            isInputDestination,
            isFilter,
            locationEnd,
            isVisible,
            mapReady
        } = this.state
        const {
            currentLocation
        } = this.props
        return (
            <KeyboardAwareScrollView
                style={styles.container}
                resetScrollToCoords={{ x: 0, y: 0 }}
                scrollEnabled={false}
                contentContainerStyle={styles.contentContainerStyle}
                keyboardShouldPersistTaps={'handled'}
            >
                <View style={{ flex: 1 }}>
                    <MapView
                        ref={this.map_ref}
                        style={styles.containerMap}
                        initialRegion={currentLocation}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        onRegionChangeComplete={(e) => this.onDragEndMarker(e)}
                        onPanDrag={() => this.setState({ isDrag: true })}
                    />
                    <View style={styles.markerFixed}>
                        <Image
                            source={require('./../../../assets/icon_design/flag_red.png')}
                            style={{ width: 50, height: 50 }}
                            resizeMode='contain'
                        />
                    </View>
                </View>
                <View style={styles.containerInput}>
                    <TouchableOpacity onPress={() => {
                        this.setState({ isInputDestination: true })
                    }}>
                        <View pointerEvents="none">
                            <Input
                                styleLabelContainer={styles.styleLabelContainer}
                                style={styles.viewInput}
                                label='Nhập Điểm Đến'
                                styleInputContainer={styles.styleInputContainer}
                                ref={this.inputRefs[0]}
                                returnKeyType='next'
                                value={destination}
                                errText={errorMgsDestination}
                                onPressSearch={() => { }}
                                disableInput
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showDatePicker}>
                        <View pointerEvents="none">
                            <Input
                                styleLabelContainer={styles.styleLabelContainer}
                                style={styles.viewInput}
                                styleInputContainer={styles.styleInputContainer}
                                label='Nhập Ngày'
                                ref={this.inputRefs[2]}
                                value={datePick != '' && `${moment(datePick).format('DD/MM/YY')}`}
                                returnKeyType='next'
                                errText={errorMgsDatePick}
                            />
                            <Image source={require('./../../../assets/icon_design/small_calendar.png')} resizeMode='contain' style={styles.imgCalendar} />
                        </View>
                    </TouchableOpacity>
                    <View style={styles.btnView}>
                        <BtnLarge title={isFilter ? 'Lọc Khách' : 'Huỷ Lọc Khách'} style={[styles.btnFilter, !isFilter && { backgroundColor: '#FF647C' }]} onPress={this.onPressFilter} />
                        <TouchableOpacity
                            onPress={() => this.setState({ isVisible: true })}
                            style={styles.btn}
                        >
                            <Text style={styles.btnText}>Tìm Hiểu</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <Modal
                    isVisible={isVisible}
                    animationIn='fadeIn'
                    animationOut='fadeOut'
                    backdropTransitionOutTiming={0}
                >
                    <View style={styles.containerModal}>
                        <View style={{ flex: 4 }} />
                        <Text style={styles.title} >TÌM HIỂU</Text>
                        <Text style={styles.subcription}>Khi bạn nhập địa chỉ nơi đến, và chọn ngày tháng, sau đó nhấn nút “Lọc Khách”, thì App sẽ chỉ thông báo cho bạn, và hiển thị trên bản đồ những khách có nhu cầu tìm xe trong ngày đó, và nơi đến của khách đó xung quanh địa chỉ bạn chọn với bán kính là 10km.</Text>
                        <View style={{ flex: 2 }} />
                        <View style={{ width: '100%', paddingHorizontal: perfectSize(25) }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ isVisible: false })}
                                style={styles.btnLarge}
                            >
                                <Text style={styles.lableBtnLarge}>Đã Hiểu</Text>
                            </TouchableOpacity>
                            <View style={{ flex: 4 }} />
                        </View>
                    </View>
                </Modal>
                <DateTimePickerModal
                    isVisible={showDateTime}
                    mode='date'
                    date={dateTime}
                    headerTextIOS='Chọn Ngày Lọc'
                    locale="en_GB"
                    onConfirm={this.handleDatePicker}
                    onCancel={this.hideDatePicker}
                    cancelTextIOS='Huỷ'
                    confirmTextIOS='Chọn'
                />
                <ModalInputGooglePlaces
                    isVisible={isInputDestination}
                    onPressClose={() => this.setState({ isInputDestination: false })}
                    lableSreach={'Địa Chỉ Điểm Đến'}
                    onDone={this.hanldeInputAddress}
                />
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProp = ({ user = new ModelUser(), booking = new ModelBooking(), map }) => ({
    currentLocation: map,
    findBooking: user.findBooking
})

export default connect(mapStateToProp)(FilterScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    containerInput: {
        paddingHorizontal: perfectSize(25),
    },
    containerMap: {
        flex: 1,
        backgroundColor: 'white',
    },
    contentContainerStyle: {
        flexGrow: 1,
    },
    title: {
        fontFamily: 'roboto',
        fontSize: perfectSize(28),
        lineHeight: perfectSize(34),
        fontWeight: '500',
        color: '#151522',
        marginHorizontal: perfectSize(25),
        marginBottom: perfectSize(16),
    },
    subcription: {
        fontSize: perfectSize(16),
        lineHeight: perfectSize(22),

        color: '#151522',
        marginHorizontal: perfectSize(25)
    },
    viewInput: {
        marginTop: perfectSize(5),
    },
    styleInputContainer: {
        height: perfectSize(38)
    },
    imgCalendar: {
        width: perfectSize(18),
        height: perfectSize(18),
        position: 'absolute',
        bottom: perfectSize(12),
        right: perfectSize(12)
    },
    btnView: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: perfectSize(15)
    },
    btnFilter: {
        marginTop: perfectSize(20)
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: perfectSize(15)
    },
    btnText: {
        fontSize: perfectSize(15),
        textDecorationLine: 'underline',
        color: '#0084F4'
    },
    containerModal: {
        width: "100%",
        height: perfectSize(350),
        backgroundColor: 'white',
        borderRadius: perfectSize(25),
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 40
    },
    btnLarge: {
        // marginVertical: 13,
        backgroundColor: '#6979F8',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: perfectSize(50),
        marginTop: perfectSize(25)
    },
    lableBtnLarge: {
        fontSize: perfectSize(16),
        fontWeight: 'bold',
        color: 'white',
        fontFamily: 'roboto'
    },
    markerFixed: {
        left: '50%',
        marginLeft: -4,
        marginTop: -40,
        position: 'absolute',
        top: '50%'
    },
    styleLabelContainer: {
        marginBottom: perfectSize(5)
    }
})
