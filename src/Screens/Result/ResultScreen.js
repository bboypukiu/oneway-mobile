import React from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet } from 'react-native'
import IconAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MapView from '../../Components/Map/MapView'
import BookingDeitail from '../../Components/BookingDeitail'
import { connect } from 'react-redux'
import ModelCallApi from '../../Model/ModelCallApi'
import ModelTrips from '../../Model/ModelTrip'
import ModelUser from '../../Model/ModelUser'
import actions from '../../Actions'
class ResultScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            mapReady: false
        }
    }

    onPressMarker = (obj) => {
        var data = new ModelCallApi()
        const { dispatch } = this.props
        data.params = obj
        data.onSuccess = () => {
            this.props.navigation.navigate('Booking_detail')
        }
        dispatch(actions.getTripDetail(data))
    }


    componentDidMount() {
        setTimeout(() => {
            this.setState({
                mapReady: true
            })
        }, 300);
    }


    render() {
        const { navigation, trips_by_bookingId } = this.props
        const {mapReady} = this.state
        return (
            <View style={styles.container}>
                <MapView 
                    items = {trips_by_bookingId}
                    onPressMarker = {this.onPressMarker}
                    mapReady={mapReady}
                />
            </View>
        )
    }
}

const mapStateToProps = ({map, trips = new ModelTrips(), user = new ModelUser()}) => {
    return {
        trips_by_bookingId: trips.trips_by_bookingId,
    }
}

export default connect(mapStateToProps)(ResultScreen);

const styles = StyleSheet.create({
    safeView: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnUser: {
        position: 'absolute',
        top: 0,
        right: 0,
        margin: 10,
        borderWidth: 2,
        backgroundColor: 'black',
        borderRadius: 100,
        height: 40,
        width: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
})