import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native'
import BtnLarge from './../../Components/BtnLarge'
import Input from '../../Components/Input'
import Validator from './../../Utils/Validator'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

import {connect} from 'react-redux'
import actions from './../../Actions'

import ModelCallApi from '../../Model/ModelCallApi'
import { removeData } from '../../Helpers/Storage'

class ChangePasswordScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            pass: '',
            confirmPass: '',

            //Validate
            errorMgsPass: '',
            errorMgsConfirmPass: '',
        }
        this.inputConfirmPassRefs = React.createRef()
    }

    onPress = () => {
        if (this.validateInput()) {
            return
        }
        const {pass, confirmPass} = this.state
        const {navigation, dispatch} = this.props
        const {token} = this.props.route.params
        var data = new ModelCallApi();
        data.params = {
            token,
            newPassword: pass
        }
        data.onSuccess = () => {
            navigation.popToTop()
            removeData('change_pass')
        }
        dispatch(actions.newPassForgotPass(data))

    }

    validateInput = () => {
        var errorMgsPass = Validator.CheckNormal(this.state.pass, 'Mật Khẩu', 6)
        var { errorMgsConfirmPass, pass, confirmPass } = this.state
        if (errorMgsPass == '' && pass != confirmPass) {
            errorMgsConfirmPass = 'Mật Khẩu Mới Không Trùng Nhau!'
        }else if(confirmPass == ''){
            errorMgsConfirmPass = 'Vui Lòng Nhập Lại Mật Khẩu Mới!'
        }
        this.setState({
            errorMgsPass,
            errorMgsConfirmPass
        })

        if (errorMgsPass != '' || errorMgsConfirmPass != '') {
            return true;
        }
        return false;
    }

    render() {

        const { errorMgsPass, errorMgsConfirmPass } = this.state

        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <KeyboardAvoidingView
                    behavior={Platform.OS == "ios" ? "padding" : null}
                    style={styles.container}
                    keyboardVerticalOffset={Platform.OS === "ios" ? 100 : 0}
                >
                    <View style={{ flex: 0.58 }} />
                    <Input
                        style={styles.viewInput}
                        label='Mật Khẩu Mới'
                        isRequire
                        secureTextEntry={true}
                        onChangeText={(pass) => this.setState({ pass, errorMgsPass: '' })}
                        onSubmitEditing={() => this.inputConfirmPassRefs.current.focus()}
                        returnKeyType='next'
                        errText={errorMgsPass}
                    />
                    <Input
                        ref={this.inputConfirmPassRefs}
                        label='Nhập Lại Mật Khẩu Mới'
                        isRequire
                        secureTextEntry={true}
                        onChangeText={(confirmPass) => this.setState({ confirmPass, errorMgsConfirmPass: '' })}
                        onSubmitEditing={this.onPress}
                        returnKeyType='done'
                        errText={errorMgsConfirmPass}
                    />
                    <View style={{ flex: 1 }} />
                    <View style={{ marginBottom: perfectSize(52) }}>
                        <BtnLarge onPress={this.onPress} title='Cập Nhật Mật Khẩu' />
                    </View>
                </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
        )
    }
}


const mapStateToProp = (state) =>({

})

export default connect(mapStateToProp) (ChangePasswordScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: perfectSize(25),
        backgroundColor: 'white',
    },
    viewInput: {
        paddingBottom: perfectSize(20)
    }
})
