import React from 'react'
import { View, Image, Platform } from 'react-native'
import { connect } from 'react-redux'
import actions from './../Actions'
import { getAccout } from '../Helpers/Keychain'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
import Splash from 'react-native-splash-screen'
import Navigator from '../Navigations'
import ModelCallApi from './../Model/ModelCallApi'
import ModelUser from '../Model/ModelUser'

class SplashScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isGotoMainScreen: false
        }
    }
    componentDidMount() {
        var count = 10;
        this.interVal = setInterval(() => {
            if(count < 0 && this.props.fcmToken == ''){
                clearInterval(this.interVal)
                this.interVal = null
                this.handleCheckLogin()
                return
            }
            if(this.props.fcmToken != ''){
                clearInterval(this.interVal)
                this.interVal = null
                this.handleCheckLogin()
            }
            count--
            
        }, 200);
    }

    handleCheckLogin = async () => {
        const { dispatch } = this.props

        var account = await getAccout()
        if (account == null || !account.username) {
            this.gotoMainScreen()
            return
        }

        var data = new ModelCallApi();
        data.params = {
            username: account.username,
            password: account.password,
            regId: this.props.fcmToken
        }
        data.onSuccess = () => {
            this.gotoMainScreen()
        }
        data.onFail = () => {
            this.gotoMainScreen()
        }

        dispatch(actions.login(data))
    }

    gotoMainScreen = () => {
        setTimeout(() => {
            Splash.hide()
            this.setState({
                isGotoMainScreen: true
            })
        }, 100);

    }

    render() {
        const { isGotoMainScreen } = this.state
        if (isGotoMainScreen) {
            return <Navigator />
        }
        return null
    }
}

const mapStateToProp = ({user = new ModelUser()}) => ({
    fcmToken: user.fcmToken
})

export default connect(mapStateToProp)(SplashScreen);