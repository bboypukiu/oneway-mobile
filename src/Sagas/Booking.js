import actions from '../Actions';
import actionTypes from '../Actions/ActionTypes';
import { call, put, takeEvery, fork } from 'redux-saga/effects';
import CallApi from '../Helpers/HttpRequests';
import load from './../Utils/Loading'

import ModelCallApi from '../Model/ModelCallApi';

import StatusResponse, { bookingFaill, getBookingsFaill, getTripsBookingsAvailable, cancelFindBookingsFaill } from '../Constants/StatusResponse';
import ExceptionMessage from '../Constants/ExceptionMessage';
import { ModelListBookings, ModelBookingDetail } from '../Model/ModelBooking';
import Store from './../Store'
function* newBooking({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  //console.log(params)
  try {
    load.showLoading()
    const response = yield call(CallApi.booking, params);
    //console.log(response)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.hideLoading()
        setTimeout(() => {
          onSuccess();
        }, 100);
      } else {
        load.hideLoading()
        var value = bookingFaill;
        value.message = response.data.message;
        onFail(value);
      }
    } else {
      load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* getBookings({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    params.isLoadFirst && load.showLoading()
    const response = yield call(CallApi.getBookings, params);
    //console.log(response)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        params.isLoadFirst && load.showDone()
        onSuccess(data?.bookingDtos?.length < 0 || data?.tripDtos?.length < 0);
        yield put(actions.addBookings(data))
      } else if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.FAILED && data == null) {
        params.isLoadFirst && load.hideLoading()
        yield put(actions.addBookings(new ModelListBookings()))
        onSuccess(data == null);
      } else {
        params.isLoadFirst && load.hideLoading()
        var value = getBookingsFaill;
        value.message = response.data.message;
        onFail(value);
      }
    } else {
      params.isLoadFirst && load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    params.isLoadFirst && load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* removeItemBooking({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.removeItemBookings, params);
    //console.log(response, params)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.hideLoading()
        setTimeout(() => {
          onSuccess()
        }, 100);
      } else {
        load.hideLoading()
        var value = getBookingsFaill;
        value.message = response.data.message;
        onFail(value);
      }
    } else {
      load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* findBooking({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.findBookingsByDateAndAddress, params);
    //console.log(response, params)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status, message } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.hideLoading()
        yield put(actions.addTripsByBookingIdToStore(data))
        setTimeout(() => {
          onSuccess(true)
        }, 100);
      } else if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.FAILED && message == "FINDALL.FAIL") {
        load.hideLoading()
        var value = getTripsBookingsAvailable;
        value.message = "App sẽ thông báo thông tin khách phù hợp với nhu cầu lọc khách của bạn."
        value.title = null
        onFail(value);
        onSuccess(false)
      } else {
        load.hideLoading()
        var value = getTripsBookingsAvailable;
        value.message = response.data.message;
        onFail(value);
      }
    } else {
      load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* cancelFindBooking({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.cancelFindBookingsByDateAndAddress, params);
    //console.log(response, params)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status, message } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.hideLoading()
        onSuccess()
      } else {
        load.hideLoading()
        var value = cancelFindBookingsFaill;
        value.message = response.data.message;
        onFail(value);
      }
    } else {
      load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* unfolowBookingTrip({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.unfollowTripBooking, params);
    //console.log(response, params)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status, message } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.hideLoading()

        yield put(actions.hideTripDetail())
        setTimeout(() => {
          var value = getTripsBookingsAvailable;
          value.title = message
          value.message = null
          onSuccess()
          onFail(value);
        }, 100);
      } else {
        load.hideLoading()
        var value = getTripsBookingsAvailable;
        value.title = message
        value.message = null
        onFail(value);
      }
    } else {
      load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}


export function* sagaNewBooking() {
  yield takeEvery(actionTypes.SAGA_NEW_BOOKING, newBooking);
}

export function* sagaGetBookings() {
  yield takeEvery(actionTypes.SAGA_GET_BOOKINGS, getBookings);
}

export function* sagaRemoveBooking() {
  yield takeEvery(actionTypes.SAGA_BOOKING_REMOVE_ITEM, removeItemBooking);
}

export function* sagaFindBooking() {
  yield takeEvery(actionTypes.SAGA_FIND_BOOKINGS_BY_DATE_AND_ADDRESS, findBooking);
}

export function* sagaCancelFindBooking() {
  yield takeEvery(actionTypes.SAGA_CANCEL_FIND_BOOKINGS_BY_DATE_AND_ADDRESS, cancelFindBooking);
}

export function* sagaUnfolowBookingTrip() {
  yield takeEvery(actionTypes.SAGA_UNFOLLOW_BOOKING_TRIP, unfolowBookingTrip);
}
