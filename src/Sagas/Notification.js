import actions from '../Actions';
import actionTypes from '../Actions/ActionTypes';
import { call, put, takeEvery, fork } from 'redux-saga/effects';
import CallApi from '../Helpers/HttpRequests';
import load from './../Utils/Loading'

import ModelCallApi from '../Model/ModelCallApi';

import StatusResponse, { bookingFaill, getBookingsFaill, getNotificationDetailFaill, getNotificationsFaill, getNotificationStatusFaill, setNotificationStatusFaill } from '../Constants/StatusResponse';
import ExceptionMessage from '../Constants/ExceptionMessage';
import { ModelListBookings, ModelBookingDetail } from '../Model/ModelBooking';
import Store from './../Store'

function* getNotifications({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    params.isLoadFirst && load.showLoading()
    const response = yield call(CallApi.getNotification, params);
    //console.log(response)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        params.isLoadFirst && load.hideLoading()
        onSuccess();
        yield put(actions.addNotification(data))
      } else {
        params.isLoadFirst && load.hideLoading()
        var value = getNotificationsFaill;
        value.message = response.data.message;
        onFail(value);
      }
    } else {
      params.isLoadFirst && load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    params.isLoadFirst && load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* getNotificationStatus({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.getNotificationStatus, params);
    //console.log(response)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.hideLoading()
        // yield put(actions.addNotificationStatusToStore(data))
        onSuccess(data);
      } else {
        load.hideLoading()
        var value = getNotificationStatusFaill;
        onFail(value);
      }
    } else {
      load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* setNotificationStatus({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.setNotificationStatus, params);
    //console.log(response)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        var _data = new ModelCallApi()
        _data.onSuccess = onSuccess
        yield put(actions.getNotificationStatus(_data))
      } else {
        load.hideLoading()
        var value = setNotificationStatusFaill;
        onFail(value);
      }
    } else {
      //TODO: loi server hoac mot thu gi do
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

function* getNotificationDetail({ data = new ModelCallApi() }) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.getNotificationDetail, params);
    //console.log(response)
    if (response && response.status == 200) {
      const { httpStatus, statusCode, data, status, message } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.hideLoading()
        onSuccess(data)
      } else {
        load.hideLoading()
        var value = getNotificationDetailFaill;
        value.message = message
        onFail(value);
      }
    } else {
      //TODO: loi server hoac mot thu gi do
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError", error)
  }
}

export function* sagaGetNotifications() {
  yield takeEvery(actionTypes.SAGA_GET_NOTIFICATIONS, getNotifications);
}

export function* sagaGetNotificationStatus() {
  yield takeEvery(actionTypes.SAGA_GET_NOTIFICATION_STATUS, getNotificationStatus);
}

export function* sagaSetNotificationStatus() {
  yield takeEvery(actionTypes.SAGA_SET_NOTIFICATION_STATUS, setNotificationStatus);
}

export function* sagaGetNotificationDetail() {
  yield takeEvery(actionTypes.SAGA_GET_NOTIFICATIONS_DETAIL, getNotificationDetail);
}

