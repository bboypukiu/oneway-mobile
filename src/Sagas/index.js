import { fork } from 'redux-saga/effects';
import {sagaLogin, sagaLogout, sagaSendPhone, sagaChangePass, sagaRegister, sagaUploadAvatar, sagaUpdateProfile, sagaGetBaseInfo, sagasSendOTPForgotPass, sagasNewPassForgotPass} from './User';
import {sagaNewBooking, sagaGetBookings, sagaRemoveBooking, sagaFindBooking, sagaUnfolowBookingTrip, sagaCancelFindBooking} from './Booking'
import { sagaGetDistanceThreePoints, sagaGetTripsByBookingId, sagaGetTripDetail, sagaGetAllTripBookingByUser, sagaGetTripBookingById} from './Trip';
import { sagaGetNotifications, sagaGetNotificationStatus, sagaSetNotificationStatus, sagaGetNotificationDetail } from './Notification';
export default function* rootSaga() {
  yield fork(sagaLogin)
  yield fork(sagaLogout)
  yield fork(sagaSendPhone)
  yield fork(sagasSendOTPForgotPass)
  yield fork(sagasNewPassForgotPass)
  yield fork(sagaChangePass)
  yield fork(sagaRegister)
  yield fork(sagaUpdateProfile)
  yield fork(sagaUploadAvatar)
  yield fork(sagaNewBooking)
  yield fork(sagaGetBookings)
  yield fork(sagaRemoveBooking)

  yield fork(sagaFindBooking)
  yield fork(sagaCancelFindBooking)
  
  yield fork(sagaGetDistanceThreePoints)
  yield fork(sagaGetTripsByBookingId)
  yield fork(sagaGetTripDetail)

  yield fork(sagaGetNotifications)
  yield fork(sagaGetNotificationStatus)
  yield fork(sagaSetNotificationStatus)
  yield fork(sagaGetNotificationDetail)

  yield fork(sagaUnfolowBookingTrip)
  yield fork(sagaGetAllTripBookingByUser)
  yield fork(sagaGetTripBookingById)
  
  yield fork(sagaGetBaseInfo)
}