import actions from '../Actions';
import actionTypes from '../Actions/ActionTypes';
import { call, put, takeEvery, fork, all } from 'redux-saga/effects';
import CallApi from '../Helpers/HttpRequests';
import load from './../Utils/Loading'

import ModelCallApi from '../Model/ModelCallApi';

import StatusResponse, { getDistanceFaill, getTripsByBookingIdFaill, getTripFaill, getAllTripBookingByUserFaill } from '../Constants/StatusResponse';
import ExceptionMessage from '../Constants/ExceptionMessage';
import { ModelOnSuccessGetTripsByBookingId } from '../Model/ModelTrip';
import Store from './../Store'
function* getDistanceThreePoint({ data = new ModelCallApi() }) {
    const { onSuccess, params, paramsTwo, onFail } = data
    try {
        load.showLoading()
        const [response, responseTwo] = yield all([
            call(CallApi.distanceTwoCoordinates, params),
            call(CallApi.distanceTwoCoordinates, paramsTwo)
        ])
        if (response && response.status == 200 && responseTwo && responseTwo.status == 200) {
            const data1 = response.data
            const data2 = responseTwo.data
            if (
                data1.httpStatus == 'OK' && data1.status == StatusResponse.SUCCESS &&
                data2.httpStatus == 'OK' && data2.status == StatusResponse.SUCCESS
            ) {
                load.hideLoading()
                setTimeout(() => {
                    onSuccess([data1.data, data2.data])
                }, 150);
            } else {
                load.hideLoading()
                var value = getDistanceFaill;
                if (data1.message != null) {
                    value.message = data1.message;
                }
                if (data2.message != null) {
                    value.message = data2.message;
                }
                onFail(value);
            }
        } else {
            load.hideLoading()
            //TODO: loi server hoac mot thu gi do
            onFail(ExceptionMessage.errorDefault);
        }

    } catch (error) {
        //TODO: loi mat mang hoac loi config
        load.hideLoading()
        onFail(ExceptionMessage.errorNetWork);
        //console.log("SagaUserError", error)
    }
}
function* getTripsByBookingId({ data = new ModelCallApi() }) {
    const { onSuccess, params, onFail } = data
    //console.log("+++++++", data)
    try {
        load.showLoading()
        const response = yield call(CallApi.getTripsByBookingId, params);
        //console.log(response)
        if (response && response.status == 200) {
            const { httpStatus, statusCode, data, status } = response.data
            if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS && data != null) {
                load.hideLoading()
                var _data = new ModelCallApi()
                var newData = yield data.map(obj => { return { ...obj, ...params } })
                _data.onSuccess = onSuccess
                _data.params = newData[0]
                yield put(actions.addTripsByBookingIdToStore(newData)),
                yield put(actions.getTripDetail(_data))

            } else if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.FAILED && data == null) {
                load.hideLoading()
                var value = getTripsByBookingIdFaill;
                // value.title = `Rất tiếc! Chưa có ${Store.getState().user.isDriver? 'khách' : 'tài xế'} `
                // value.message = ''
                // onFail(value)
                onSuccess()
            } else {
                load.hideLoading()
                var value = getTripsByBookingIdFaill;
                value.message = response.data.message;
                onFail(value);
            }
        } else {
            load.hideLoading()
            //TODO: loi server hoac mot thu gi do
            onFail(ExceptionMessage.errorDefault);
        }

    } catch (error) {
        //TODO: loi mat mang hoac loi config
        load.hideLoading()
        onFail(ExceptionMessage.errorNetWork);
        //console.log("SagaUserError", error)
    }
}

function* getTripDetail({ data = new ModelCallApi() }) {
    const { onSuccess, params, onFail } = data
    try {
        load.showLoading()
        const response = yield call(CallApi.findByUserId, { id: params.userId });
        //console.log(response)
        if (response && response.status == 200) {
            const { httpStatus, data, status } = response.data
            if (httpStatus == 'OK' && status == StatusResponse.SUCCESS && data != null) {
                load.hideLoading()
                yield put(actions.showTripDetail({ trip: params, user: data }))
                setTimeout(() => {
                    onSuccess()
                }, 100);
            } else {
                load.hideLoading()
                var value = getTripFaill;
                value.message = response.data.message;
                onFail(value);
            }
        } else {
            load.hideLoading()
            //TODO: loi server hoac mot thu gi do
            onFail(ExceptionMessage.errorDefault);
        }

    } catch (error) {
        //TODO: loi mat mang hoac loi config
        load.hideLoading()
        onFail(ExceptionMessage.errorNetWork);
        //console.log("SagaUserError", error)
    }
}

function* getAllTripBookingByUser({ data = new ModelCallApi() }) {
    const { onSuccess, params, onFail } = data
    try {
        load.showLoading()
        const response = yield call(CallApi.getAllTripBookingByUser, {});
        //console.log(response)
        if (response && response.status == 200) {
            const { httpStatus, data, status } = response.data
            if (httpStatus == 'OK' && status == StatusResponse.SUCCESS && data != null) {
                var _data = Store.getState().user.isDriver ? data.bookingDtos : data.tripDtos
                yield put(actions.addTripsBookingsAvailable(_data))
                setTimeout(() => {
                    load.hideLoading()
                    onSuccess()
                }, 1000);
            } else {
                load.hideLoading()
                var value = getAllTripBookingByUserFaill;
                value.message = response.data.message;
                onFail(value);
            }
        } else {
            load.hideLoading()
            //TODO: loi server hoac mot thu gi do
            onFail(ExceptionMessage.errorDefault);
        }

    } catch (error) {
        //TODO: loi mat mang hoac loi config
        load.hideLoading()
        onFail(ExceptionMessage.errorNetWork);
        //console.log("SagaUserError", error)
    }
}
function* getTripBookingById({ data = new ModelCallApi() }) {
    const { onSuccess, params, onFail } = data
    //console.log('getTripBookingById', params)
    try {
        load.showLoading()
        const response = yield call(CallApi.getTripBookingById, params);
        //console.log(response)
        if (response && response.status == 200) {
            const { httpStatus, data, status } = response.data
            if (httpStatus == 'OK' && status == StatusResponse.SUCCESS && data != null) {
                let _data = new ModelCallApi()
                _data.params = data
                _data.onSuccess = onSuccess
                yield put(actions.getTripDetail(_data))
            } else {
                load.hideLoading()
                var value = getTripsByBookingIdFaill;
                value.message = response.data.message;
                onFail(value);
            }
        } else {
            load.hideLoading()
            //TODO: loi server hoac mot thu gi do
            onFail(ExceptionMessage.errorDefault);
        }

    } catch (error) {
        //TODO: loi mat mang hoac loi config
        load.hideLoading()
        onFail(ExceptionMessage.errorNetWork);
        //console.log("SagaUserError", error)
    }
}


export function* sagaGetDistanceThreePoints() {
    yield takeEvery(actionTypes.SAGA_GET_DISTANCE_THREE_POINT, getDistanceThreePoint);
}

export function* sagaGetTripsByBookingId() {
    yield takeEvery(actionTypes.SAGA_GET_TRIPS_BY_BOOKING_ID, getTripsByBookingId);
}

export function* sagaGetTripDetail() {
    yield takeEvery(actionTypes.SAGA_GET_TRIP_DETAIL, getTripDetail);
}

export function* sagaGetAllTripBookingByUser() {
    yield takeEvery(actionTypes.SAGA_GET_ALL_TRIP_BOOKING_BY_USER, getAllTripBookingByUser);
}

export function* sagaGetTripBookingById() {
    yield takeEvery(actionTypes.SAGA_GET_BOOKING_TRIP_BY_ID, getTripBookingById);
}