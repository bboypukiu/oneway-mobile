import actions from '../Actions';
import actionTypes from '../Actions/ActionTypes';
import { call, put, takeEvery, fork } from 'redux-saga/effects';
import CallApi from '../Helpers/HttpRequests';
import load from './../Utils/Loading'

import ModelCallApi from '../Model/ModelCallApi';

import StatusResponse, {loginFaill, registerFaill, logoutFaill, changePassFaill, uploadAvatarFaill, uploadProfileFaill, sendPhoneFaill, sendOTPForgotPassFaill, newPassForgotFaill} from '../Constants/StatusResponse';
import ExceptionMessage from '../Constants/ExceptionMessage';

import Store from './../Store'

function* login({data = new ModelCallApi()}) {
  const { onSuccess, params, onFail } = data
  try {
    //console.log('loguot params', params);
    load.showLoading()
    const response = yield call(CallApi.login, params);
    //console.log('login', response)
    if(response && response.status == 200){
      const {httpStatus, statusCode, data, status} = response.data
      if(httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS){
        onSuccess();
        const {userDto, token, permissions, commonInfo, findBooking} = data
        yield put(actions.setCurrentProFile({...userDto, token, permissions, commonInfo, findBooking}))
      }else {
        var value = loginFaill;
        value.message = response.data.message;
        onFail(value);
      }
    }else{
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }
  } catch (error) {
    //console.log(error)
    //TODO: loi mat mang hoac loi config
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
  load.hideLoading()
}

function* logout({data = new ModelCallApi()}) {
  const { onSuccess, params, onFail } = data
  load.showLoading()
  try {
    //console.log('loguot params', params);
    const response = yield call(CallApi.logout, params);
    //console.log('loguot', response);
    if(response && response.status == 200){
      const {httpStatus, statusCode, data, status} = response.data
      // if(httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS){
      if(status == StatusResponse.SUCCESS){
        onSuccess();
      }else {
        var value = logoutFaill;
        value.message = response.data.message;
        onFail(value);
      }
    }else{
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }
    load.hideLoading()
  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}

function* sendPhoneForgotPass({data = new ModelCallApi}) {
  const { onSuccess, params, onFail } = data
  load.showLoading()
  try {
    const response = yield call(CallApi.sendPhoneForgotPass, params);
    //console.log(response)
    if(response && response.status == 200){
      const {httpStatus, statusCode, data, status} = response.data
      if(status == StatusResponse.SUCCESS && httpStatus == "OK"&& statusCode == 200){
        load.hideLoading()
        onSuccess();
      }else{
        load.hideLoading()
        var value = sendPhoneFaill;
        value.message = response.data.message;
        onFail(value);
      }
    }else{
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}

function* sendOTPForgotPass({data = new ModelCallApi}) {
  const { onSuccess, params, onFail } = data
  load.showLoading()
  try {
    const response = yield call(CallApi.verificationCodeForgotPass, params);
    //console.log(response)
    if(response && response.status == 200){
      const {httpStatus, statusCode, data, status} = response.data
      if(status == StatusResponse.SUCCESS && httpStatus == "OK"){
        load.hideLoading()
        onSuccess(data);
      }else{
        load.hideLoading()
        var value = sendOTPForgotPassFaill;
        value.message = response.data.message;
        onFail(value);
      }
    }else{
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}
function* newPassForgotPass({data = new ModelCallApi}) {
  const { onSuccess, params, onFail } = data
  load.showLoading()
  try {
    const response = yield call(CallApi.setNewPassForgotPass, params);
    //console.log(response)
    if(response && response.status == 200){
      const {httpStatus, statusCode, data, status} = response.data
      if(status == StatusResponse.SUCCESS && httpStatus == "OK"&& statusCode == 200){
        load.showDone()
        onSuccess(data);
      }else{
        load.hideLoading()
        var value = newPassForgotFaill;
        value.message = response.data.message;
        onFail(value);
      }
    }else{
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}

function* findByUserId({data = new ModelCallApi()}) {
  const { onSuccess, params, onFail } = data
  //console.log(params)
  try {
    load.showLoading()
    const response = yield call(CallApi.findByUserId, params);
    if(response && response.status == 200){
      const {httpStatus, statusCode, data, status} = response.data
      if(httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS){
        onSuccess();
        const {userDto, token} = data
        yield put(actions.setCurrentProFile({...userDto, token}))
      }else {
        var value = loginFaill;
        value.message = response.data.message;
        onFail(value);
      }
    }else{
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }
  } catch (error) {
    //console.log(error)
    //TODO: loi mat mang hoac loi config
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
  load.hideLoading()
}

function* changePass({data = new ModelCallApi}) {
  const { onSuccess, params, onFail } = data
  load.showLoading()
  try {
    const response = yield call(CallApi.changePassword, params);
    if(response && response.status == 200){
      const { httpStatus, statusCode, status, message } = response.data
      if(httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS){
        load.showDone()
        onSuccess();
      }else{
        load.hideLoading()
        var value = changePassFaill;
        value.message = message;
        onFail(value)
      }
    }else{
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }
  } catch (error) {
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}

function* register({data = new ModelCallApi}) {
  const { onSuccess, params, onFail } = data
  try {
    load.showLoading()
    const response = yield call(CallApi.register, params);
    //console.log(response)
    if(response && response.status == 200){
      const {httpStatus, statusCode, data, status} = response.data
      if(httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS){
        load.showDone()
        onSuccess();
        // yield put(Actions.setCurrentUser(response.data))
      }else{
        
        load.hideLoading()
        var value = registerFaill;
        value.message = response.data.message;
        onFail(value);
      }
    }else{
      load.hideLoading()
      //TODO: loi server hoac mot thu gi do
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    //TODO: loi mat mang hoac loi config
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}

function* uploadUserAvatar({data = new ModelCallApi}) {
  const { onSuccess, params, onFail, paramsTwo } = data
  try {

    load.showLoading()
    const response = yield call(CallApi.uploadAvatar, params);
    if(response && response.status == 200){
      const { httpStatus, statusCode, data, status, message } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.showDone()
        onSuccess();
        yield put(actions.setCurrentUserAvatar(data))
      }else{
        load.hideLoading()
        var value = uploadAvatarFaill
        value.message = message;
        onFail(value);
      }
    }else{
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}

function* updateUserProfile({data = new ModelCallApi()}){
  const { onSuccess, params, onFail } = data
  const {
    userId,
    address,
    birthday,
    email,
    firstname,
    lastname,
    fullname,
    job,
    passport,
    phone,
    sex,
    userCode,
    username,
    avatar,
  } = Store.getState().user

  var user = {
    userId,
    address,
    birthday,
    email,
    firstname,
    lastname,
    fullname,
    job,
    passport,
    phone,
    sex,
    userCode,
    username,
    avatar,   
  }

  user = {...user, ...params}
  try {
    //console.log(user)
    const response = yield call(CallApi.updateUserProfile, user);
    if(response && response.status == 200){
      const { httpStatus, statusCode, data, status, message } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        load.showDone()
        onSuccess();
        yield put(actions.setCurrentProFile(data))
      }else{
        //console.log(response)
        load.hideLoading()
        var value = uploadProfileFaill
        value.message = message;
        onFail(value);
      }
    }else{
      load.hideLoading()
      onFail(ExceptionMessage.errorDefault);
    }

  } catch (error) {
    load.hideLoading()
    onFail(ExceptionMessage.errorNetWork);
    //console.log("SagaUserError",error)
  }
}

function* getBaseInfo({data = new ModelCallApi()}){
  const { onSuccess, params, onFail } = data

  try {
    const response = yield call(CallApi.getBaseInfo, params);
    //console.log(response)
    if(response && response.status == 200){
      const { httpStatus, statusCode, data, status, message } = response.data
      if (httpStatus == 'OK' && statusCode == 200 && status == StatusResponse.SUCCESS) {
        onSuccess(data)
        yield put(actions.setBaseInfo(data))
      }else{
      }
    }else{
    }

  } catch (error) {
    //console.log("SagaUserError",error)
  }
}



export function* sagaLogin() {
  yield takeEvery(actionTypes.SAGA_USER_LOGIN, login);
}
export function* sagaLogout() {
  yield takeEvery(actionTypes.SAGA_USER_LOGOUT, logout);
}
export function* sagaSendPhone() {
  yield takeEvery(actionTypes.SAGA_USER_SEND_NUMBER_PHONE_FORGOT_PASS, sendPhoneForgotPass);
}

export function* sagasSendOTPForgotPass() {
  yield takeEvery(actionTypes.SAGA_USER_SEND_OTP_FORGOT_PASS, sendOTPForgotPass);
}
export function* sagasNewPassForgotPass() {
  yield takeEvery(actionTypes.SAGA_USER_NEW_PASS_FORGOT_PASS, newPassForgotPass);
}

export function* sagaChangePass() {
  yield takeEvery(actionTypes.SAGA_USER_CHANGE_PASS, changePass);
}

export function* sagaRegister() {
  yield takeEvery(actionTypes.SAGA_USER_REGISTER, register);
}

export function* sagaUploadAvatar() {
  yield takeEvery(actionTypes.SAGA_UPLOAD_USER_AVATAR, uploadUserAvatar);
}

export function* sagaFindByUserId() {
  yield takeEvery(actionTypes.SAGA_FIND_BY_USER_ID, findByUserId);
}

export function* sagaUpdateProfile(){
  yield takeEvery(actionTypes.SAGA_UPDATE_USER_PROFILE, updateUserProfile);
}

export function* sagaGetBaseInfo(){
  yield takeEvery(actionTypes.SAGA_GET_BASE_INFO, getBaseInfo);
}