import * as React from 'react'
import { Image, Linking, View, Alert } from 'react-native'

import styles from './styles'
import IconAwesome5 from 'react-native-vector-icons/FontAwesome5'
import IconAwesome from 'react-native-vector-icons/FontAwesome'
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Store from './../Store'

// stack navigation in bottomtab
import HomeStack from './HomeStack'
import NotificationStack from './NotificationStack'
import BookingStack from './BookingStack'
const Tab = createBottomTabNavigator();

import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import BadgeNotification from '../Components/BadgeNotification'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

export default BottomTab = ({ navigation, route }) => {
    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            //console.log('adjsf')
        });

        return unsubscribe;
    }, [navigation]);
    return (
        <Tab.Navigator
            tabBarOptions={{
                showLabel: false,
                activeTintColor: 'rgba(255,255,255,1)',
                inactiveTintColor: 'rgba(0, 0, 0, 1)',
                style: styles.containerTabbar,
                headerTitleAlign: 'center'
            }}
            backBehavior='none'
            initialRouteName={"HomeTab"}
        >
            {
                route?.params && route.params.isDriver &&
                <Tab.Screen name="FilterTab"
                    component={() => null}
                    options={{
                        tabBarIcon: ({ focused, color, size }) => (<Image style={[{ tintColor: color }, styles.iconFilter, focused && { height: perfectSize(28) * 1.2, width: perfectSize(33) * 1.2 }]} source={require('./../../assets/icon_design/filter.png')} />),
                    }}
                    listeners={({ navigation, route }) => ({
                        tabPress: e => {
                            navigation.navigate("Filter_stack")
                            e.preventDefault();
                        }
                    })}
                />
            }
            <Tab.Screen name="BookingTab"
                component={BookingStack}
                options={{
                    tabBarIcon: ({ focused, color, size }) => (<IconAwesome name="calendar" size={perfectSize(26) * (focused ? 1.2 : 1)} color={color} />),
                }} />
            <Tab.Screen name="HomeTab"
                component={HomeStack}
                options={{
                    tabBarIcon: ({ focused, color, size }) => (<IconAwesome5 name="home" size={perfectSize(28) * (focused ? 1.2 : 1)} color={color} />),
                }} />
            <Tab.Screen name="NotificationTab"
                component={() => null}
                options={{
                    tabBarIcon: ({ focused, color, size }) => (<View>
                        <IconMaterial name="notifications" size={perfectSize(38) * (focused ? 1.2 : 1)} color={color} style={styles.iconNotifi} />
                        <BadgeNotification size={perfectSize(50) * (focused ? 1.1 : 1) / 5} />
                    </View>),
                }}
                listeners={({ navigation, route }) => ({
                    tabPress: e => {
                        navigation.navigate("Notification")
                        e.preventDefault();
                    }
                })} />
            <Tab.Screen
                name="HotlineTab"
                component={() => null}
                options={{
                    tabBarIcon: ({ focused, color, size }) => (<Image style={[{ tintColor: color }, styles.iconHotline, focused && { height: perfectSize(28) * 1.2, width: perfectSize(23) * 1.2 }]} source={require('./../../assets/icon_design/phone.png')} />),
                }}
                listeners={({ navigation, route }) => ({
                    tabPress: e => {
                        const { commonInfo } = Store.getState().user
                        if (commonInfo && commonInfo.phoneNumber) {
                            Linking.openURL(`tel:${commonInfo.phoneNumber}`)
                        } else {
                            Alert.alert(null, 'Hệ Thống Chăm Sóc Khách Hàng Đang Bảo Trì!', [{
                                text: 'Đồng ý',
                                onPress: () => {
                                    error = null
                                },
                            }])
                        }
                        e.preventDefault();
                    }
                })}
            />
        </Tab.Navigator>
    );
}
