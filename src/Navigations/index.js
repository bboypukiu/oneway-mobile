import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { Text } from 'react-native'
//Stack
import LoginStack from './LoginStack'
import BottomTab from './BottomTab'
import OfficialHomeScreen from './../Screens/OfficialHome/OfficialHomeScreen'

import styles from './styles'
import Store from './../Store'

import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import NewBookingScreen from '../Screens/Booking/NewBookingScreen'
import BookingDetail from '../Screens/Booking/BookingDetail'
import FilterScreen from '../Screens/Filter/FilterScreen'
import { BackButton } from './HeaderElements'
import ProfileScreen from '../Screens/Profile/ProfileScreen'
import ResultScreen from '../Screens/Result/ResultScreen'
import NotificationDetailScreen from '../Screens/Nofitication/NotificationDetailScreen'
import NotificationScreen from '../Screens/Nofitication/NotificationScreen'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

const RootStack = createStackNavigator();

export default Navigator = React.forwardRef(({ onStateChange}, ref) => {
  return (
    <NavigationContainer
      ref={ref ? ref : React.createRef()}
      onStateChange={()=>onStateChange&&onStateChange()}
    >
      <RootStack.Navigator
        initialRouteName={Store.getState().user.token == "" ? "LoginStack" : "Official_home"}
        headerMode="screen"
        screenOptions={({ navigation }) => ({
          headerStyle: styles.headerStyle,
          headerLeft: (val) => BackButton(val.canGoBack, navigation),
          headerTitleAlign: 'center'
        })}
      >
        <RootStack.Screen
          name="LoginStack"
          component={LoginStack}
          options={() => ({
            headerShown: false
          })}
        />
        <RootStack.Screen
          name="Official_home"
          component={OfficialHomeScreen}
          options={() => ({
            headerShown: false
          })}
        />
        <RootStack.Screen
          options={({ navigation, route }) => ({
            headerTitle: props => <Text style={styles.titleLogin}>Thông Tin Cá Nhân</Text>,
          })}
          name="Profile"
          component={ProfileScreen}
        />
        <RootStack.Screen
          name="MyTabs"
          component={BottomTab}
          options={() => ({
            headerBackTitle: ' ',
            headerShown: false
          })}
        />
        <RootStack.Screen
          name="Create_booking"
          component={NewBookingScreen}
          options={({ navigation }) => ({
            headerShown: true,
            headerTitle: props => <Text style={styles.titleLogin}>{'Thêm Booking Mới'}</Text>
          })}
        />
        <RootStack.Screen
          name="Booking_detail"
          component={BookingDetail}
          options={({ navigation }) => ({
            headerTitle: props => <Text style={styles.titleLogin}>{Store.getState().user.isDriver ? 'Thông Tin Khách Hàng' : 'Thông Tin Chuyến Xe'}</Text>,
          })}
        />
        <RootStack.Screen
          name="Filter_stack"
          component={FilterScreen}
          options={({ navigation }) => ({
            headerShown: true,
            headerTitle: props => <Text style={styles.titleLogin}>{'Lọc Khách'}</Text>
          })}
        />
        <RootStack.Screen
          name="Result_screen"
          component={ResultScreen}
          options={({ navigation }) => ({
            headerShown: true,
            headerTitle: props => <Text style={styles.titleLogin}>{'Kết Quả'}</Text>
          })}
        />
        <RootStack.Screen
          name="Notification"
          component={NotificationScreen}
          options={({ navigation }) => ({
            headerShown: true,
            headerTitle: props => <Text style={styles.titleLogin}>{'Thông Báo'}</Text>
          })}
        />
        <RootStack.Screen
          name="Notification_detail"
          component={NotificationDetailScreen}
          options={({ navigation }) => ({
            headerShown: true,
            headerTitle: props => <Text style={styles.titleLogin}>{'Nội Dung Chi Tiết'}</Text>
          })}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
})
