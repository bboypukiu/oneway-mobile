import * as React from 'react'
import {Text} from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'

import styles from './styles'

import NotificationScreen from './../Screens/Nofitication/NotificationScreen'

const Notification = createStackNavigator();
export default NotificationStack = () => {
  return (
    <Notification.Navigator
      screenOptions={{
        headerStyle: styles.headerStyle,
        headerTitleAlign: 'center'
        // title: ''
      }}
    >
      <Notification.Screen 
        name="Notification" 
        component={NotificationScreen} 
        options={()=>({
          headerTitle: props => <Text style={styles.titleForgotPassword}>Thông Báo</Text>,
        })}
      />
    </Notification.Navigator>
  )
}