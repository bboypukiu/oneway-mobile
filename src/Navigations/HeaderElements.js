import React, { useState, useEffect } from 'react'
import { StyleSheet, Image, Vibration } from 'react-native'
import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons'
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import Store from './../Store'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Toast from '../Utils/Toast'
import actions from '../Actions'
import ModelCallApi from '../Model/ModelCallApi'

const perfectSize = create(PREDEF_RES.iphoneX.dp)

export const BackButton = (canGoBack, navigation) => (
    canGoBack && <TouchableOpacity onPress={() => navigation.goBack()} style={styles.buttonHeader}>
        <IconSimpleLine name={'arrow-left'} size={perfectSize(25)} />
    </TouchableOpacity>
)

export const BackToOfficalHome = (navigation) => (
    <TouchableOpacity onPress={() => navigation.navigate('Official_home')} style={styles.buttonHeader}>
        <IconSimpleLine name={'arrow-left'} size={perfectSize(25)} />
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    buttonHeader: {
        paddingHorizontal: perfectSize(18)
    },
})

export const VolumeButton = () => {
    const [isOn, setOn] = useState(true);
    const setNotification = () => {
        if(!Store.getState().user.isDriver) return
        var data = new ModelCallApi()
        data.params = {
            status: isOn?"off":"on"
        }
        data.onSuccess = (value) => {
            if(value == "on"){
                Toast("Bật Thông Báo!")
            }else{
                Toast("Tắt Thông Báo!")
            }
            setOn(value == "on")
        }
        Store.dispatch(actions.setNotificationStatus(data))
    }

    useEffect(() => {
        if(!Store.getState().user.isDriver) return
        var data = new ModelCallApi()
        data.onSuccess = (value) => {
            setOn(value == "on")
        }
        Store.dispatch(actions.getNotificationStatus(data))
    }, []);

    return (
        Store.getState().user.isDriver &&
        <TouchableOpacity style={styles.buttonHeader} onPress={setNotification}>
            <Image source={isOn?require('./../../assets/icon_design/alert_on.png'):require('./../../assets/icon_design/alert.png')}
                style={{width: perfectSize(35), height: perfectSize(35), }}
                resizeMode='contain' />
        </TouchableOpacity>
    )
}

export const AddButton = (navigation, params) => {
    return (
        <TouchableOpacity style={styles.buttonHeader} onPress={() => navigation.navigate('Create_booking', { ...params })}>
            <IconMaterial name={'add'} size={perfectSize(36)} style={{ width: perfectSize(41) }} color='#6979F8' />
        </TouchableOpacity>
    )
}
