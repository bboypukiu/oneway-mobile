import * as React from 'react'
import { Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'

import styles from './styles'

import HomeMapScreen from '../Screens/Home/HomeMapScreen'
import {BackButton, VolumeButton, BackToOfficalHome} from './HeaderElements'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Store from './../Store'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)

const Home = createStackNavigator();
export default HomeStack = () => {
  return (
    <Home.Navigator
      screenOptions={({ navigation }) => ({
        headerStyle: styles.headerStyle,
        headerLeft: (val) => BackButton(val.canGoBack, navigation),
        headerTitleAlign: 'center'
      })}
    >
      <Home.Screen
        name="Home_Map"
        component={HomeMapScreen}
        options={({navigation, route}) => ({
          headerTitle: props => <Text style={styles.titleLogin}>{Store.getState().user.isDriver ? 'Trang Chủ Tài Xế' : 'Trang Chủ Khách'}</Text>,
          headerLeft: props =>props.canGoBack?BackButton(props.canGoBack, navigation): BackToOfficalHome(navigation),
          headerRight: props =><VolumeButton/>
        })}
      />
    </Home.Navigator>
  )
}
