
import * as React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'

import styles from './styles'

import LoginScreen from './../Screens/Login/LoginScreen'
import RegisterScreen from './../Screens/Register/RegisterScreen'
import ForgotPasswordScreen from './../Screens/ForgotPassword/ForgotPasswordScreen'
import OTPScreen from './../Screens/ForgotPassword/OtpScreen'
import ChangePasswordScreen from './../Screens/ChangePassword/ChangePasswordScreen'
import Terms from './../Screens/Terms/TermsScreen'

import {BackButton} from './HeaderElements'
const Login = createStackNavigator();

export default LoginStack = () => {
  return (
    <Login.Navigator
      screenOptions={({ navigation }) => ({
        headerStyle: styles.headerStyle,
        headerLeft: (val) => BackButton(val.canGoBack, navigation),
        headerTitleAlign: 'center'
      })}
    >
      <Login.Screen
        name="Login"
        component={LoginScreen}
        options={({ navigation, route }) => ({
          title: '',
        })}
      />
      <Login.Screen
        name="Register"
        component={RegisterScreen}
        options={() => ({
          headerTitle: props => <Text style={styles.titleLogin}>Đăng Ký</Text>,
          headerBackTitle: ' '
        })}
      />
      <Login.Screen
        name="Forgot_password"
        component={ForgotPasswordScreen}
        options={() => ({
          headerTitle: props => <Text style={styles.titleForgotPassword}>Quên Mật Khẩu</Text>,
          headerBackTitle: ' '
        })}

      />
      <Login.Screen
        name="OTP"
        component={OTPScreen}
        options={() => ({
          headerTitle: props => <Text style={styles.titleForgotPassword}>Quên Mật khẩu</Text>,
          headerBackTitle: ' '
        })}
      />
      <Login.Screen
        name="Change_password"
        component={ChangePasswordScreen}
        options={() => ({
          headerTitle: props => <Text style={styles.titleForgotPassword}>Thay Đổi Thông Tin</Text>,
          headerBackTitle: ' '
        })}

      />
      <Login.Screen
        name="Terms"
        component={Terms}
        options={() => ({
          headerTitle: props => <Text style={styles.titleTerms}>Điều Khoản Và Điều Kiện</Text>,
          headerBackTitle: ' '
        })}
      />
    </Login.Navigator>
  );
}
