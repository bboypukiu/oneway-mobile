import { StyleSheet } from 'react-native'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
const perfectSize = create(PREDEF_RES.iphoneX.dp)
export default styles = StyleSheet.create({
    lableRightLogin: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'rgb(108,120,240)',
        marginHorizontal: 20
    },
    titleLogin: {
        fontSize: perfectSize(24),
        fontWeight: 'bold',
        fontFamily: 'roboto',
    },
    titleTerms: {
        fontSize: perfectSize(17),
        fontWeight: '500',
        fontFamily: 'roboto',
    },
    titleForgotPassword: {
        fontSize: perfectSize(24),
        fontWeight: 'bold',
        fontFamily: 'roboto',
    },
    containerTabbar: {
        // borderTopWidth: 2,
        // borderTopColor: '#007AFF',
        backgroundColor: '#6979F8'
    },
    headerStyle: {
        borderBottomColor: 'white',
        borderBottomWidth: 0,
        shadowOffset: {
            height: 0,
            width: 0
        },
        shadowRadius: 0,
        shadowColor: 'transparent',
        elevation: 0
    },
    iconHotline: {
        height: perfectSize(30),
        width: perfectSize(25),
        transform: [{ translateY: -perfectSize(1) }],
    },
    iconFilter: {
        transform: [{ translateY: perfectSize(1) }],
        height: perfectSize(28),
        width: perfectSize(33)
    },
    iconNotifi: {
        transform: [{ translateY: perfectSize(2) }],
    },


})