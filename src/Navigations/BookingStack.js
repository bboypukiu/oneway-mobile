import * as React from 'react'
import { Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'

import styles from './styles'
import BookingScreen from './../Screens/Booking/BookingSceen'
import {BackButton, AddButton} from './HeaderElements'
import Store from './../Store'
const Booking = createStackNavigator();
export default BookingStack = () => {
  return (
    <Booking.Navigator
      screenOptions={({navigation}) => ({
        headerStyle: styles.headerStyle,
        headerLeft: (val) => BackButton(val.canGoBack, navigation),
        headerTitleAlign: 'center'
      })}
    >
      <Booking.Screen
        name="Booking"
        component={BookingScreen}
        options={({navigation}) => ({
          headerTitle: props => <Text style={styles.titleLogin}>{'Booking Của Tôi'}</Text>,
          headerBackTitle: ' ',
        })}
      />
    </Booking.Navigator>
  )
}
