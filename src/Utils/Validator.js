import validator from 'validator'
export const CheckNumberPhone = (str) =>{
  if(!str){
    return "Vui lòng nhập số điện thoại của bạn!"
  }
  if(!validator.isNumeric(str, {no_symbols: true})){
    return 'Số điện thoại chỉ chứa ký tự số!'
  }
  if(str.length < 5){
    return "Số điện thoại quá ngắn!"
  }
  if(str.length > 15){
    return 'Số điện thoại quá dài!'
  }

  return ""
}

export const CheckEmailOrNumberPhone = (str) =>{
  if(!str){
    return `Vui lòng nhập số điện thoại hoặc email!`
  }
  if(str.length < 5){
    return "Số điện thoại hoặc email quá ngắn!"
  }
  if(str.length > 30){
    return 'Số điện thoại hoặc email quá dài!'
  }
  if(validator.isNumeric(str, {no_symbols: true}) || validator.isEmail(str)){
    return ""
  }
  return 'Vui lòng đúng định dạng số điện thoại hoặc email';
}

export const CheckEmail = (str = '', isRequire = true) =>{
  if(!isRequire && !str){
    return ""
  }

  if(!str){
    return `Vui lòng nhập email!`
  }
  if(validator.isEmail(str)){
    return ""
  }
  if(str.length < 5){
    return `Email phải chứa ít nhất 5 ký tự!`
  }
  if(str.length >30){
    return `Email quá dài!`
  }

  return "Email không đúng định dạng"
}

export const CheckNormal = (str = '', textValue = 'Mật khẩu', length = 0) =>{
  if(!str){
    return `Vui lòng nhập ${textValue.toLowerCase()}!`
  }
  if(str.length < length){
    return `${textValue} phải chứa ít nhất ${length} ký tự!`
  }
  return ""
}

export const CheckAlpha = (str = '', textName = 'Mật khẩu', length = 0) =>{
  if(!str){
    return `Vui lòng nhập ${textName.toLowerCase()}!`
  }
  if(!validator.isAlpha(str)){
    return 'Vui lòng nhập đúng tên';
  }
  if(str.length < length){
    return `${textName} phải chứ ít nhất 6 ký tự!`
  }
  return '';
}

export const CheckTime = (str = '') =>{
  if(!str){
    return true
  }
  return ""
}

export default {
  CheckAlpha,
  CheckNumberPhone,
  CheckNormal,
  CheckEmailOrNumberPhone,
  CheckEmail,
  CheckTime
};