import Toast from 'react-native-root-toast';


// Add a Toast on screen.
export default toast = ( mgs ='',duration = 3000, position = 20) => {
    let toast = Toast.show(mgs, {
        duration: Toast.durations.LONG,
        position,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        visible: true
        // onShow: () => {
        //     // calls on toast\`s appear animation start
        // },
        // onShown: () => {
        //     // calls on toast\`s appear animation end.
        // },
        // onHide: () => {
        //     // calls on toast\`s hide animation start.
        // },
        // onHidden: () => {
        //     // calls on toast\`s hide animation end.
        // }
    });

    setTimeout(function () {
        Toast.hide(toast);
    }, duration);    
}