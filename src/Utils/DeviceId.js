import React from 'react'
import {Platform, Alert, BackHandler} from 'react-native'
import DeviceInfo from 'react-native-device-info'
import IMEI from 'react-native-imei'
import {check, PERMISSIONS, RESULTS, request, openSettings} from 'react-native-permissions';

export default DeviceId = async () => {
    // if(Platform.OS == 'ios'){
        return await DeviceInfo.getUniqueId()
    // }
    // if(Platform.OS == 'android'){
    //     var permissionReadPhone = await check(PERMISSIONS.ANDROID.READ_PHONE_STATE)
    //     if(permissionReadPhone == RESULTS.GRANTED){
    //         var device = await IMEI.getImei()

    //         //console.log(device)
    //         return device[0]
    //     }else if(permissionReadPhone == RESULTS.BLOCKED){
    //         Alert.alert(
    //             'Ứng dụng không được cấp quyền', 
    //             'Vui lòng chọn cấp quyền cho ứng dung, trong phần cài đặt!', 
    //             [{text: 'Mở cài đặt', onPress: () => {
    //                 BackHandler.exitApp();
    //                 openSettings(1).catch(() => {
    //                     //console.warn('cannot open settings')
    //                 })
    //             }},])
    //     }else{
    //         await request(PERMISSIONS.ANDROID.READ_PHONE_STATE)
    //     }
    // }
    return ''
}

export const permissionGetDeviceId = async () => {
    var permissionReadPhone = await check(PERMISSIONS.ANDROID.READ_PHONE_STATE)
    if(permissionReadPhone != RESULTS.GRANTED){
        await request(PERMISSIONS.ANDROID.READ_PHONE_STATE)
    }
}