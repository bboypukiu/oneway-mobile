import Store from '../Store'
import actions from './../Actions'
const showLoading = () => {
    Store.dispatch(actions.load.loading())
}

const showError = () => {
    Store.dispatch(actions.load.error())
}

const showDone = () => {
    Store.dispatch(actions.load.done())
}

const hideLoading = () => {
    Store.dispatch(actions.load.hinde())
}

export default {
    showDone,
    showError,
    showLoading,
    hideLoading
}