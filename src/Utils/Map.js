import Geocoder from 'react-native-geocoding'
import { GOOGLE_PLACES_API_KEY } from '../Constants/Api';
import { Dimensions } from 'react-native';
const { height, width } = Dimensions.get( 'window' );

export const getRegionForCoordinates = (points) =>{
    if(points.length < 2){
        return {
            latitude: points[0].latitude,
            longitude: points[0].longitude,
            latitudeDelta: points[0].latitudeDelta,
            longitudeDelta: points[0].longitudeDelta
        }
    }
    let minX, maxX, minY, maxY;

    // init first point
    ((point) => {
        minX = point.latitude;
        maxX = point.latitude;
        minY = point.longitude;
        maxY = point.longitude;
    })(points[0]);

    // calculate rect
    points.map((point) => {
        minX = Math.min(minX, point.latitude);
        maxX = Math.max(maxX, point.latitude);
        minY = Math.min(minY, point.longitude);
        maxY = Math.max(maxY, point.longitude);
    });
    
    const deltaX = (maxX - minX)*3;
    const deltaY = (maxY - minY)*3;

    return {
        latitude: points[0].latitude,
        longitude: points[0].longitude,
        latitudeDelta: deltaX > points[0].latitudeDelta ? deltaX : points[0].latitudeDelta,
        longitudeDelta: deltaY > points[0].longitudeDelta ? deltaY : points[0].longitudeDelta
    };
}

export const ranDomeMaker = ({ longitude, latitude, latitudeDelta, longitudeDelta }, num = 2) => {
    var arr = []
    for (let i = 0; i < num; i++) {
        arr.push({
            longitude: longitude + (Math.random(-3, 3) / 1000),
            latitude: latitude + (Math.random(-3, 3) / 1000),
        })
    }
    return arr
}

export const mapCoordinateToAddress = async (coordinate) => {
    Geocoder.init(GOOGLE_PLACES_API_KEY, {language : "vi"})
    return  await Geocoder.from(coordinate)
        .then(json => {
            //console.log(json)
            var formatted_address = json.results[0].formatted_address;
            if (formatted_address.includes("Việt Nam")) {
                var address =  `${formatted_address}`.replace(', Việt Nam', '')
                return address
            }
            return null
        })
        .catch(error => {
            //console.warn(error)
            return null
        });
}

export const getZoomDefault = (LATITUDE, LONGITUDE) => {
    const LATITUDE_DELTA = 0.01;
    const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);
    return {
		latitude: LATITUDE,
		longitude: LONGITUDE,
		latitudeDelta: LATITUDE_DELTA,
		longitudeDelta: LONGITUDE_DELTA
	}
}