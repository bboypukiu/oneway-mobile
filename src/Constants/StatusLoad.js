export default statusLoadProcess = {
    loading: 'loading',
    done: 'done',
    hide: 'hide',
    error: 'error'
}