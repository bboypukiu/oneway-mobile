export const host = 'http://api.xemotchieu.vn/'
export const version = 'api/v1/'
export const GOOGLE_PLACES_API_KEY = "AIzaSyDHOSE8xr0-NCA-8XQQvtvki9jDRB8P_S0"
export const urlAvatar = `${host}user/avatar/`
const booking = {
    register: `${host}${version}booking`,
    delete: `${host}${version}booking/delete`,
    // update: `$${host}${version}update`,
    getBookings: `${host}${version}bookings`,
    getBookingByTripId: `${host}${version}getBookingByTripId`,
    findBookingsByDateAndAddress: `${host}${version}findBookings/`,
    cancelFindBookingsByDateAndAddress: `${host}${version}cancelFind`,
    unfollow: `${host}${version}unfollow-booking`,
    getAllBooking: `${host}${version}all-bookings`,
    getBookingById: `${host}${version}bookingById`,
}

const user = {
    login: `${host}${version}authentication/login`,
    logout: `${host}${version}logout`,
    register: `${host}${version}register`,
    updateUserInfo: `${host}user/update`,
    findByUserId: `${host}user/findByUserId`,
    changePassword: `${host}user/change-password`,
    uploadAvatar: `${host}user/avatar`,
    getBaseInfo:`${host}user/v1/getBaseInfo`
}

const trip = {
    distance: `${host}${version}cal/`,
    getTripsByBookingId: `${host}${version}getTripsByBookingId`, 
    getTrips: `${host}${version}trips`,
    delete: `${host}${version}trip/delete`, 
    register: `${host}${version}trip`,
    unfollow: `${host}${version}unfollow-trip`,
    getAllTrip: `${host}${version}all-trips`,
    getTripById: `${host}${version}tripById`
}

const notification = {
    getNotifications: `${host}${version}notifications`,
    setNotificationStatus: `${host}${version}notification/change`, 
    getNotificationStatus: `${host}${version}notification/status`, 
    getNotificationDetail: `${host}${version}read-notification`, 
}

const forgot_password = {
    sendPhone: `${host}${version}forgot-password`,
    verificationCode: `${host}${version}forgot-password/verificationCode`,
    newPassword: `${host}${version}forgot-password/newPassword`,
}

export default {
    user,
    booking,
    trip,
    notification,
    forgot_password
}