export default {
    errorNetWork: {
        title: 'Lỗi Kết Nối!',
        message: 'Vui Lòng Kiểm Tra Kết Nối Mạng và Thử Lại!'
    },
    errorDefault: {
        title: 'Đã Xảy Ra Lỗi!',
        message: 'Vui Lòng Thử Lại!'
    },
}
