import Store from '../Store'
export default {
    SUCCESS:'SUCCESS',
    FAILED: 'FAILED'
}

export const loginFaill = {
    title: 'Đăng nhập thất bại',
    message: 'Vui lòng kiểm tra'
}

export const sendPhoneFaill = {
    title: 'Gửi số điện thoại thất bại',
    message: 'Vui lòng kiểm tra'
}

export const sendOTPForgotPassFaill = {
    title: 'Gửi OTP thất bại',
    message: 'Vui lòng kiểm tra'
}

export const newPassForgotFaill = {
    title: 'Tạo mới mật khẩu thất bại',
    message: 'Vui lòng kiểm tra'
}

export const changePassFaill = {
    title: 'Đổi mật khẩu thất bại',
    message: 'Vui lòng kiểm tra'
}

export const logoutFaill = {
    title: 'Đăng xuất thất bại',
    message: 'Vui lòng kiểm tra'
}

export const uploadAvatarFaill = {
    title: 'Cập nhật ảnh thất bại',
    message: 'Vui lòng kiểm tra'
}

export const uploadProfileFaill = {
    title: 'Cập nhật họ tên thất bại',
    message: 'Vui lòng kiểm tra'
}

export const registerFaill = {
    title: 'Đăng ký thất bại',
    message: 'Vui lòng kiểm tra'
}

export const bookingFaill = {
    title: 'Thêm booking mới thất bại',
    message: 'Vui lòng kiểm tra'
}

export const getBookingsFaill = {
    title: 'Lấy dữ liệu booking thất bại',
    message: 'Vui lòng kiểm tra'
}

export const getDistanceFaill = {
    title: 'Tính toán khoảng cách thất bại',
    message: 'Vui lòng kiểm tra'
}

export const getTripsByBookingIdFaill = {
    title: 'Xem chi tiết booking thất bại',
    message: 'Vui lòng kiểm tra'
}

export const getTripFaill = {
    title: `Lấy thông tin thất bại`,
    message: 'Vui lòng kiểm tra' 
}

export const getTripsBookingsAvailable = {
    title: `Lọc khách thất bại`,
    message: 'Vui lòng kiểm tra' 
}

export const cancelFindBookingsFaill = {
    title: `Huỷ lọc khách thất bại`,
    message: 'Vui lòng kiểm tra' 
}

export const getNotificationsFaill = {
    title: 'Lấy dữ liệu thông báo thất bại',
    message: 'Vui lòng kiểm tra'
}

export const getNotificationStatusFaill = {
    title: 'Lấy trạng thái thông báo thất bại',
    message: 'Vui lòng kiểm tra lại'
}

export const setNotificationStatusFaill = {
    title: 'Thay đổi trạng thái thông báo thất bại',
    message: 'Vui lòng kiểm tra lại'
}

export const getNotificationDetailFaill = {
    title: 'Xem nội dung thông báo thất bại',
    message: 'Vui lòng kiểm tra lại'
}

export const getAllTripBookingByUserFaill = {
    title: `Lấy thông tin thất bại`,
    message: 'Vui lòng kiểm tra' 
}

export const getTripBookingByIdFaill = {
    title: `Lấy thông tin thất bại`,
    message: 'Vui lòng kiểm tra' 
}