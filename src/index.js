import React, { useEffect } from 'react';
import { StatusBar } from 'react-native'
import Navigator from './Navigations';
import { Provider } from 'react-redux';
import Store from './Store';
import LoadingProress from './Components/Load';
import FirebaseNotification from './Components/FirebaseNotification'
import GeolocationWrapper from './Components/Map/GeolocationWrapper'
import SplashScreen from './Screens/SplashScreen'
import { RootSiblingParent } from 'react-native-root-siblings';

function App() {
  //console.disableYellowBox = true; 
  return (
    <Provider store={Store}>
      <RootSiblingParent>
        <StatusBar barStyle='dark-content' backgroundColor='white' />
        <LoadingProress />
        {/* <SplashScreen /> */}
        <GeolocationWrapper />
        <FirebaseNotification />
      </RootSiblingParent>
    </Provider>
  );
}

export default App;